package healthTracker;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import healthTracker.captures.Exercise;
import healthTracker.captures.Food;
import healthTracker.user.SystemUser;
import healthTracker.utils.DatabaseController;

/**
 * Administration.java
 * 
 * Static methods allowing an administrator to delete a group or user, get unapproved
 * Food or Exercise items as well as approving or deleting them.
 * 
 * @author 6183891
 */
public class Administration {
	
	/**
	 * Administrative method to delete a group from the system. This can only be
	 * called by an administrative user.
	 * @param id The ID of the group to delete.
	 */
	public static void deleteGroup(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("DELETE FROM GroupDetails WHERE groupID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not delete group!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				sql = null;
				db.closeConnection();
				db = null;
			}
		}
	}
	
	/**
	 * Administrative method to delete a user from the system. This can only be called
	 * by an administrative user.
	 * @param id
	 */
	public static void deleteUser(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("DELETE FROM SystemUser WHERE userID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not delete user!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				sql = null;
				db.closeConnection();
				db = null;
			}
		}
	}
	
	/**
	 * Method for an administrator to retrieve all of the custom exercises which have
	 * yet to be approved, so that they can take action on them.
	 * @return An ArrayList containing all of the Exercises.
	 */
	public static ArrayList<Exercise> getUnapprovedExercises() {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		ArrayList<Exercise> exercises = new ArrayList<Exercise>();
		
		try {			
			// If you're having SQLException problems I feel bad for you son
			// I got 99 problems but an Exception aint one
			sql = db.getConnection().prepareStatement("SELECT * FROM Exercise WHERE approved = false");
			result = db.runQuery(sql);
			// Loop through all of the un-approved exercises, creating an Exercise object
			// And adding them to the ArrayList.
			while (result.next()) {
				int exerciseID = result.getInt("exerciseID");
				String name = result.getString("name");
				int calsPerUnit = result.getInt("calsPerUnit");
				String description = result.getString("description");
				
				Exercise exercise = new Exercise(exerciseID, calsPerUnit, name,
						description, false);
				
				exercises.add(exercise);
			}
		// Yikes, better catch this naughty little Exception.
		} catch (SQLException e) {
			// Do nothing, we'll just return an empty ArrayList.
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					result.close();
				} catch (SQLException exc) {
					System.err.println("Could not close ResultSet!");
					System.err.println(exc.getMessage());
				} finally {
					sql = null;
					result = null;
					db.closeConnection();
					db = null;
				}
			}
		}
		
		return exercises;
	}
	
	/**
	 * Method for an administrator to get all of the food items which have yet to be
	 * approved.
	 * @return ArrayList containing all of the Foods.
	 */
	public static ArrayList<Food> getUnapprovedFoods() {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ArrayList<Food> foods = new ArrayList<Food>();
		ResultSet result = null;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM Food WHERE approved = false");
			result = db.runQuery(sql);
			// Loop through all of the un-approved food items and add them to the ArrayList.
			while (result.next()) {
				int foodID = result.getInt("foodID");
				String name = result.getString("name");
				int cals = result.getInt("cals");
				String description = result.getString("description");
				
				Food food = new Food(foodID, cals, name, description, false);
				
				foods.add(food);
			}			
		// Again, need to catch this just in case.
		} catch (SQLException e) {
			// Do nothing, we'll just return an empty ArrayList.
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					result.close();
				} catch (SQLException exc) {
					System.err.println("Could not close ResultSet!");
					System.err.println(exc.getMessage());
				} finally {
					sql = null;
					result = null;
					db.closeConnection();
					db = null;
				}
			}
		}
		
		return foods;
	}
	
	/**
	 * Method to approve a specific food item
	 * @param id The ID of the food item to approve.
	 */
	public static void approveFoodItem(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("UPDATE food SET approved = true WHERE foodID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Failed to approve food item!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				sql = null;
				db.closeConnection();
				db = null;
			}
		}
	}
	
	/**
	 * A food item is to be removed.
	 * @param id The ID of the food item to remove.
	 */
	public static void removeFoodItem(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("DELETE FROM Food WHERE foodID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Failed to remove food item!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				sql = null;
				db.closeConnection();
				db = null;
			}
		}
	}
	
	/**
	 * Method to approve an Exercise item.
	 * @param id The ID of the Exercise to approve.
	 */
	public static void approveExerciseItem(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("UPDATE exercise SET approved = true WHERE exerciseID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not approve exercise!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				sql = null;
				db.closeConnection();
				db = null;
			}
		}
	}
 	
	/**
	 * Method to remove an Exercise item from the system.
	 * @param id The ID of the Exercise to remove.
	 */
	public static void removeExerciseItem(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("DELETE FROM exercise WHERE exerciseID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Failed to remove exercise!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				sql = null;
				db.closeConnection();
				db = null;
			}
		}
	}
	
	public static ArrayList<SystemUser> getUsers() {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs= null;		
		ArrayList<SystemUser> users = new ArrayList<SystemUser>();
		SystemUser user;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser ORDER BY userID DESC");
			rs = db.runQuery(sql);
			while (rs.next()) {
				user = new SystemUser();
				user.setUserID(rs.getInt("userID"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
				user.setForename(rs.getString("forename"));
				user.setSurname(rs.getString("surname"));
				user.setDateOfBirth(rs.getDate("DoB"));
				user.setSex(rs.getBoolean("sexMale"));
				user.setCounty(rs.getString("county"));
				user.setAdmin(rs.getBoolean("admin"));
				user.setVerified(rs.getBoolean("verified"));
				user.setMeasurementPreference(rs.getString("measurementPreference"));
				
				users.add(user);
			}	
		// Again, need to catch this just in case.
		} catch (SQLException e) {
			// Do nothing, we'll just return an empty ArrayList.
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Could not close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					rs.close();
				} catch (SQLException exc) {
					System.err.println("Could not close ResultSet!");
					System.err.println(exc.getMessage());
				} finally {
					sql = null;
					rs = null;
					db.closeConnection();
					db = null;
				}
			}
		}
		
		return users;
	}
}
