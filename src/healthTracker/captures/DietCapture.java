package healthTracker.captures;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import healthTracker.user.UserActivities;
import healthTracker.utils.DatabaseController;

/**
 * DietCapture.java
 * 
 * Includes methods to add foods and set meals and get them from the database.
 * 
 * @author 6183891, 6266215
 * @version 19/03/2013
 */
public class DietCapture {

	/**
	 * Method to add a brand new food item into the system
	 * 
	 * @param name
	 *            The name of the item
	 * @param cals
	 *            The number of calories it contains
	 * @param description
	 *            The description of the item
	 * @return The ID of the new food item
	 */
	public static int addFoodItem(String name, int cals, String description) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int foodID = 0;

		try {

			// Query to add the new food item
			query = db
					.getConnection()
					.prepareStatement(
							"INSERT INTO Food(name, cals, description, approved) VALUES(?, ?, ?, false) RETURNING foodID");
			query.setString(1, name);
			query.setInt(2, cals);
			query.setString(3, description);
			result = db.runQuery(query);

			// Get the ID of the food item
			while (result.next()) {
				foodID = result.getInt("foodID");
			}
		} catch (SQLException e) {
			System.err.println("Unable to fetch Food ID");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}
		return foodID;
	}

	/**
	 * The user would like to add a food item that they've consumed. Enter it
	 * into the database.
	 * 
	 * @param name
	 *            The name of the food item
	 * @param quantity
	 *            The number they have consumed.
	 * @param cals
	 *            The number of calories involved.
	 * @param description
	 *            A description of what it actually is.
	 * @return Boolean saying whether it was added or not.
	 * @param activityID
	 *            The ID of the current user Activity
	 */
	public static boolean addFoodItemConsumed(int foodID, Date d, int userID,
			int quantity) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int actID = 0;

		// Due to the new table joining user activities and food, this needs to
		// be completed at the same time, using the newly added item
		try {
			query = db.getConnection().prepareStatement(
					"SELECT actID FROM userActivities WHERE actDate = ? AND userID = ?");
			query.setDate(1, d);
			query.setInt(2, userID);
			result = db.runQuery(query);
			if (result.next()) {
				actID = result.getInt("actID");
			}
			if (actID == 0) {
				query = db
						.getConnection()
						.prepareStatement(
								"INSERT INTO userActivities(userID, actDate) VALUES(?, ?) RETURNING actID");
				query.setInt(1, userID);
				query.setDate(2, d);
				result = db.runQuery(query);
				if (result.next()) {
					actID = result.getInt("actID");
				}
				System.err.println("Created activities");
			}
			// We need to check if an ActFood row exists in the database already
			query = db.getConnection().prepareStatement(
					"SELECT actID FROM ActFood WHERE actID = ? AND foodID = ?");
			query.setInt(1, actID);
			query.setInt(2, foodID);
			result = db.runQuery(query);
			// If it exists, they've already consumed the item so simply
			// increase the quantity
			if (result.next()) {
				query = db
						.getConnection()
						.prepareStatement(
								"UPDATE ActFood SET quantity = quantity + ? WHERE actID = ? AND foodID = ?");
				query.setInt(1, quantity);
				query.setInt(2, actID);
				query.setInt(3, foodID);
				db.runUpdate(query);

				// It doesn't exist, so add it into the table
			} else {
				query = db.getConnection().prepareStatement(
						"INSERT INTO ActFood VALUES(?, ?, ?)");
				query.setInt(1, actID);
				query.setInt(2, foodID);
				query.setInt(3, quantity);
				db.runUpdate(query);
			}
		} catch (SQLException e) {
			System.err.println("Unable to insert into ActFood");
		} finally {
			try {
				result.close();
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
		return true;
	}

	/**
	 * Method to add an item to a setmeal
	 * 
	 * @param foodID
	 *            The ID of the food
	 * @param mealID
	 *            The ID of the meal
	 * @param quantity
	 *            The quantity consumed
	 * @param userActivitiesID
	 *            The ID of the UserActivities row in the table
	 * @return
	 */
	public static boolean addItemToSetMeal(int foodID, int mealID,
			int quantity, int userID, Date d) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int actID = 0;

		try {
			query = db.getConnection().prepareStatement(
					"SELECT actID FROM userActivities WHERE actDate = ? AND userID = ?");
			query.setDate(1, d);
			query.setInt(2, userID);
			result = db.runQuery(query);
			if (result.next()) {
				actID = result.getInt("actID");
			}
			if (actID == 0) {
				query = db
						.getConnection()
						.prepareStatement(
								"INSERT INTO userActivities(userID, actDate) VALUES(?, ?) RETURNING actID");
				query.setInt(1, userID);
				query.setDate(2, d);
				result = db.runQuery(query);
				if (result.next()) {
					actID = result.getInt("actID");
				}
				System.err.println("Created activities");
			}
			// We need to check if an ActMeal already exists
			query = db.getConnection().prepareStatement(
					"SELECT actID FROM ActMeal WHERE actID = ? AND mealID = ?");
			query.setInt(1, actID);
			query.setInt(2, mealID);
			result = db.runQuery(query);

			// If it doesn't exist we need to create one, as we're going to use
			// it later
			if (!result.next()) {
				query = db.getConnection().prepareStatement(
						"INSERT INTO ActMeal VALUES(?, ?)");
				query.setInt(1, actID);
				query.setInt(2, mealID);
				db.runUpdate(query);
			}

			// Now we need to check if the mealID exists in FoodInMeal, too.
			query = db
					.getConnection()
					.prepareStatement(
							"SELECT mealID FROM FoodInMeal WHERE mealID = ? AND foodID = ?");
			query.setInt(1, mealID);
			query.setInt(2, foodID);
			result = db.runQuery(query);

			// It doesn't exist, so simply insert it
			if (!result.next()) {
				query = db.getConnection().prepareStatement(
						"INSERT INTO FoodInMeal VALUES(?, ?, ?)");
				query.setInt(1, mealID);
				query.setInt(2, foodID);
				query.setInt(3, quantity);
				db.runUpdate(query);

				// The user has already consumed this item, so simply increase
				// the quantity
			} else {
				query = db
						.getConnection()
						.prepareStatement(
								"UPDATE FoodInMeal SET quantity = quantity + ? WHERE mealID = ? AND foodID = ?");
				query.setInt(1, quantity);
				query.setInt(2, mealID);
				query.setInt(3, foodID);
				db.runUpdate(query);
			}
		} catch (SQLException e) {
			System.err.println("Unable to insert into ActFood");
		} finally {
			try {
				result.close();
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
		return true;
	}

	/**
	 * Method to remove a food item which has been consumed
	 * 
	 * @param foodID
	 *            The ID of the food item
	 * @param day
	 *            The day to delete from
	 */
	public static void removeFoodItem(int foodID, Date day, int userID,
			String mealType) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;

		try {

			// If it's a single snack item, it'll be deleted in this query
			query = db.getConnection().prepareStatement(
					"DELETE FROM ActFood USING UserActivities "
							+ "WHERE ActFood.actID = UserActivities.actID "
							+ "AND actDate = ? " + "AND foodID = ? "
							+ "AND userID = ?");
			query.setDate(1, day);
			query.setInt(2, foodID);
			query.setInt(3, userID);
			db.runUpdate(query);

			// If it's part of a meal, it'll be deleted in this query
			query = db.getConnection().prepareStatement(
					"DELETE FROM FoodInMeal USING UserActivities, ActMeal, SetMeal "
							+ "WHERE FoodInMeal.mealID = ActMeal.mealID "
							+ "AND ActMeal.mealID = SetMeal.mealID "
							+ "AND ActMeal.actID = UserActivities.actID "
							+ "AND actDate = ? " + "AND foodID = ? "
							+ "AND userID = ? " + "AND SetMeal.name = ?");
			query.setDate(1, day);
			query.setInt(2, foodID);
			query.setInt(3, userID);
			query.setString(4, mealType);
			db.runUpdate(query);
		} catch (SQLException e) {
			System.err.println("Could not remove food item!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				query = null;
				db.closeConnection();
				db = null;
			}
		}
	}

	/**
	 * The user would like to add an entire set meal consisting of multiple
	 * ingredients.
	 * 
	 * @param name
	 *            The name of the meal
	 * @param desc
	 *            A description of the meal
	 * @param ingredients
	 *            An array of IDs of all of the ingredients.
	 */
	public static void addSetMeal(String name, String desc, int[] ingredients) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int mealID = 0;

		try {
			query = db
					.getConnection()
					.prepareStatement(
							"INSERT INTO Setmeal(name, description) VALUES(?, ?) RETURNING mealID");
			query.setString(1, name);
			query.setString(2, desc);
			result = db.runQuery(query);
			// This should (in theory) just return 1 row containing the mealID.
			while (result.next()) {
				mealID = result.getInt("mealID");
			}
			// ***THIS NEEDS TO INCLUDE QUANTITY VALUES***
			// For each ingredient we have, let's add it to the database.
			for (int ingredient : ingredients) {
				query = db.getConnection().prepareStatement(
						"INSERT INTO FoodInMeal VALUES(?, ?)");
				query.setInt(1, mealID);
				query.setInt(2, ingredient);
				db.runUpdate(query);
			}
		} catch (SQLException e) {
			System.err.println("Couldn't locate setMeal.");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}
	}

	/**
	 * An ingredient in a set meal is to be added.
	 * 
	 * @param mealID
	 *            The ID of the meal to add it to.
	 * @param ingredient
	 *            The ID of the ingredient to add.
	 * @param quantity
	 *            the amount of each ingredient added
	 */
	public static void addIngredient(int mealID, int ingredient, int quantity) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;

		try {
			query = db.getConnection().prepareStatement(
					"INSERT INTO FoodInMeal VALUES(?, ?, ?)");
			query.setInt(1, mealID);
			query.setInt(2, ingredient);
			query.setInt(3, quantity);
			db.runUpdate(query);
		} catch (SQLException e) {
			System.err.println("Failed to add ingredient!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				query = null;
				db.closeConnection();
				db = null;
			}
		}
	}

	/**
	 * An ingredient in a set meal is to be removed.
	 * 
	 * @param mealID
	 *            The ID of the meal to remove it from.
	 * @param ingredient
	 *            The ID of the ingredient to remove. ingredient = foodID
	 */
	public static void removeIngredient(int mealID, int ingredient) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;

		try {
			query = db
					.getConnection()
					.prepareStatement(
							"DELETE FROM foodinMeal WHERE mealID = ? AND foodid = ?");
			query.setInt(1, mealID);
			query.setInt(2, ingredient);
			db.runUpdate(query);
		} catch (SQLException e) {
			System.err.println("Could not remove ingredient!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				query = null;
				db.closeConnection();
				db = null;
			}
		}
	}

	/**
	 * An entire set meal is to be removed.
	 * 
	 * @param mealID
	 *            The ID of the meal to remove.
	 */
	public static void removeSetMeal(int mealID) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;

		try {
			query = db.getConnection().prepareStatement(
					"DELETE FROM FoodInMeal WHERE mealID = ?");
			query.setInt(1, mealID);
			db.runUpdate(query);
			query = db.getConnection().prepareStatement(
					"DELETE FROM SetMeal WHERE mealID = ?");
			query.setInt(1, mealID);
			db.runUpdate(query);
		} catch (SQLException e) {
			System.err.println("Failed to remove set meal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				query = null;
				db.closeConnection();
				db = null;
			}
		}
	}

	/**
	 * Method to get the food & exercise consumed on a given date
	 * 
	 * @param date
	 *            The day to check
	 * @param userID
	 *            The ID of the user
	 * @return A UserActivities object containing all of the food & exercise
	 */
	public static UserActivities getFoodAndExerciseOnDate(java.util.Date date,
			int userID) {

		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		UserActivities act = new UserActivities(date);

		try {
			// Get all of the individual snack items consumed
			sql = db.getConnection()
					.prepareStatement(
							"SELECT f.foodID, f.name, quantity, description, cals, approved "
									+ "FROM Food AS f, ActFood AS af, UserActivities AS ua "
									+ "WHERE ua.actID = af.actID "
									+ "AND af.foodID = f.foodID "
									+ "AND actDate = ? " + "AND userID = ?");
			Date sqlDate = new Date(date.getTime());
			sql.setDate(1, sqlDate);
			sql.setInt(2, userID);
			result = db.runQuery(sql);

			ArrayList<Food> consumed = new ArrayList<Food>(); // Temporarily
																// hold Food
																// here.
			ArrayList<Exercise> exercised = new ArrayList<Exercise>();

			// Loop through them and add them
			while (result.next()) {
				Food item = new Food();
				item.setFoodID(result.getInt(1));
				item.setName(result.getString(2));
				item.setQuantity(result.getInt(3));
				item.setDescription(result.getString(4));
				item.setCals(result.getInt(5));
				item.setApproved(result.getBoolean(6));
				item.setMeal("s");
				/* Add the created Food to the ArrayList. */
				consumed.add(item);
			}

			// Now we need to get all of the food items that were as part of a
			// meal
			sql = db.getConnection()
					.prepareStatement(
							"SELECT f.foodID, f.name, f.cals, fim.quantity, f.description, sm.name, f.approved "
									+ "FROM Food AS f, FoodInMeal AS fim, SetMeal AS sm, ActMeal as am, UserActivities AS ua "
									+ "WHERE fim.foodID = f.foodID "
									+ "AND am.mealID = fim.mealID "
									+ "AND sm.mealID = am.mealID "
									+ "AND ua.actID = am.actID "
									+ "AND actDate = ? " + "AND userID = ?;");
			sql.setDate(1, sqlDate);
			sql.setInt(2, userID);
			result = db.runQuery(sql);

			// Loop through all of the set meal items
			while (result.next()) {
				Food item = new Food();
				item.setFoodID(result.getInt(1));
				item.setName(result.getString(2));
				item.setQuantity(result.getInt(4));
				item.setDescription(result.getString(5));
				item.setCals(result.getInt(3));
				item.setApproved(result.getBoolean(7));
				item.setMeal(result.getString(6));
				/* Add the created Food to the ArrayList. */
				consumed.add(item);
			}

			sql = db.getConnection()
					.prepareStatement(
							"SELECT exercise.exerciseID, name, calsperunit, description, units, approved"
									+ " FROM exercise,actexercise,useractivities"
									+ " WHERE exercise.exerciseid = actexercise.exerciseid"
									+ " AND actexercise.actid = useractivities.actid"
									+ " AND useractivities.actdate = ?"
									+ " AND useractivities.userID = ?");
			sql.setDate(1, sqlDate);
			sql.setInt(2, userID);

			result = db.runQuery(sql);

			while (result.next()) {
				Exercise exercise;
				int exID = result.getInt("exerciseID");
				String name = result.getString("name");
				String description = result.getString("description");
				int calsPerUnit = result.getInt("calsPerUnit");
				int quantity = result.getInt("units");
				boolean approved = result.getBoolean("approved");

				exercise = new Exercise(exID, calsPerUnit, name, description,
						approved);
				exercise.setQuantity(quantity);

				exercised.add(exercise);
			}

			/* Set the Food consumed if possible. */
			act.setFood(consumed);
			act.setExercise(exercised);
		} catch (SQLException e) {
			System.err
					.println("Could not get food history for specified date!");
			System.err.println(e.getMessage());
			act = null;
		} finally {
			try {
				if (result != null) {
					result.close();
				}
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					sql = null;
				}
			}
		}

		return act;
	}

	/**
	 * Method to get a Food object from the ID entered.
	 * 
	 * @param id
	 *            The ID of the Food item to retrieve from the database.
	 * @return
	 */
	public static Food getFoodFromID(int id) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		Food food = null;

		try {
			query = db.getConnection().prepareStatement(
					"SELECT * FROM Food WHERE foodID = ?");
			query.setInt(1, id);
			result = db.runQuery(query);
			// This should only contain 1 row as all Food items have unique IDs.
			while (result.next()) {
				int foodID = result.getInt("foodID");
				String name = result.getString("name");
				int cals = result.getInt("cals");
				String description = result.getString("description");
				boolean approved = result.getBoolean("approved");

				food = new Food(foodID, cals, name, description, approved);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through food");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return food;
	}

	/**
	 * Method to get all of the food consumed on a single date.
	 * 
	 * @param day
	 *            The day to check
	 * @param userID
	 *            The ID of the user
	 * @return A UserActivities object
	 */
	public static UserActivities getFoodOnDate(Date day, int userID) {

		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		UserActivities act = new UserActivities(day);

		try {

			// Get all of the individual snack items consumed
			sql = db.getConnection()
					.prepareStatement(
							"SELECT f.foodID, f.name, quantity, description, cals, approved "
									+ "FROM Food AS f, ActFood AS af, UserActivities AS ua "
									+ "WHERE ua.actID = af.actID "
									+ "AND af.foodID = f.foodID "
									+ "AND actDate = ? " + "AND userID = ?");
			sql.setDate(1, day);
			sql.setInt(2, userID);
			result = db.runQuery(sql);

			ArrayList<Food> consumed = new ArrayList<Food>(); // Temporarily
																// hold Food
																// here.

			// Loop through them and add them
			while (result.next()) {
				Food item = new Food();
				item.setFoodID(result.getInt(1));
				item.setName(result.getString(2));
				item.setQuantity(result.getInt(3));
				item.setDescription(result.getString(4));
				item.setCals(result.getInt(5));
				item.setApproved(result.getBoolean(6));
				item.setMeal("s");
				/* Add the created Food to the ArrayList. */
				consumed.add(item);
			}

			// Now we need to get all of the food items that were as part of a
			// meal
			sql = db.getConnection()
					.prepareStatement(
							"SELECT f.foodID, f.name, f.cals, fim.quantity, f.description, sm.name, f.approved "
									+ "FROM Food AS f, FoodInMeal AS fim, SetMeal AS sm, ActMeal as am, UserActivities AS ua "
									+ "WHERE fim.foodID = f.foodID "
									+ "AND am.mealID = fim.mealID "
									+ "AND sm.mealID = am.mealID "
									+ "AND ua.actID = am.actID "
									+ "AND actDate = ? " + "AND userID = ?;");
			sql.setDate(1, day);
			sql.setInt(2, userID);
			result = db.runQuery(sql);

			// Loop through all of the set meal items
			while (result.next()) {
				Food item = new Food();
				item.setFoodID(result.getInt(1));
				item.setName(result.getString(2));
				item.setQuantity(result.getInt(4));
				item.setDescription(result.getString(5));
				item.setCals(result.getInt(3));
				item.setApproved(result.getBoolean(7));
				item.setMeal(result.getString(6));
				/* Add the created Food to the ArrayList. */
				consumed.add(item);
			}

			/* Set the Food consumed if possible. */
			act.setFood(consumed);
		} catch (SQLException e) {
			System.err
					.println("Could not get food history for specified date!");
			System.err.println(e.getMessage());
			act = null;
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					sql = null;
				}
			}
		}

		return act;
	}

	/**
	 * Method to get all of the food items in the system (that are approved)
	 * 
	 * @return An ArrayList of food items
	 */
	public static ArrayList<Food> getFoodItems() {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		Food food = null;
		ArrayList<Food> foods = new ArrayList<Food>();

		try {
			query = db.getConnection().prepareStatement(
					"SELECT * FROM Food WHERE approved = true;");
			result = db.runQuery(query);

			// This should only contain 1 row as all Food items have unique IDs.
			while (result.next()) {
				int foodID = result.getInt("foodID");
				String name = result.getString("name");
				int cals = result.getInt("cals");
				String description = result.getString("description");
				boolean approved = result.getBoolean("approved");

				food = new Food(foodID, cals, name, description, approved);

				foods.add(food);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through food");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return foods;
	}

	/**
	 * Searches for approved food items that match the query.
	 * 
	 * @param search
	 *            A String to search for Food items with.
	 * @return An ArrayList of food items
	 */
	public static ArrayList<Food> searchFoodItems(String search) {

		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		Food food = null;
		ArrayList<Food> foods = new ArrayList<Food>();

		try {
			query = db.getConnection().prepareStatement(
					"SELECT * FROM Food WHERE approved = true AND LOWER (name) LIKE ?");
			query.setString(1, "%" + search + "%");
			result = db.runQuery(query);

			// This should only contain 1 row as all Food items have unique IDs.
			while (result.next()) {
				int foodID = result.getInt("foodID");
				String name = result.getString("name");
				int cals = result.getInt("cals");
				String description = result.getString("description");
				boolean approved = result.getBoolean("approved");

				food = new Food(foodID, cals, name, description, approved);

				foods.add(food);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through food");
		} finally {
			try {
				if (result != null) {
					result.close();
				}
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return foods;
	}

	/**
	 * Gets the ID of the appropriate UserActivities ID for the user and date
	 * entered
	 * 
	 * @param userID
	 *            The ID of the user
	 * @param day
	 *            The day requested
	 * @return
	 */
	public static int getUserActivitiesForDate(int userID, Calendar day) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null, result2 = null;
		int userActivitiesID = 0;

		try {
			query = db.getConnection().prepareStatement(
					"SELECT * FROM UserActivities WHERE userID = " + userID
							+ " AND actDate = '" + (day.getTime()) + "';");
			result = db.runQuery(query);
			boolean hasResults = false;

			// This should only contain 1 row.
			while (result.next()) {
				userActivitiesID = result.getInt("actID");
				hasResults = true;
			}

			if (hasResults == false) {
				query = db.getConnection().prepareStatement(
						"INSERT INTO UserActivities (userID, actDate) VALUES ("
								+ userID + ", '" + (day.getTime())
								+ "') RETURNING actID;");
				result2 = db.runQuery(query);

				if (result2.next()) {
					userActivitiesID = result2.getInt("actID");
					result2.close();
				}
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through food");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return userActivitiesID;
	}

	/**
	 * Get the SetMeal ID for the details entered. Creates a new one if there
	 * isn't one.
	 * 
	 * @param userActivitiesID
	 *            The ID of the user activities
	 * @param mealType
	 *            The type of meal
	 * @return The SetMeal ID
	 */
	public static int getSetMealForDetails(int userActivitiesID, String mealType) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null, result2 = null;
		int setMealID = 0;

		try {

			// Query to check if a setmeal already exists
			query = db
					.getConnection()
					.prepareStatement(
							"SELECT mealID FROM SetMeal WHERE name = ? AND mealID IN (SELECT mealID FROM ActMeal WHERE actID = ?);");
			query.setString(1, mealType);
			query.setInt(2, userActivitiesID);
			result = db.runQuery(query);

			// If this is true, the setmeal already exists
			if (result.next()) {
				setMealID = result.getInt("mealID");

				// It must be false, so we need to create a new setmeal
			} else {

				// Add the new setmeal
				query = db
						.getConnection()
						.prepareStatement(
								"INSERT INTO SetMeal (name, description) VALUES (?, ?) RETURNING mealID;");
				query.setString(1, mealType);
				query.setString(2, mealType);
				result2 = db.runQuery(query);

				// Get the ID of it and return it
				if (result2.next()) {
					setMealID = result2.getInt("mealID");
					result2.close();
				}
			}

		} catch (SQLException e) {
			System.err.println("Unable to loop through food");
			e.printStackTrace();
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return setMealID;
	}

}