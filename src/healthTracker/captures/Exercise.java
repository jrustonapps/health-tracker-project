package healthTracker.captures;

/**
 * Exercise.java
 * 
 * Stores an exercise ID, the number of calories per unit as well as the name of the
 * Exercise, the description and whether it has been approved or not.
 * 
 * @author 6183891
 */
public class Exercise {
        
        protected int exID, calsPerUnit;
        private float quantity;
        protected String name, description;
        protected boolean approved;
        
        /**
         * Default constructor to create an Exercise object
         */
        public Exercise() {
                exID = 0;
                calsPerUnit = 0;
                quantity = 0;
                name = "";
                description = "";
                approved = false;
        }
        
        /**
         * Constructor to create Exercise item with all relevant data.
         * @param exID The ID of the exercise item
         * @param calsPerUnit The number of calories per unit of exercise
         * @param name The name of the exercise item
         * @param description The description of the exercise
         * @param approved Boolean value saying whether it has been approved or not
         */
        public Exercise(int exID, int calsPerUnit, String name,
                        String description, 
                        boolean approved) {
                this.exID = exID;
                this.calsPerUnit = calsPerUnit;
                this.name = name;
                this.description = description;
                this.approved = approved;
                this.quantity = 0;
        }
        
        /**
         * Method to get the exerciseID.
         * @return ID of the exercise.
         */
        public int getExID() {
                return exID;
        }
        
        /**
         * Method to get the number of calories per unit
         * @return Number of calories
         */
        public int getCalsPerUnit() {
                return calsPerUnit;
        }
        
        /**
         * Method to get the name of the Exercise.
         * @return Name of Exercise.
         */
        public String getName() {
                return name;
        }
        
        /**
         * Method to get the description of the Exercise.
         * @return Description of Exercise.
         */
        public String getDescription() {
                return description;
        }
        
        /**
         * Method to find out whether the Exercise is approved or not.
         * @return Boolean stating if it's approved.
         */
        public boolean getApproved() {
                return approved;
        }
        
        /**
         * Method to set the exerciseID.
         * @param exID The ID of the Exercise.
         */
        public void setExID(int exID) {
                this.exID = exID;
        }
        
        /**
         * Method to set the calories per unit.
         * @param calsPerUnit The number of calories.
         */
        public void setCalsPerUnit(int calsPerUnit) {
                this.calsPerUnit = calsPerUnit;
        }
        
        /**
         * Method to set the name of the Exercise.
         * @param name
         */
        public void setName(String name) {
                this.name = name;
        }
        
        /**
         * Method to set the description of the Exercise.
         * @param description The description.
         */
        public void setDescription(String description) {
                this.description = description;
        }
        
        /**
         * Method to set whether the Exercise is approved or not.
         * @param approved The status of the Exercise.
         */
        public void setApproved(boolean approved) {
                this.approved = approved;
        }
        
        public float getQuantity() {
        	return this.quantity;
        }
        
        public void setQuantity(float quantity) {
        	this.quantity = quantity;
        }
}