package healthTracker.captures;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import healthTracker.captures.Exercise;
import healthTracker.utils.DatabaseController;

/**
 * ExerciseCapture.java
 * 
 * Includes methods to add exercises and to get an Exercise from the database.
 * 
 * @author 6183891, 6266215, 6102581
 * @version 19/03/2013
 */
public class ExerciseCapture {

	/**
	 * Method to add an Exercise item into the system.
	 * 
	 * @param name The name of the exercise.
	 * @param description The description of what the Exercise is
	 * @param calsPerUnit The number of calories per unit of exercise
	 * @param units The units in which the Exercise is measured in
	 * @param approved Whether it has been approved by an admin or not.
	 * @return Boolean stating whether it was added or not
	 */
	public static boolean addExerciseItem(String name, String description,
			int calsPerUnit, boolean approved) {

		PreparedStatement sql = null;
		DatabaseController db = new DatabaseController();

		try {
			sql = db.getConnection().prepareStatement("INSERT INTO exercise(name, calsperunit, description, approved) VALUES(?, ?, ?, ?)");
			sql.setString(1, name);
			sql.setInt(2, calsPerUnit);
			sql.setString(3, description);
			sql.setBoolean(4, false);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not add exercise item!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				db = null;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param date The date for which exercises should be fetched
	 * @param Userid The id of the user for which the exercises are being fetched
	 * @return
	 */
	public static ArrayList<Exercise> getExerciseOnDate(Date date, int Userid) {

		PreparedStatement sql = null;
		DatabaseController db = new DatabaseController();
		ResultSet rs = null;
		ArrayList<Exercise> exercise = new ArrayList<>();

		try {
			sql = db.getConnection().prepareStatement("SELECT useractivities.actID, exercise.exerciseID, name, calsperunit, description, units"
					+ " FROM exercise,actexercise,useractivities"
					+ " WHERE exercise.exerciseid = actexercise.exerciseid"
					+ " AND actexercise.actid = useractivities.actid"
					+ " AND useractivities.actdate = ?"
					+ " AND useractivities.userID = ?");
			sql.setDate(1, date);
			sql.setInt(2, Userid);

			rs = db.runQuery(sql);
			while(rs.next()) {
				Exercise ex = new Exercise();
				ex.setExID(rs.getInt("exerciseID"));
				ex.setName(rs.getString("name"));
				ex.setDescription(rs.getString("description"));
				ex.setCalsPerUnit(rs.getInt("calsPerUnit"));
				ex.setQuantity(rs.getFloat("units"));
				exercise.add(ex);
			}

		} catch (SQLException e) {
			System.err.println("Could not get exercise history for specified date");
			System.err.println(e.getMessage());
			return null;
		} finally {
			db.closeConnection();
			db = null;
			sql = null;
		}
		
		return exercise;
	}

	/**
	 * Removes an exercise from a users activities.
	 * @param actID The ID to remove
	 */
	public static void removeExerciseWithID(Date d,int exID) {
		PreparedStatement sql = null;
		DatabaseController db = new DatabaseController();
		ResultSet result = null;
		int actIDFound = 0;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT actID FROM userActivities WHERE actDate = ?");
			sql.setDate(1, d);	
			result = db.runQuery(sql);
			if (result.next()) {
				actIDFound = result.getInt("actID");
			}
			if (actIDFound != 0) {
				sql = db.getConnection().prepareStatement("DELETE FROM actexercise WHERE exerciseID = ? AND actID = ?");
				sql.setInt(1, exID);
				sql.setInt(2, actIDFound);
				db.runUpdate(sql);
			}

		} catch (SQLException e) {
			System.err.println("Failed to remove exercise!");
			System.err.println(e.getMessage());
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					sql = null;
				}
			}
		}
	}

	/**
	 * Creates an arrayList of exercises the user can potentially pick from
	 * 
	 * @return
	 */
	public static ArrayList<Exercise> getAllApproved() {
		
		PreparedStatement sql = null;
		DatabaseController db = new DatabaseController();
		ResultSet result = null;
		ArrayList<Exercise> exerciseList = new ArrayList<Exercise>();

		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM exercise WHERE approved = true");
			Exercise exercise = null;
			result = db.runQuery(sql);

			while (result.next()) {
				int exID = result.getInt("exerciseID");
				String name = result.getString("name");
				String description = result.getString("description");
				int calsPerUnit = result.getInt("calsPerUnit");
				boolean approved = result.getBoolean("approved");

				exercise = new Exercise(exID, calsPerUnit, name, description, approved);
				exerciseList.add(exercise);
			}

		} catch (SQLException e) {
			System.err.println("Could not get approved exercises!");
			System.err.println(e.getMessage());
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					sql = null;
				}
			}
		}
		return exerciseList;
	}
	
	/**
	 * Searches for any exercises with a similar name to the provided query. Only
	 * returns them if they are approved.
	 * @param query The exercise to search for as a String.
	 * @return An ArrayList of Exercises matching the query.
	 */
	public static ArrayList<Exercise> searchApproved(String query) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		ArrayList<Exercise> exercises = new ArrayList<>();

		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM exercise WHERE LOWER(name) LIKE ? " +
					"AND approved = true");
			sql.setString(1, "%"+query+"%");
			result = db.runQuery(sql);
			
			while (result.next()) {
				int exID = result.getInt("exerciseID");
				String name = result.getString("name");
				String description = result.getString("description");
				int calsPerUnit = result.getInt("calsPerUnit");
				boolean approved = result.getBoolean("approved");

				exercises.add(new Exercise(exID, calsPerUnit, name, description, approved));
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through exercise");
			System.err.println(e.getMessage());
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					sql = null;
				}
			}
		}

		return exercises;
	}

	/**
	 * Method to get an Exercise object from an ID specified.
	 * 
	 * @param id The ID of the Exercise to retrieve.
	 * @return An Exercise object
	 */
	public static Exercise getExerciseFromID(int id) {

		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		Exercise exercise = null;

		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM exercise WHERE exerciseID = ?");
			sql.setInt(1, id);
			result = db.runQuery(sql);
			// This should only have 1 row, containing the relevant info for our
			// object.
			while (result.next()) {
				int exID = result.getInt("exerciseID");
				String name = result.getString("name");
				String description = result.getString("description");
				int calsPerUnit = result.getInt("calsPerUnit");
				boolean approved = result.getBoolean("approved");

				exercise = new Exercise(exID, calsPerUnit, name, description, approved);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through exercise");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					sql = null;
				}
			}
		}

		return exercise;
	}
	
	/**
	 * Adds a performed exercise for the user.
	 * @param actID The activities of the user to modify.
	 * @param exID The exercise to add.
	 * @param units The amount of exercise done.
	 */
	public static void addPerformedExercise(Date d, int userID, int exID, float units) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		int actID = 0;

		try {
			sql = db.getConnection().prepareStatement("SELECT actID FROM userActivities WHERE actDate = ? AND userID = ?");
			sql.setDate(1, d);	
			sql.setInt(2,  userID);
			result = db.runQuery(sql);
			if (result.next()) {
				actID = result.getInt("actID");
			}
			if (actID == 0) {
				sql = db.getConnection().prepareStatement("INSERT INTO userActivities(userID, actDate) VALUES(?, ?) RETURNING actID");
				sql.setInt(1, userID);
				sql.setDate(2, d);
				result = db.runQuery(sql);
				if (result.next()) {
					actID = result.getInt("actID");
				}
			}
			sql = db.getConnection().prepareStatement("SELECT actID FROM actExercise WHERE actID = ? AND exerciseID = ?");
			sql.setInt(1, actID);
			sql.setInt(2, exID);
			result = db.runQuery(sql);

			if (result.next()) {
				sql = db.getConnection().prepareStatement("UPDATE actExercise SET units = units + ? WHERE actID = ? AND exerciseID = ?");
				sql.setFloat(1, units);
				sql.setInt(2, actID);
				sql.setInt(3, exID);
				db.runUpdate(sql);
			} else {
				sql = db.getConnection().prepareStatement("INSERT INTO actExercise VALUES(?, ?, ?)");
				sql.setInt(1, actID);
				sql.setInt(2, exID);
				sql.setFloat(3, units);
				db.runUpdate(sql);
			}
		} catch (SQLException e) {
			System.err.println("Could not add performed exercise!");
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					sql = null;
					result = null;
				}
			}
		}
	}
}