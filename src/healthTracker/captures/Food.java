package healthTracker.captures;

/**
 * Food.java
 * 
 * Stores the ID of the food, the quantity consumed and the number of calories it
 * Contains. Also stores the name, description and whether it has been approved or not.
 * 
 * @author 6183891
 */
public class Food {
        
        protected int foodID, quantity, cals;
        protected String name, description, meal;
        protected boolean approved;
        
        /**
         * Default constructor to create a Food object
         */
        public Food() {
                foodID = 0;
                cals = 0;
                name = "";
                description = "";
                meal = "";
                approved = false;
        }
        
        /**
         * Constructor to create a Food item with all required fields.
         * @param foodID The ID of the Food item
         * @param cals The number of calories it contains
         * @param name The name of the food
         * @param description A description of what the food item is
         * @param approved If it's been approved by an administrator or not
         */
        public Food(int foodID, int cals, String name,
                        String description, boolean approved) {
                this.foodID = foodID;
                this.cals = cals;
                this.name = name;
                this.description = description;
                this.approved = approved;
                this.meal = "";
        }
        
        /**
         * Method to get the food ID
         * @return The food ID
         */
        public int getFoodID() {
                return foodID;
        }
        
        /**
         * Method to get the number of calories
         * @return Number of calories
         */
        public int getCals() {
                return cals;
        }
        
        /**
         * Method to get the name of the Food
         * @return Name of the Food
         */
        public String getName() {
                return name;
        }
        
        /**
         * Method to get the description of the food
         * @return The description
         */
        public String getDescription() {
                return description;
        }
        
        /**
         * Method to get whether the food is approved or not
         * @return Boolean stating the status of the food item
         */
        public boolean getApproved() {
                return approved;
        }
        
        /**
         * Method to set the food ID
         * @param foodID The ID of the food
         */
        public void setFoodID(int foodID) {
                this.foodID = foodID;
        }
        
        /**
         * Method to set the number of calories
         * @param cals Number of calories
         */
        public void setCals(int cals) {
                this.cals = cals;
        }
        
        /**
         * Method to set the name of the food
         * @param name The name
         */
        public void setName(String name) {
                this.name = name;
        }
        
        /**
         * Method to set the description of the food
         * @param description The description
         */
        public void setDescription(String description) {
                this.description = description;
        }
        
        /**
         * Method to set whether the food is approved or not
         * @param approved Boolean stating if it's approved
         */
        public void setApproved(boolean approved) {
                this.approved = approved;
        }
        
        /**
         * Gets the quantity of this Food.
         * @return The quantity as an integer.
         */
        public int getQuantity() {
                
                return this.quantity;
        }
        
        /**
         * Sets the quantity of this Food that was consumed.
         * @param q The new quantity as an integer.
         */
        public void setQuantity(int q) {
                
                this.quantity = q;
        }
        
        /**
         * Gets the type of meal this food is part of
         * @return
         */
        public String getMeal() {
        	return this.meal;
        }
        
        /**
         * Sets the type of meal this food is part of
         * @param meal
         */
        public void setMeal(String meal) {
        	this.meal = meal;
        }

}