package healthTracker.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.lang.String;

import healthTracker.goal.Goal;
import healthTracker.utils.DatabaseController;
import healthTracker.utils.Security;
import healthTracker.user.SystemUser;

/**
 * UserManager.java
 * 
 * Contains static methods to deal with users in the database - creating a new
 * user, checking if their login details are correct as well as removing them from
 * the system.
 * 
 * @author 6183891, 6266215
 * @version 19/03/2013
 */
public class UserManager {
	
	/**
	 * Method to create a brand new user and add it to the database.
	 * @param email The e-mail address of the user.
	 * @param password The password the user would like to use to login.
	 * @param forename The first name of the user.
	 * @param surname The surname of the user.
	 * @param county The county in which they live in.
	 * @param dateOfBirth Their date of birth (as a Date)
	 * @param sex Their gender (true = male, false = female)
	 * @param weight Their current weight as a double
	 * @param height Their current height as a double
	 * @return true if entry was successful, false if not.
	 */
	public static boolean createUser(String email, String password, 
			String forename, String surname,
			String county, Calendar dateOfBirth, boolean sex) {
		
		String shaPass = Security.hashSHA1(password);
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			/* Attempt to create the PreparedStatement we need. */
			sql = db.getConnection().prepareStatement(
				"INSERT INTO SystemUser(email,password,forename,surname,dob,sexmale,county,admin,verified,measurementPreference) "
				+ "VALUES (? ,? ,? ,? ,? ,? ,? ,false ,false ,'m')"
			);
			/* Then try and set all the parameters appropriately. */
			java.sql.Date sqlDate = new java.sql.Date(dateOfBirth.getTime().getTime());
			sql.setString(1, email);
			sql.setString(2, shaPass);
			sql.setString(3, forename);
			sql.setString(4, surname);
			sql.setDate(5, sqlDate);
			sql.setBoolean(6, sex);
			sql.setString(7, county);
			/* Run this SQL query. */
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not persist new user!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
			}
		}
		
		return true;
	}
	
	/**
	 * Method to change a user's details to the supplied parameters.
	 * @param userID The ID of the user. This cannot be changed.
	 * @param email The e-mail address of the user.
	 * @param password The password the user would like to use to login.
	 * @param forename The first name of the user.
	 * @param surname The surname of the user.
	 * @param county The county in which they live in.
	 * @param dateOfBirth Their date of birth (as a Date)
	 * @param sex Their gender (true = male, false = female)
	 */
	public static void updateUserDetails(int userID, String email, String password,
			String forename, String surname, String county, Calendar dateOfBirth,
			boolean sex) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement(
				"UPDATE SystemUser SET email = ?, password = ?, forename = ?, surname = ?, dob = ?, county = ?, sexmale = ? "
				+ "WHERE userID = ?"
			);	
			java.sql.Date sqlDate = new java.sql.Date(dateOfBirth.getTime().getTime());
			sql.setString(1, email);
			sql.setString(2, password);
			sql.setString(3, forename);
			sql.setString(4, surname);
			sql.setDate(5, sqlDate);
			sql.setString(6, county);
			sql.setBoolean(7, sex);
			sql.setInt(8, userID);
			/* Run this SQL query. */
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not update User!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
			}
		}
	}
	
	/**
	 * Method to update the type of measurement the user prefers.
	 * m = metric, i = imperial, s = Scott Grandisons
	 * @param userID The ID of the user
	 * @param measurement The type of measurement
	 */
	public static void updateMeasurementPreference(int userID, String measurement) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement(
				"UPDATE SystemUser SET measurementPreference = ? "
				+ "WHERE userID = ?"
			);
			sql.setString(1, measurement);
			sql.setInt(2, userID);
			/* Run this SQL query. */
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not set user measurement preference!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
			}
		}
	}
	
	/**
	 * Gets a list of all the previous weights for the specified user.
	 * @param userID The User ID of the user to get the history of.
	 * @param limit The number of history items to get.
	 * @return An ArrayList of doubles. 
	 */
	public static ArrayList<Double> getPreviousWeights(int userID, int limit) {
		
		DatabaseController db = new DatabaseController();
		/* Prepare a ResultSet and PreparedStatement. */
		ResultSet result = null;
		PreparedStatement sql = null;
		try {
			sql = db.getConnection().prepareStatement(
				"SELECT weight FROM UserHistory WHERE userID = ? ORDER BY histDate DESC LIMIT ? "
			);
			sql.setInt(1, userID);
			sql.setInt(2, limit);
			/* Run this SQL query. */
			result = db.runQuery(sql);
		} catch (SQLException e) {
			System.err.println("Failed to retrieve previous user weights!");
			System.err.println(e.getMessage());
		}
		/* Create an ArrayList to move things into. */
		ArrayList<Double> previousWeights = new ArrayList<Double>();
		
		try {
			while (result.next()) {
				previousWeights.add(result.getDouble("weight"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					sql = null;
				}
			}
		}
		
		return previousWeights;
	}
	
	/**
	 * Method to check whether the username and password entered exist in the database.
	 * @param username The username to check.
	 * @param password The password to check.
	 * @return
	 */
	public static boolean checkCredentials(String username, String password) {
		
		String shaPass = Security.hashSHA1(password);
		
		DatabaseController db = new DatabaseController();
		ResultSet result = null;
		PreparedStatement sql = null;
		
		boolean answer = false;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE email LIKE ? AND password = ?");
			sql.setString(1, username);
			sql.setString(2, shaPass);
			result = db.runQuery(sql);
			// If we actually have results, the user must exist. So we can return true.
			if (result.next()) {
				answer = true;
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through users");
			e.printStackTrace();
		} finally {
			try {
				result.close();
			} catch (SQLException e) {
				System.err.println("Could not close ResultSet!");
				System.err.println(e.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException ex) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(ex.getMessage());
				} finally {
					db.closeConnection();
					result = null;
					sql = null;
					db = null;
				}
			}
		}
		
		return answer;
	}
	
	/**
	 * Checks whether the given user has entered their weight + height before
	 * @param id The ID of the user
	 * @return True if the user has entered the information, false otherwise.
	 */
	public static boolean hasEnteredDiet(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		int numberOfRows = 0;
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM UserHistory WHERE userID = ?");
			sql.setInt(1, id);	
			numberOfRows = db.numRows(sql);
		} catch (SQLException e) {
			System.err.println("Could not find user.");
			e.printStackTrace();
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
		
		if (numberOfRows>0) {
			return true;
		} else {
			return false;
		}		
	}
	
	/**
	 * Gets the last history record for the specified user.
	 * @param id The User ID as an integer.
	 * @return A single UserHistory reference.
	 */
	public static UserHistory getLastHistory(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs = null;
		UserHistory userHistory = null;
		
		try {
			/* Set up the SQL query properly. */
			sql = db.getConnection().prepareStatement("SELECT * FROM UserHistory WHERE userID = ? "
					+ "ORDER BY histDate DESC LIMIT 1");
			sql.setInt(1, id); // Set the UserID to search for.
			rs = db.runQuery(sql); // Run the query.
			
			if (rs != null) {
				try {
					if(rs.next()) {
						userHistory = new UserHistory();
						userHistory.setUserID(rs.getInt("userID"));
						userHistory.setHeight(rs.getDouble("height"));
						userHistory.setWeight(rs.getDouble("weight"));
						userHistory.setDate(rs.getDate("histDate"));
					}
				} catch (SQLException e) {
					System.err.println("Failed to get results!");
					e.printStackTrace();
					userHistory = null;
				}				
			}
		} catch (SQLException e) {
			System.err.println("Could not close ResultSet!");
			System.err.println(e.getMessage());
		} finally {
			try {
				rs.close();
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				rs = null;
				db = null;
			}
		}
		return userHistory;
	}

	/**
	 * Method to remove a user from the database.
	 * @param id The unique user ID of the user to be deleted.
	 */
	public static void removeUser(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("DELETE FROM SystemUser WHERE userID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Failed to delete user!");
			e.printStackTrace();
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				db = null;
			}
		}
	}
	
	/**
	 * Checks if the email provided already exists in the database.
	 * @param email The email to check as a String.
	 * @return Returns true if the email is found, false otherwise.
	 */
	public static boolean doesEmailExist(String email) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		boolean answer = false;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE email = ?");
			sql.setString(1, email);
			result = db.runQuery(sql);
			
			try {
				while (result.next()) {
					answer = true;
				}
			} catch (SQLException e) {
				System.err.println("Failed to get results!");
				e.printStackTrace();
			}
		} catch (SQLException ex) {
			System.err.println("Could not run query!");
			System.err.println(ex.getMessage());			
		} finally {
			try {
				result.close();
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				result = null;
				db = null;
			}
		}
				
		return answer;
	}
	
	/**
	 * Verifies the user specified is allowed to use the system.
	 * @param userID The user ID of the user to authenticate.
	 */
	public static void verifyUser(int userID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("UPDATE SystemUser SET verified = true WHERE userID = ?");
			sql.setInt(1, userID);
			db.runUpdate(sql);
		} catch (SQLException ex) {
			System.err.println("Could not verify user account!");
			System.err.println(ex.getMessage());			
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				db = null;
			}
		}
	}
	
	/**
	 * Method to set the current weight + height
	 * @param userID The user ID to update
	 * @param height The height of the user
	 * @param weight The weight of the user
	 */
	@SuppressWarnings("resource")
	public static void updateInitialWeightAndHeight(int userID, double height, double weight) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			// Check if the user has any history entries.
			sql = db.getConnection().prepareStatement("SELECT * FROM UserHistory WHERE userID = ? AND histDate = CURRENT_DATE");
			sql.setInt(1, userID);
			int numberOfRows = db.numRows(sql);
			
			// If they haven't entered their details today, insert a new row
			if (numberOfRows==0) {
				sql = db.getConnection().prepareStatement("INSERT INTO UserHistory VALUES(?, CURRENT_DATE, ?, ?)");
				sql.setInt(1, userID);
				sql.setDouble(2, weight);
				sql.setDouble(3, height);
				db.runUpdate(sql);				
			// If they have, simply update the previous details
			} else {
				sql = db.getConnection().prepareStatement("UPDATE UserHistory SET weight = ?, height = ? "
						+ "WHERE userId = ? AND histdate = CURRENT_DATE");
				sql.setDouble(1, weight);
				sql.setDouble(2, height);
				sql.setInt(3, userID);
				db.runUpdate(sql);
			}
		} catch (SQLException ex) {
			System.err.println("Failed to store initial height/ weight readings.");
			System.err.println(ex.getMessage());			
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				db = null;
			}
		}
	}
	
	/**
	 * Method to get a User object using only a user's unique ID.
	 * @param id The user's ID.
	 * @return User object containing all of the user's details.
	 */
	public static SystemUser getUserFromID(int id) {
		
		/* Create some objects to work with. */
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs = null;
		SystemUser user = null; // This has the chance of being null.
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE userID = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
				
			if (rs != null) {			
				try {
					if(rs.next()) {
						user = new SystemUser();
						user.setUserID(rs.getInt("userID"));
						user.setPassword(rs.getString("password"));
						user.setEmail(rs.getString("email"));
						user.setForename(rs.getString("forename"));
						user.setSurname(rs.getString("surname"));
						user.setDateOfBirth(rs.getDate("DoB"));
						user.setSex(rs.getBoolean("sexMale"));
						user.setCounty(rs.getString("county"));
						user.setAdmin(rs.getBoolean("admin"));
						user.setVerified(rs.getBoolean("verified"));
						user.setMeasurementPreference(rs.getString("measurementPreference"));
					}
				} catch (SQLException e) {
					System.err.println("Failed to get results!");
					e.printStackTrace();
					user = null;
				}				
			}
		} catch (SQLException e) {
			System.err.println("Could not retrieve user details.");
			System.err.println(e.getMessage());
		} finally {
			try {
				rs.close();
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				rs = null;
				db = null;
			}
		}
		/* Return the user details. */
		return user;
	}
	
	/**
	 * Finds an existing user with the specified email address.
	 * @param email The email address to find.
	 * @return A SystemUser if a user can be found, null otherwise.
	 */
	public static SystemUser findUserByEmail(String email) {
		
		/* Create some objects to work with. */
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs = null;
		SystemUser user = null; // This has the chance of being null.
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE email LIKE ?");
			sql.setString(1, email);
			rs = db.runQuery(sql);
				
			if (rs != null) {			
				try {
					if(rs.next()) {
						user = new SystemUser();
						user.setUserID(rs.getInt("userID"));
						user.setPassword(rs.getString("password"));
						user.setEmail(rs.getString("email"));
						user.setForename(rs.getString("forename"));
						user.setSurname(rs.getString("surname"));
						user.setDateOfBirth(rs.getDate("DoB"));
						user.setSex(rs.getBoolean("sexMale"));
						user.setCounty(rs.getString("county"));
						user.setAdmin(rs.getBoolean("admin"));
						user.setVerified(rs.getBoolean("verified"));
						user.setMeasurementPreference(rs.getString("measurementPreference"));
					}
				} catch (SQLException e) {
					System.err.println("Failed to get results!");
					e.printStackTrace();
					user = null;
				}				
			}
		} catch (SQLException e) {
			System.err.println("Could not retrieve user details.");
			System.err.println(e.getMessage());
		} finally {
			try {
				rs.close();
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				rs = null;
				db = null;
			}
		}
		/* Return the user details. */
		return user;
	}
	
	public static ArrayList<Date> getDatesLastEnteredInUserActivities(int userID, int limit) {
		/* Create some objects to work with. */
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs = null;
		ArrayList<Date> dates = new ArrayList<Date>();
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM UserActivities WHERE userID = ? ORDER BY actDate DESC LIMIT ?");
			sql.setInt(1, userID);
			sql.setInt(2, limit);
			rs = db.runQuery(sql);
				
			if (rs != null) {			
				try {
					while(rs.next()) {
						dates.add(rs.getDate("actDate"));
					}
				} catch (SQLException e) {
					System.err.println("Failed to get results!");
					e.printStackTrace();
				}				
			}
		} catch (SQLException e) {
			System.err.println("Could not retrieve user activities details.");
			System.err.println(e.getMessage());
		} finally {
			try {
				rs.close();
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				rs = null;
				db = null;
			}
		}
		/* Return the user details. */
		return dates;
	}
	
	public static ArrayList<SystemUser> getUsersFromGoals(ArrayList<Goal> goals) {
		
		/* Create some objects to work with. */
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs = null;
		SystemUser user = null; // This has the chance of being null.
		ArrayList<SystemUser> users = new ArrayList<>();
		
		try {
			
			for (Goal g : goals) {
				sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE userID IN (SELECT userID FROM Goal WHERE goalID = ?);");
				sql.setInt(1, g.getId());
				rs = db.runQuery(sql);
					
				if (rs != null) {			
					try {
						if(rs.next()) {
							user = new SystemUser();
							user.setUserID(rs.getInt("userID"));
							user.setPassword(rs.getString("password"));
							user.setEmail(rs.getString("email"));
							user.setForename(rs.getString("forename"));
							user.setSurname(rs.getString("surname"));
							user.setDateOfBirth(rs.getDate("DoB"));
							user.setSex(rs.getBoolean("sexMale"));
							user.setCounty(rs.getString("county"));
							user.setAdmin(rs.getBoolean("admin"));
							user.setVerified(rs.getBoolean("verified"));
							user.setMeasurementPreference(rs.getString("measurementPreference"));
							
							users.add(user);
						}
					} catch (SQLException e) {
						System.err.println("Failed to get results!");
						e.printStackTrace();
						user = null;
					}				
				}
			}
	
		} catch (SQLException e) {
			System.err.println("Could not retrieve user details.");
			System.err.println(e.getMessage());
		} finally {
			try {
				rs.close();
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close DB Assests safely!");
				System.err.println(ex.getMessage());
			} finally {
				db.closeConnection();
				sql = null;
				rs = null;
				db = null;
			}
		}
		/* Return the user details. */
		return users;
	}
}
