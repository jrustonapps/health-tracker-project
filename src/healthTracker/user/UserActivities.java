package healthTracker.user;

import healthTracker.captures.Exercise;
import healthTracker.captures.Food;

import java.util.ArrayList;
import java.util.Date;

/**
 * UserActivities.java
 * 
 * Stores a date of which the activity occurred.
 * 
 * @author 6183891
 * @version 20/03/2013
 */
public class UserActivities {
	
	protected Date date;
	protected ArrayList<Food> food;
	protected ArrayList<Exercise> exercise;
	
	/**
	 * Empty constructor to create a UserActivities object
	 */
	public UserActivities() {
		date = new Date();
		food = new ArrayList<Food>();
		exercise = new ArrayList<Exercise>();
	}
	
	/**
	 * Constructor to create a UserActivities object with an associated date
	 * @param date
	 */
	public UserActivities(Date date) {
		this.date = date;
	}

	/**
	 * Method to get the date
	 * @return The date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Method to set the date
	 * @param date The date
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Get a list of all the Food consumed on this day.
	 * @return An ArrayList of Food objects.
	 */
	public ArrayList<Food> getFood() {
		
		return this.food;
	}
	
	/**
	 * Set the list of food consumed on this day.
	 * @param foods An ArrayList of Food objects.
	 */
	public void setFood(ArrayList<Food> foods) {
		
		this.food = foods;
	}
	
	
	public ArrayList<Exercise> getExercise() {
		
		return this.exercise;
	}
	
	
	public void setExercise(ArrayList<Exercise> exercises) {
		
		this.exercise = exercises;
	}
	
}
