package healthTracker.user;

import java.util.Date;

/**
 * UserHistory.java
 * 
 * Stores a date of which the details were measured, as well as the height and weight
 * Of the user. The ID of the user is also stored.
 * 
 * @author 6183891
 */
public class UserHistory {
	
	protected Date date;
	protected double weight, height;
	protected int userID;
	
	/**
	 * Empty constructor to create a UserHistory object
	 */
	public UserHistory() {
		date = new Date();
		weight = 0.0;
		height = 0.0;
		userID = 0;
	}
	
	/**
	 * Constructor to create a UserHistory object with all required fields completed.
	 * @param userID The ID of the user
	 * @param date The date of which it was measured
	 * @param weight The weight of the user
	 * @param height The height of the user
	 */
	public UserHistory(int userID,Date date, double weight, double height) {
		this.date = date;
		this.weight = weight;
		this.height = height;
		this.userID = userID;
	}
	
	/**
	 * Method to get the date
	 * @return The date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Method to get the weight
	 * @return The weight
	 */
	public double getWeight() {
		return weight;
	}
	
	/**
	 * Method to get the height
	 * @return The height
	 */
	public double getHeight() {
		return height;
	}
	
	/**
	 * Method to get the ID of the user
	 * @return The ID of the user
	 */
	public int getUserID(){
		return userID;
	}
	
	/**
	 * Method to set the date
	 * @param date The date of which it was measured
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Method to set the weight
	 * @param weight The weight
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	/**
	 * Method to set the height
	 * @param height The height
	 */
	public void setHeight(double height) {
		this.height = height;
	}
	
	/**
	 * Method to set the ID of the user
	 * @param newID The ID of the user
	 */
	public void setUserID(int userID){
		this.userID = userID;
	}

}
