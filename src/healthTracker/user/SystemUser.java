package healthTracker.user;

import java.util.Date;

/**
 * User.java
 * 
 * Stores a user's information - including their ID, login details, name and current
 * height + weight info. Contains accessor and setter methods for all fields, including
 * a method to calculate the current BMI of the user.
 * 
 * @author 6183891
 */
public class SystemUser {
	
	protected int userID;
	protected String email, password, forename, surname, county, measurementPreference;
	protected Date dateOfBirth;
	protected boolean sexMale, admin, verified;

	/**
	 * Creates a new user with empty details.
	 */
	public SystemUser() {
		userID = 0;
		password = "";
		email = "";
		forename = "";
		surname = "";
		measurementPreference = "m";
		dateOfBirth = new Date();
		sexMale = false;
		admin = false;
		verified = false;
	}
	
	/**
	 * Creates a new user with the specified details held.
	 * @param userID A unique ID for the user as an integer.
	 * @param username The username for this user as a String.
	 * @param password The password for this user as a String.
	 * @param email The email address of this user as a String.
	 * @param forename The users' forename as a String.
	 * @param surname The users' surname as a String.
	 * @param county The county this user lives in as a String.
	 * @param dateOfBirth The users' date of birth as a Date.
	 * @param sex The users sex (true = male, false = female).
	 * @param weight The users' weight as a double.
	 * @param height The users' height as a double.
	 */
	public SystemUser(int userID, String email, String password,
			String forename, String surname,
			String county, Date dateOfBirth, boolean sex) {
		this.userID = userID;
		this.password = password;
		this.email = email;
		this.forename = forename;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.sexMale = sex;
		this.admin = false;
		this.verified = false;
		this.measurementPreference = "m";
	}
	
	
	/**
	 * Gets the user ID for this User.
	 * @return The user ID as an integer.
	 */
	public int getUserID() {
		return userID;
	}
	
	/**
	 * Gets the password for this User.
	 * @return The password as a String.
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Gets the email address tied to this User.
	 * @return The email address as a String.
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Gets the forename stored for this User.
	 * @return The forename as a String.
	 */
	public String getForename() {
		return forename;
	}
	
	/**
	 * Gets the surname stored for this User.
	 * @return The surname as a String.
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Gets the county this User lives in.
	 * @return The count as a String.
	 */
	public String getCounty() {
		return county;
	}
	
	/**
	 * Gets the date of birth (DOB) for this User.
	 * @return The DOB as a Date.
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	/**
	 * Gets the sex stored for this User.
	 * @return Returns true for males, false for females.
	 */
	public boolean getSex() {
		return sexMale;
	}
	

	/**
	 * Checks if this user is an administrator.
	 * @return Returns true if they are, false otherwise.
	 */
	public boolean getAdmin() {
		return admin;
	}
	
	/**
	 * Sets the ID of this User to the User ID supplied.
	 * @param userID The unique user ID number from the database.
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	/**
	 * Sets the password of this User to the password supplied.
	 * @param password The password the user has entered.
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Sets the e-mail address of this User to the e-mail address supplied.
	 * @param email The e-mail address the user has entered.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Sets the forename of this User to the forename supplied.
	 * @param forename The forename of the user.
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}
	
	/**
	 * Sets the surname of this User to the surname supplied.
	 * @param surname The surname of the user.
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/**
	 * Sets the county of this User to the county supplied.
	 * @param county The county in which the user lives (in the UK).
	 */
	public void setCounty(String county) {
		this.county = county;
	}
	
	/**
	 * Sets the date of birth of this User to the one supplied.
	 * @param dateOfBirth The date of birth required, as a Date.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	/**
	 * Sets the gender of this User to the gender supplied.
	 * @param sex Gender of the user - true if male, false if female.
	 */
	public void setSex(boolean sex) {
		this.sexMale = sex;
	}
	
	/**
	 * Sets if the user is an admin or not.
	 * @param a True if the user is an admin, false otherwise.
	 */
	public void setAdmin(boolean a) {
		this.admin = a;
	}
	
	/**
	 * Sets whether the user has verified their account or not
	 * @param verified True if verified, false otherwise
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	
	/**
	 * Gets whether the user has verified their account or not
	 * @return A boolean stating whether they've verified or not
	 */
	public boolean getVerified() {
		return this.verified;
	}
	
	/**
	 * Sets the measurement preference of the user
	 * @param measurementPreference how the user wants to measure themselves
	 */
	public void setMeasurementPreference(String measurementPreference) {
		this.measurementPreference = measurementPreference;
	}
	
	/**
	 * Gets the measurement preference of the user
	 * @return A 1 character string identifying the preference
	 * (m = metric, i = imperial, s = Scott Grandisons)
	 */
	public String getMeasurementPreference() {
		return this.measurementPreference;
	}
}
