package healthTracker.servlets;

import healthTracker.goal.Goal;
import healthTracker.group.GroupController;
import healthTracker.group.GroupDetails;
import healthTracker.user.SystemUser;
import healthTracker.user.UserHistory;
import healthTracker.user.UserManager;
import healthTracker.utils.Conversions;
import healthTracker.utils.Mail;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GoalManager
 * 
 * @author 6266215
 */
@WebServlet(description = "Manages users goals, allowing for the display, addition and removal of Goals.", urlPatterns = { "/GoalManager" })
public class GoalManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getSession().getAttribute("user") == null) {
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
			dispatch.forward(request, response);
		} else {
			/* If they are, they can use this feature. First get the User */
			SystemUser user = (SystemUser)request.getSession().getAttribute("user");
			
			String action = request.getParameter("do");
			
			if (action != null && !action.isEmpty()) {
				if (action.equals("edit")) {
					/* If an action was provided. */
					String idS = request.getParameter("id");
					int id = 0;
					
					try {
						/* Attempt to convert the value to an integer. */
						id = Integer.valueOf(idS);
					} catch (NumberFormatException e) {
						return;
					}
					/* Check that a usable value was actually provided. */
					if (id == 0) {
						String errType = "Invalid goal!";
						String errMsg = "The goal you want to edit is not valid.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						return;
					}
					
					Goal g = healthTracker.goal.GoalManager.getGoalFromID(id);
					/* Check that the Goal actually belongs to the signed in user. */
					if (g.getUserID() != user.getUserID()) {
						String errType = "Invalid goal!";
						String errMsg = "This goal is not yours.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						return;
					}
					
					/* Check if the Goal is a group goal. */
					if (healthTracker.goal.GoalManager.isSubscribedToGroupGoal(user.getUserID(), g.getId())) {
						/* If it is, the user cannot edit it. */
						String errType = "Group Goal";
						String errMsg = "As this is a group goal, you are not able to edit it.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						return;
					}
					
					double tarVal = Math.abs(g.getTarget());
					/* If the goal is a weight goal and the user is measuring in imperial. */
					if (g.isWeightGoal() && user.getMeasurementPreference().contains("i")) {
						/* Convert the target value. */
						tarVal = Conversions.kgToStone(tarVal);
					}
					
					request.setAttribute("goal", g);
					request.setAttribute("val", tarVal);
					request.getRequestDispatcher("WEB-INF/edit_goal.jsp").forward(request, response);
				}
			} else {
				/* If no valid parameter was provided, just show the manager. */
				Calendar today = Calendar.getInstance(); // Get today.
				
				/* Store two types of unit, as we are dealing with both weight and height. */
				String wUnit = "kg";
				
				if (user.getMeasurementPreference().contains("i")) {
					/* If the user is using imperial measurements, set both units. */
					wUnit = "lb";
				}
				
				String goalToDeleteString = request.getParameter("rGoal");
				int goalToDelete;
				
				try {
					goalToDelete = Integer.parseInt(goalToDeleteString);
				} catch (Exception e) {
					goalToDelete = 0;
				}
				
				// They want to delete a goal
				if (goalToDelete>0) {
					Goal theGoal = healthTracker.goal.GoalManager.getGoalFromID(goalToDelete);
					
					if (theGoal != null) {
						if (theGoal.getUserID()==user.getUserID()) {
							
							int groupGoalID = healthTracker.goal.GoalManager.isPartOfGroupGoal(goalToDelete);
							
							// Is this a group goal that the user did create? If so, we need to handle it differently
							// To prevent the goal being deleted for the entire group.
							if (healthTracker.goal.GoalManager.didCreateGroupGoal(goalToDelete)) {
								
								// This will change the user ID of the main goal so it doesn't need to be deleted
								healthTracker.goal.GoalManager.removeUserIDInGroupGoal(goalToDelete);
								
							// If this is a group goal we're subscribed to, we need to unsubscribe
							} else if (groupGoalID>0) {
								healthTracker.goal.GoalManager.unsubscribeUserFromGroupGoal(groupGoalID, goalToDelete);
								
								// Now we can safely delete the goal
								healthTracker.goal.GoalManager.removeGoal(goalToDelete);
							} else {
								healthTracker.goal.GoalManager.removeGoal(goalToDelete);
							}						
						}
					}
				}
				
				/* If there are not GET parameters, show a listing of all Goals. */
				ArrayList<Goal> allGoals = healthTracker.goal.GoalManager.getUserGoals(user.getUserID());
				ArrayList<Goal> active = new ArrayList<>();
				ArrayList<Goal> inActive = new ArrayList<>();
				for (Goal g : allGoals) {
					if (g.getGoalDate().before(today.getTime())
							|| g.isCompleted()) {
						/* If the goal is already completed. */
						inActive.add(g); // Add it to inactive.
					} else {
						/* Otherwise the goal is fine, add it to active. */
						g = healthTracker.goal.GoalManager.updateGoal(g);
						active.add(g);
					}
					/* Check each Goal to see if they are active or not. */
					if (g.isWeightGoal()) {
						/* Convert the values of weight goals to the right units. */
						if (user.getMeasurementPreference().contains("i")) {
							g.setStart(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getStart())));
							g.setTarget(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getTarget())));
						} else {
							g.setStart(Conversions.roundTwoDecimals(g.getStart()));
							g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
						}						
					} else {
						/* Convert the units for exercise goals. */
							g.setStart(Conversions.roundTwoDecimals(g.getStart()));
							g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
					}
				}
				
				/* Send everything to the page. */
				request.setAttribute("wUnit", wUnit);
				request.setAttribute("active", active);
				request.setAttribute("inActive", inActive);
				
				request.getRequestDispatcher("WEB-INF/goal_manager.jsp").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getSession().getAttribute("user") == null) {
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
			dispatch.forward(request, response);
		} else {
			/* If they are, they can use this feature. First get the User */
			SystemUser user = (SystemUser)request.getSession().getAttribute("user");
			Calendar today = Calendar.getInstance();
			
			if (request.getParameter("do").equals("add")) {
				/* The user wants to add something. */
				String goalType = request.getParameter("goalType");
				String goalDate = request.getParameter("goalDate");
				String groupID = request.getParameter("groupID");
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				double goalValD = 0;
				
				/* Check the date and value provided first to avoid wasted time. */
				if (goalDate == null || goalDate.isEmpty()) {
					/* The user modified the date format. */
					String errType = "Invalid date!";
					String errMsg = "The date you entered could not be used, please use dd/mm/yyyy format.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}
				
				try {
					dateFormat.parse(goalDate); // Format the date so we can store it.
				} catch (ParseException e) {
					/* If the date fails to parse for some reason. */
					String errType = "Invalid Date Format!";
					String errMsg = "The date you entered could not be understood, please use 'dd/mm/yyyy'.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				/* Check if the date is a future date. */
				if (!dateFormat.getCalendar().getTime().after(today.getTime())) {
					String errType = "Incorrect Goal Date!";
					String errMsg = "You can't use dates that have already passed or today for a goal date.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				/* Now we can use them properly. */
				String goalVal = request.getParameter("goalVal");
				if (goalVal != null && !goalVal.isEmpty()) {
					/* So we need to check if the value they entered is good. */
					try {
						goalValD = Double.valueOf(goalVal);
					} catch (NumberFormatException e) {
						/* It wasn't for some reason. */
						String errType = "Invalid value!";
						String errMsg = "Please check you entered a valid number.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						return;
					}
				} else {
					/* The user didn't provide a value. */
					String errType = "You must provide a value!";
					String errMsg = "You need to provide a value to create a new goal.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}				
				
				/* Create the base of a new Goal. */
				Goal newGoal = new Goal();
				newGoal.setCreationDate(today.getTime());
				newGoal.setGoalDate(dateFormat.getCalendar().getTime());
				
				if (goalType != null && !goalType.isEmpty()) {
					if (goalType.equals("1")) {
						/* We are dealing with a weight goal now. */
						String goalWeight = request.getParameter("goalWeight");
						if ((goalWeight != null && !goalWeight.isEmpty())
								&& (goalWeight.equals("1") || goalWeight.equals("2")))
						{
							/* The user has selected a valid weight target type.
								Get the unit the user measures in. */
							String unit = request.getParameter("unit");
							/* Check the unit was actually provided and was valid. */
							if ((unit != null && !unit.isEmpty())
									&& unit.equals("1") || unit.equals("2")) {
								if (user.getMeasurementPreference().contains("i")) {
									/* If the user is measuring in imperial units, convert the goal value. */
									if (unit.equals("1")) {
										goalValD = Conversions.poundToKg(goalValD);
									} else if (unit.equals("2")) {
										goalValD = Conversions.stoneToKG(goalValD);
									}
								} else {
									/* If they are using anything else, assume Metric. */
									if (unit.equals("1")) {
										goalValD = goalValD/1000; // Grams to KG.
									}
								}
							} else {
								String errType = "Invalid unit!";
								String errMsg = "You have picked an invalid unit of measurement. Please only use the values provided.";
								request.setAttribute("errType", errType);
								request.setAttribute("errMsg", errMsg);
								request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
								return;
							}
							
							if (goalWeight.equals("2")) {
								/* The user wants to lose this weight, so invert the value. */
								goalValD = -goalValD;
							}
							/* Everything has gone well up to here. */
							UserHistory hist = UserManager.getLastHistory(user.getUserID());
							/* Set goal type specific values. */
							newGoal.setGoalType(true);
							newGoal.setStart(hist.getWeight());
							newGoal.setTarget(goalValD);							
						} else {
							String errType = "Invalid weight goal!";
							String errMsg = "You have select an invalid weight goal type, it can only be the values we provide.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
							return;
						}
					} else if (goalType.equals("2")) {
						/* The user wants a diet goal. */
						String dietType = request.getParameter("goalDiet");
						
						if ((dietType != null && !dietType.isEmpty())
								&& (dietType.equals("1") || dietType.equals("2"))) {
							
							if (dietType.equals("2")) {
								goalValD = -goalValD;
							}
							
							newGoal.setGoalType(false);
							newGoal.setStart(0.0);
							newGoal.setTarget(goalValD);
						} else {
							/* The user shouldn't be able to reach these errors, but just in case... */
							String errType = "Invalid diet goal!";
							String errMsg = "You have select an invalid diet goal type, it can only be the values we provide.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
							return;
						}
					}
				}
				/* Persist the goal to the database. */
				int goalID = healthTracker.goal.GoalManager.createGoal(user.getUserID(), newGoal.getCreationDate(), 
						newGoal.getGoalDate(), newGoal.isWeightGoal(), 
						newGoal.getStart(), newGoal.getTarget());
				
				// Is this going to be a group goal?
				if (groupID!=null && !groupID.isEmpty()) {
					
					int group = 0;
					
					try {
						group = Integer.parseInt(groupID);
					} catch (Exception e) {
						group = 0;
					}
					
					healthTracker.goal.GoalManager.createGroupGoal(goalID, group);
					
					GroupDetails theGroup = GroupController.getGroupFromID(group);
					
					ArrayList<SystemUser> usersToEmail = healthTracker.goal.GoalManager.getEmailsOfGroupUsers(group, user.getUserID());
					
					for (SystemUser theUser : usersToEmail) {
						String subject[] = {"Health Tracker - New Group Goal"};
						String message[] = {"Dear "+theUser.getForename()+",\n\n"+user.getForename()+" "+user.getSurname()+", a member of the " +
								"'"+theGroup.getGroupName()+"' group has created a new group goal.\n\nTo subscribe to it, please go to the following link: " +
								"\n\nhttp://localhost:8080/Health_Tracker/ViewGroup?id="+theGroup.getGroupID()+"\n\nYou can find the subscription link at the bottom " +
								"of the page.\n\nThanks,\nSystem Admins"};
						String emailArray[] = {theUser.getEmail()};
		                		
		                // Send the user an e-mail
		                Mail.sendMail(emailArray, subject, message);
					}
					
					response.sendRedirect("ViewGroup?id="+group);
					
				// This is just an individual goal, so we don't have to worry about anything
				} else {
					response.sendRedirect("GoalManager"); // Redirect to GoalManager (avoids a POST loop I found).
				}
			} else if (request.getParameter("do").equals("editGoal")) {
				
				String idS = request.getParameter("goal");
				int id = 0;
				
				/* Attempt to parse the ID for the edited Goal. */
				try {
					id = Integer.valueOf(idS);
				} catch (NumberFormatException e) {
					/* If the ID can't be parsed the user did something silly. */
					String errType = "Invalid GoalID!";
					String errMsg = "The Goal ID provided could not be understood, was it a integer?";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}
				
				/* Check the ID was assigned and is non-zero. */
				if (id == 0) {
					/* The ID was zero... */
					String errType = "Invalid GoalID!";
					String errMsg = "The provided GoalID does not exist.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}
				
				Goal editedGoal = healthTracker.goal.GoalManager.getGoalFromID(id);
				/* Check if the user is allowed to edit this Goal. */
				if (editedGoal.getUserID() != user.getUserID()) {
					String errType = "Cannot Change Goal!";
					String errMsg = "This is not your goal to edit.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}
				
				/* Get the Goal details. */
				String valS = request.getParameter("goalVal");
				double val = editedGoal.getTarget();
				String dateS = request.getParameter("goalDate");
				Date date = editedGoal.getGoalDate();
				String targetType = request.getParameter("lose");
				
				/* Make sure the user hasn't modifed something they shouldn't have. */
				if (targetType == null || targetType.isEmpty()
						&& !(targetType.equals("true") || targetType.equals("false"))) {
					String errType = "Invalid Target Type!";
					String errMsg = "The target type provided is incorrect.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}
				
				/* Check if there was a value provided, we don't care if there wasn't. */
				if (valS != null && !valS.isEmpty()) {
					try {
						val = Double.valueOf(valS);
					} catch (NumberFormatException e) {
						String errType = "Invalid Target!";
						String errMsg = "The target value you provided is not valid. Was it a number?";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						return;
					}
				}
				
				/* Check if there is a date, we don't care if there isn't. */
				if (dateS != null && !dateS.isEmpty()) {
					try {
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); // We need a formatter.
						dateFormat.parse(dateS);
						date = new Date(dateFormat.getCalendar().getTime().getTime());
					} catch (ParseException e) {
						String errType = "Invalid Date!";
						String errMsg = "The date you provided was not correctly formatted!";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						return;
					}
				}
				
				editedGoal.setGoalDate(date);
				
				if (editedGoal.isWeightGoal()) {
					/* If the Goal is a weight Goal. */
					String unit = request.getParameter("unit");
					/* Convert the target value based on what the user is using. */
					if (user.getMeasurementPreference().equals("i")) {
						if (unit.equals("1")) {
							editedGoal.setTarget(Conversions.poundToKg(val));
						} else if (unit.equals("2")) {
							editedGoal.setTarget(Conversions.stoneToKG(val));
						} else {
							String errType = "Invalid Unit!";
							String errMsg = "The unit you picked doesn't exist.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
							return;
						}
					} else {
						if (unit.equals("1")) {
							val /= 1000;
						}
					}
				}
				
				/* Abs the value to stop negatives playing up. */
				if (val < 0.0) {
					val = Math.abs(val);
				}
				/* Invert the value if this is a loss goal. */
				if (targetType.equals("true")) {
					val = -val;
				}
				
				editedGoal.setTarget(val);
				/* Edit the goal! */
				healthTracker.goal.GoalManager.editGoal(editedGoal);
				response.sendRedirect("GoalManager");
			}
		}
	}
}
