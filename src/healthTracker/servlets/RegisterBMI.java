package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class RegisterBMI
 */
@WebServlet("/RegisterBMI")
public class RegisterBMI extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("WEB-INF/register_bmi.jsp").forward(
				request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		SystemUser usr = null;

		if (request.getSession().getAttribute("user") != null) {
			usr = (SystemUser) request.getSession().getAttribute("user");

			int userID = usr.getUserID();
			int measurementType = Integer.parseInt(request
					.getParameter("measurementType"));
			double heightVal = 0, weightVal = 0;

			if (measurementType == 1) {
				// Is this metric?
				String heightM = request.getParameter("heightValMeters");
				String weightKg = request.getParameter("weightValKilograms");

				// Convert the metres to cm for the database
				try {
					heightVal = Double.parseDouble(heightM) * 100;
				} catch (Exception e) {
					heightVal = 0;
				}

				try {
					weightVal = Double.parseDouble(weightKg);
				} catch (Exception e) {
					weightVal = 0;
				}
				
				UserManager.updateMeasurementPreference(userID, "m");
				usr.setMeasurementPreference("m");
			} else if (measurementType == 2) {
				// No, it's imperial...
				String heightValF = request.getParameter("heightValFeet");
				String heightValI = request.getParameter("heightValInches");
				String weightValS = request.getParameter("weightValStone");
				String weightValP = request.getParameter("weightValPounds");

				// Convert the "ft in" to just inches, and then into cm
				try {
					int inches = Integer.parseInt(heightValF) * 12
							+ Integer.parseInt(heightValI);
					heightVal = inches * 2.54;
				} catch (Exception e) {
					heightVal = 0;
				}

				// Convert the "st lb" into just pounds, and then into kg
				try {
					int pounds = Integer.parseInt(weightValS) * 14
							+ Integer.parseInt(weightValP);
					weightVal = pounds * 0.453592;
				} catch (Exception e) {
					weightVal = 0;
				}
				
				UserManager.updateMeasurementPreference(userID, "i");
				usr.setMeasurementPreference("i");
			}

			// Check if the height and weight actually make sense
			if (heightVal > 0 && weightVal > 0) {
				// Update the database with these values
				UserManager.updateInitialWeightAndHeight(userID, heightVal,
						weightVal);

				HttpSession userSession = request.getSession(true);
				userSession.setAttribute("user", usr);

				// And send the user to the profile page
				response.sendRedirect("Profile");

			} else {
				// Looks like the values entered weren't valid
				String type = "Invalid height/weight";
				String message = "The height and weight must be numbers greater than 0.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(
						request, response);
			}
		} else {
			// The user ID was blank, odd. Let's show an error.
			String type = "Session expired";
			String message = "Your session has expired. Please try again.";
			request.setAttribute("errType", type);
			request.setAttribute("errMsg", message);
			request.getRequestDispatcher("WEB-INF/error.jsp").forward(request,
					response);
		}
	}
}