package healthTracker.servlets;

import healthTracker.group.GroupController;
import healthTracker.user.SystemUser;
import healthTracker.utils.Conversions;
import healthTracker.goal.Goal;
import healthTracker.goal.GoalManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewGroup
 */
@WebServlet("/ViewGroup")
public class ViewGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String group = request.getParameter("id");
		int groupID;
		
		try {
			groupID = Integer.parseInt(group);
		} catch (Exception e) {
			groupID = 0;
		}
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
			
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			// Does the group they want to look at actually exist?
			if (GroupController.doesGroupIDExist(groupID)) {
				request.setAttribute("group", GroupController.getGroupFromID(groupID));
				
				request.setAttribute("users", GroupController.getGroupMembersFromGroupID(groupID));
				
				boolean hasJoinedGroup = GroupController.hasJoinedGroup(usr.getUserID(), groupID);
				
				request.setAttribute("hasJoinedGroup", hasJoinedGroup);
				
				ArrayList<Goal> groupGoals = GoalManager.getGroupGoals(groupID, usr.getUserID());
				ArrayList<Goal> groupGoalsConv = new ArrayList<>();
				
				for (Goal g : groupGoals) {
					/* Check each Goal to see if they are active or not. */
					if (g.isWeightGoal()) {
						/* Convert the values of weight goals to the right units. */
						if (usr.getMeasurementPreference().contains("i")) {
							g.setStart(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getStart())));
							g.setTarget(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getTarget())));
						}							
					} else {
						/* Convert the units for exercise goals. */
						g.setStart(Conversions.roundTwoDecimals(g.getStart()));
						g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
					}
					
					groupGoalsConv.add(g);
				}
				
				request.setAttribute("groupGoals", groupGoalsConv);
				
				/* Store two types of unit, as we are dealing with both weight and height. */
				String wUnit = usr.getMeasurementPreference();
				if (wUnit.contains("i")) {
					/* If the user is using imperial measurements, set both units. */
					wUnit = "lb";
				} else if (wUnit.contains("s")) {
					/* If they are measured in Scott, only change the weight unit. */
					wUnit = "SG";
				} else {
					wUnit = "kg";
				}
				
				request.setAttribute("wUnit", wUnit);
				
				Calendar today = Calendar.getInstance(); // Get today.
				
				ArrayList<Goal> allGoals = healthTracker.goal.GoalManager.getUserGoals(usr.getUserID());
				ArrayList<Goal> active = new ArrayList<>();
				for (Goal g : allGoals) {
					
					if (!(g.getGoalDate().before(today.getTime())
							|| g.isCompleted())) {
						/* The goal is running, add it to active. */
						/* Check each Goal to see if they are active or not. */
						if (g.isWeightGoal()) {
							/* Convert the values of weight goals to the right units. */
							if (usr.getMeasurementPreference().contains("i")) {
								g.setStart(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getStart())));
								g.setTarget(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getTarget())));
							}						
						} else {
							g.setStart(Conversions.roundTwoDecimals(g.getTarget()));
							g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
						}
						
						active.add(g);
					}
				}
				
				request.setAttribute("active", active);
						
				RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/viewgroup.jsp");
				dispatch.forward(request, response);
				
			// It doesn't exist, so show an error.
			} else {
				String type = "Group ID not valid";
				String message = "Group ID not valid!";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
	
		// The user isn't logged in, send them to the admin page
		} else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String group = request.getParameter("joinGroup");
		boolean isJoiningGroup = true;
		
		// Maybe they want to be removed from a group instead.
		if (group==null || group.isEmpty()) {
			group = request.getParameter("leaveGroup");
			isJoiningGroup = false;
		}
		
		int groupID;
		
		try {
			groupID = Integer.parseInt(group);
		} catch (Exception e) {
			groupID = 0;
		}
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
			
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			Calendar today = Calendar.getInstance(); // Get today.
			
			/* Store two types of unit, as we are dealing with both weight and height. */
			String wUnit = "kg";
			
			if (usr.getMeasurementPreference().contains("i")) {
				/* If the user is using imperial measurements, set both units. */
				wUnit = "lb";
			}
			
			request.setAttribute("wUnit", wUnit);
			
			ArrayList<Goal> allGoals = healthTracker.goal.GoalManager.getUserGoals(usr.getUserID());
			ArrayList<Goal> active = new ArrayList<>();
			for (Goal g : allGoals) {
				if (!(g.getGoalDate().before(today.getTime())
						|| g.isCompleted())) {
					/* Check each Goal to see if they are active or not. */
					if (g.isWeightGoal()) {
						/* Convert the values of weight goals to the right units. */
						if (usr.getMeasurementPreference().contains("i")) {
							g.setStart(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getStart())));
							g.setTarget(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getTarget())));
						}							
					} else {
						/* Convert the units for exercise goals. */
							g.setStart(Conversions.roundTwoDecimals(g.getStart()));
							g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
					}
					/* Otherwise the goal is fine, add it to active. */
					active.add(g);
				}
			}
			
			request.setAttribute("active", active);
			
			// Was the group ID they entered valid?
			if (GroupController.doesGroupIDExist(groupID)) {
				
				// Does the user want to join a group?
				if (isJoiningGroup) {
					
					// Have they already joined the group?
					if (!GroupController.hasJoinedGroup(usr.getUserID(), groupID)) {
						
						// Are they joined to less than 3 groups? If so, they can join
						if (GroupController.getGroupsFromUser(usr.getUserID()).size()<3) {
							
							GroupController.addUser(groupID, usr.getUserID());
							
							request.setAttribute("group", GroupController.getGroupFromID(groupID));
							
							request.setAttribute("users", GroupController.getGroupMembersFromGroupID(groupID));
							
							request.setAttribute("hasJoinedGroup", GroupController.hasJoinedGroup(usr.getUserID(), groupID));
							
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/viewgroup.jsp");
							dispatch.forward(request, response);
							
						// They're already joined to 3 groups, so they cannot join.	
						} else {
							String type = "Unable to join";
							String message = "You are currently joined to 3 groups. Please leave one of these groups to join.";
							request.setAttribute("errType", type);
							request.setAttribute("errMsg", message);
							request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						}
						
					// They've already joined the group, so show an error.
					} else {
						String type = "Already joined";
						String message = "You are already a member of this group.";
						request.setAttribute("errType", type);
						request.setAttribute("errMsg", message);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					}
					
				// No, the user wants to leave a group
				} else {
					
					// Are they actually a member of this group?
					if (GroupController.hasJoinedGroup(usr.getUserID(), groupID)) {
						
						GroupController.leaveGroup(groupID, usr.getUserID());
						
						// If there are still members, the group still exists.
						if (GroupController.doesGroupIDExist(groupID)) {
							request.setAttribute("group", GroupController.getGroupFromID(groupID));
							
							request.setAttribute("users", GroupController.getGroupMembersFromGroupID(groupID));
							
							request.setAttribute("hasJoinedGroup", GroupController.hasJoinedGroup(usr.getUserID(), groupID));
							
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/viewgroup.jsp");
							dispatch.forward(request, response);
							
						// They were the last member. The group was deleted, so send them back to GroupManager.
						} else {
							response.sendRedirect("GroupManager");
						}

					// They've already joined the group, so show an error.
					} else {
						String type = "Not joined";
						String message = "You cannot leave this group as you are not a member.";
						request.setAttribute("errType", type);
						request.setAttribute("errMsg", message);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					}
				}
				
			// It wasn't valid, show an error.
			} else {
				String type = "Group ID not valid";
				String message = "Group ID not valid!";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
				
		// The user isn't logged in, send them to the admin page
		} else {
			response.sendRedirect("Login");
		}
	}

}
