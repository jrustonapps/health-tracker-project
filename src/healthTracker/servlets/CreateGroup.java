package healthTracker.servlets;

import healthTracker.group.GroupController;
import healthTracker.user.SystemUser;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreateGroup
 */
@WebServlet("/CreateGroup")
public class CreateGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateGroup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getSession().getAttribute("user") == null) {
			/* If the user is not logged in, show them the login screen. */
			response.sendRedirect("Login");
		} else {
			/* If they are logged in, take them to the BMI Settings page. */
			RequestDispatcher dispatch = getServletContext()
					.getRequestDispatcher("/WEB-INF/create_group.jsp");
			dispatch.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getSession().getAttribute("user") == null) {
			/* If the user is not logged in, show them the login screen. */
			response.sendRedirect("Login");
		} else {
			
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			String groupName = request.getParameter("groupName");
			String groupDescription = request.getParameter("groupDescription");
			
			// Has both fields been entered correctly?
			if ((groupName != null && !groupName.isEmpty()) &&
					(groupDescription != null && !groupDescription.isEmpty())) {
				
				// Check if the group name already exists
				if (!GroupController.doesGroupNameExist(groupName)) {
					
					// Are they a member of less than 3 groups?
					if (GroupController.getGroupsFromUser(usr.getUserID()).size()<3) {
						GroupController.createGroup(groupName, groupDescription, usr.getUserID());
						
						response.sendRedirect("GroupManager");
						
					// They're already a member of 3 groups.
					} else {
						String type = "Unable to create group";
						String message = "You are already joined to 3 groups. Please leave a group before creating another.";
						request.setAttribute("errType", type);
						request.setAttribute("errMsg", message);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					}
				
				// It does already exist, show an error
				} else {
					String type = "Group already exists!";
					String message = "A group already exists with this name!";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
				}
			
			// Fields weren't entered, show an error
			} else {
				String type = "Incomplete information!";
				String message = "Required field was not entered.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
		}
	}

}
