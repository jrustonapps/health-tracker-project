package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import healthTracker.utils.Security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VerifyEmail
 */
@WebServlet("/VerifyEmail")
public class VerifyEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int userID;
		
		// Get the user ID and the security key (their e-mail hashed)
		String id = request.getParameter("id");
		String key = request.getParameter("key");
		
		// Parse the string as an int
		try {
			userID = Integer.parseInt(id);
		} catch (Exception e) {
			userID = 0;
		}
		
		// Check if both feels were entered correctly
		if ((userID > 0) &&
				(key != null && !key.isEmpty())) {
			
			SystemUser user = UserManager.getUserFromID(userID);
			
			// Does a user exist with this ID?
			if (user != null) {
				
				// Was the key correct - their e-mail hashed?
				if (key.equals(Security.hashSHA1(user.getEmail()))) {
					
					// Verify the user
					UserManager.verifyUser(userID);
					
					// Show a confirmation page
					String type = "Verification Success!";
					String message = "Your account has been verified. You may now login.";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/success.jsp").forward(request, response);
					
				// The key wasn't correct
				} else {
					String type = "Invalid Key";
					String message = "The key entered was invalid.";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
				}
			
			// The user ID doesn't exist
			} else {
				String type = "Invalid User ID";
				String message = "The user ID entered was invalid.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
		
		// The link entered wasn't valid - something was missing
		} else {
			String type = "Invalid Link";
			String message = "The link you clicked was invalid.";
			request.setAttribute("errType", type);
			request.setAttribute("errMsg", message);
			request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// No POST method for this Servlet.
	}

}
