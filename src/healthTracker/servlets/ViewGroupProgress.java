package healthTracker.servlets;

import healthTracker.goal.Goal;
import healthTracker.group.GroupController;
import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import healthTracker.utils.Conversions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewGroupProgress
 */
@WebServlet("/ViewGroupProgress")
public class ViewGroupProgress extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewGroupProgress() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
							
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			String group = request.getParameter("gID");
			String goal = request.getParameter("glID");
			int goalID, groupID;
			
			try {
				goalID = Integer.parseInt(goal);
			} catch (Exception e) {
				goalID = 0;
			}
			
			try {
				groupID = Integer.parseInt(group);
			} catch (Exception e) {
				groupID = 0;
			}
			
			// Are we a member of this group?
			if (GroupController.hasJoinedGroup(usr.getUserID(), groupID)) {
				
				ArrayList<Goal> groupGoals = healthTracker.goal.GoalManager.getGroupGoalProgress(goalID);
				
				ArrayList<Goal> updatedGoals = new ArrayList<Goal>();
				
				Calendar today = Calendar.getInstance(); // Get today.
				
				/* Store two types of unit, as we are dealing with both weight and height. */
				String wUnit = "kg";
				
				if (usr.getMeasurementPreference().contains("i")) {
					/* If the user is using imperial measurements, set both units. */
					wUnit = "lb";
				}
				
				ArrayList<SystemUser> goalUsers = new ArrayList<SystemUser>();
				
				if (groupGoals.size()>0) {
					goalUsers = UserManager.getUsersFromGoals(groupGoals);
				}
				
				for (Goal g : groupGoals) {
					/* Check each Goal to see if they are active or not. */
					
					if (!(g.getGoalDate().before(today.getTime())
							|| g.isCompleted())) {
						/* Otherwise the goal is fine, add it to active. */
						g = healthTracker.goal.GoalManager.updateGoal(g);
					}
					
					if (g.isWeightGoal()) {
						/* Convert the values of weight goals to the right units. */
						if (usr.getMeasurementPreference().contains("i")) {
							g.setStart(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getStart())));
							g.setTarget(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getTarget())));
						}							
					} else {
						/* Convert the units for exercise goals. */
							g.setStart(Conversions.roundTwoDecimals(g.getStart()));
							g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
					}
					
					if (g.getGoalDate().before(today.getTime())
							|| g.isCompleted()) {
						/* If the goal is already completed. */
						updatedGoals.add(g); // Add it to inactive.
					} else {
						/* Otherwise the goal is fine, add it to active. */
						//Goal mg = healthTracker.goal.GoalManager.updateGoal(g);
						updatedGoals.add(g);
					}
					
				}
				
				request.setAttribute("goalProgress", updatedGoals);
				request.setAttribute("goalUsers", goalUsers);
				request.setAttribute("wUnit", wUnit);
				
				RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/viewgroupprogress.jsp");
				dispatch.forward(request, response);
				
			// We are not a member, so show an error.
			} else {
				String type = "Not Joined";
				String message = "You are not a member of this group.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
			
		// The user isn't logged in, send them back to the login page
		} else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
