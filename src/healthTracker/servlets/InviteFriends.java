package healthTracker.servlets;

import healthTracker.group.GroupController;
import healthTracker.group.GroupDetails;
import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import healthTracker.utils.Mail;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InviteFriends
 */
@WebServlet("/InviteFriends")
public class InviteFriends extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InviteFriends() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String emailAddress = request.getParameter("emailAddress");
		
		String group = request.getParameter("groupID");
		int groupID;
		
		try {
			groupID = Integer.parseInt(group);
		} catch (Exception e) {
			groupID = 0;
		}
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
					
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			// Does the emailAddress variable actually contain something?
			if (emailAddress!=null && !emailAddress.isEmpty()) {
				
				// Does this group actually exist?
				if (GroupController.doesGroupIDExist(groupID)) {
					
					// Does this email address already exist in the system?
					if (UserManager.doesEmailExist(emailAddress)) {
						
						SystemUser invitedUser = UserManager.findUserByEmail(emailAddress);
						
						// Check to make sure they aren't already part of the group
						if (!GroupController.hasJoinedGroup(invitedUser.getUserID(), groupID)) {
							
							// Check if the e-mail address is valid
							if (Mail.isValidEmailAddress(emailAddress)) {
								GroupDetails invitedGroup = GroupController.getGroupFromID(groupID);
								
								// Prepare our e-mail message
				        		String emailSubject[] = {"Health Tracker Invitation"};
				        		String emailMessage[] = {"Dear "+invitedUser.getForename()+",\n\nYour friend "+
				        		usr.getForename()+" "+usr.getSurname()+" has invited you to the '"+
				        		invitedGroup.getGroupName()+"' group. To join this group, please go to the following link: "+
				        		"\n\n http://localhost:8080/Health_Tracker/ViewGroup?id="+invitedGroup.getGroupID()+
				        		"\n\nThanks,\nSystem Admins"};
				        		String emailArray[] = {emailAddress};
				        		
				        		// Send the user an e-mail
				        		Mail.sendMail(emailArray, emailSubject, emailMessage);
				        		
				        	// The e-mail address is invalid.
							} else {
								String type = "Invalid E-Mail";
								String message = "The e-mail address entered was invalid.";
								request.setAttribute("errType", type);
								request.setAttribute("errMsg", message);
								request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
							}
			        		
			        	// They're already part of the group, show an error.
						} else {
							String type = "Already Joined";
							String message = "This user has already joined the group.";
							request.setAttribute("errType", type);
							request.setAttribute("errMsg", message);
							request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						}
		        		
					// It doesn't exist, maybe we should tell them to join.
					} else {
						
						GroupDetails invitedGroup = GroupController.getGroupFromID(groupID);
						
						// Prepare our e-mail message
						String emailSubject[] = {"Health Tracker Invitation"};
		        		String emailMessage[] = {"Dear User,\n\nYour friend "+
		        		usr.getForename()+" "+usr.getSurname()+" has invited you to the '"+
		        		invitedGroup.getGroupName()+"' group on Health Tracker. Our records indicate that you are not yet a registered user. "+
		        		"To join this group, please create an account at: "+
		        		"\n\nhttp://localhost:8080/Health_Tracker/Register"+
		        		"\n\nYou will then be able to join the group by going to the following link: "+
		        		"\n\n http://localhost:8080/Health_Tracker/ViewGroup?id="+invitedGroup.getGroupID()+
		        		"\n\nThanks,\nSystem Admins"};
		        		String emailArray[] = {emailAddress};
		        		
		        		// Send the user an e-mail
		        		Mail.sendMail(emailArray, emailSubject, emailMessage);
					}
	        		
	        		// Show the confirmation page
	        		String type = "E-Mail Sent!";
					String message = "An e-mail has been sent to your friend.";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/success.jsp").forward(request, response);
					
				// This group doesn't actually exist
				} else {
					String type = "Invalid Group";
					String message = "This group does not exist.";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
				}
				
			// The user didn't enter anything, so show an error.
			} else {
				String type = "E-Mail Address not valid";
				String message = "Please enter a valid e-mail address.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
			
		// The user isn't logged in, send them to the admin page
		} else {
			response.sendRedirect("Login");
		}
	}

}
