package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserHistory;
import healthTracker.user.UserManager;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class BMISettings
 */
@WebServlet("/BMISettings")
public class BMISettings extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (request.getSession().getAttribute("user") == null) {
			/* If the user is not logged in, show them the login screen. */
			response.sendRedirect("Login");
		} else {
			/* If they are logged in, take them to the BMI Settings page. */
			RequestDispatcher dispatch = getServletContext()
					.getRequestDispatcher("/WEB-INF/bmi_setting.jsp");
			dispatch.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		/* If the user submits the BMI Settings form, use this. */
		if (request.getSession().getAttribute("user") == null) {
			/* If the user is not logged in, show them the login screen. */
			response.sendRedirect("Login");
		} else {
			/* If they are logged in, then we need to update their details. */
			SystemUser usr = (SystemUser)request.getSession().getAttribute("user");
			UserHistory hist = UserManager.getLastHistory(usr.getUserID());
			double heightVal = hist.getHeight(), weightVal = hist.getWeight();

			int measurementType = Integer.parseInt(request.getParameter("measurementType"));
			if (measurementType == 1) {
				// Is this metric?
				String heightM = request.getParameter("heightValMeters");
				String weightKg = request.getParameter("weightValKilograms");

				// Convert the metres to cm for the database
				if (heightM != null && !heightM.isEmpty()) {
					/* Only update if there was a value entered. */
					try {
						heightVal = Double.parseDouble(heightM) * 100;
					} catch (Exception e) {	}
				}

				if (weightKg != null && !weightKg.isEmpty()) {
					try {
						weightVal = Double.parseDouble(weightKg);
					} catch (Exception e) { }
				}
				
				UserManager.updateMeasurementPreference(usr.getUserID(), "m");
				usr.setMeasurementPreference("m");
			} else if (measurementType == 2) {
				// No, it's imperial...
				String heightValF = request.getParameter("heightValFeet");
				String heightValI = request.getParameter("heightValInches");
				String weightValS = request.getParameter("weightValStone");
				String weightValP = request.getParameter("weightValPounds");

				// Convert the "ft in" to just inches, and then into cm
				if (heightValF != null && !heightValF.isEmpty()) {
					try {
						int inches = Integer.parseInt(heightValF) * 12
								+ Integer.parseInt(heightValI);
						heightVal = inches * 2.54;
					} catch (Exception e) { }
				}

				// Convert the "st lb" into just pounds, and then into kg
				if (weightValP != null && !weightValP.isEmpty()
						&& weightValS != null && !weightValS.isEmpty()) {
					try {
						int pounds = Integer.parseInt(weightValS) * 14
								+ Integer.parseInt(weightValP);
						weightVal = pounds * 0.453592;
					} catch (Exception e) { }
				}	
				
				UserManager.updateMeasurementPreference(usr.getUserID(), "i");
				usr.setMeasurementPreference("i");
			}
			
			UserManager.updateInitialWeightAndHeight(usr.getUserID(), heightVal, weightVal);
			
			HttpSession userSession = request.getSession(true);
			userSession.setAttribute("user", usr);
			
			response.sendRedirect("Profile");
		}
	}

}
