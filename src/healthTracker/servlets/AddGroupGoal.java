package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserHistory;
import healthTracker.user.UserManager;
import healthTracker.goal.Goal;
import healthTracker.goal.GoalManager;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddGroupGoal
 */
@WebServlet("/AddGroupGoal")
public class AddGroupGoal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
					
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			String goal = request.getParameter("id");
			int goalID;
			
			try {
				goalID = Integer.parseInt(goal);
			} catch (Exception e) {
				goalID = 0;
			}
			
			// Was the goal ID valid?
			if (goalID>0) {
				
				// If we have less than 3 goals, we can add it.
				if (GoalManager.getNumberOfActiveGoals(usr.getUserID())<3) {
					
					// Make sure they aren't already subscribed to it
					if (!GoalManager.isSubscribedToGroupGoal(usr.getUserID(), goalID)) {
					
						Goal groupGoal;
						
						try {
							groupGoal = GoalManager.getGoalFromID(goalID);
						} catch (Exception e) {
							groupGoal = null;
						}
						
						// Check to make sure we have the group goal details properly
						if (groupGoal != null) {
							Date date = new Date();
							Calendar today = Calendar.getInstance();
							
							// Check if the completion date is in the future
							if (groupGoal.getGoalDate().after(today.getTime())) {
								UserHistory userHistory = UserManager.getLastHistory(usr.getUserID());
								int individualGoalID = GoalManager.createGoal(usr.getUserID(), date, groupGoal.getGoalDate(), groupGoal.isWeightGoal(), userHistory.getWeight(), groupGoal.getTarget());
								
								GoalManager.subscribeUserToGroupGoal(individualGoalID, goalID);
								
								response.sendRedirect("GoalManager");
								
							// It isn't in the future, so show an error.
							} else {
								String type = "Invalid Date";
								String message = "The date for a goal must be in the future.";
								request.setAttribute("errType", type);
								request.setAttribute("errMsg", message);
								request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
							}
							
						// We don't have the group goal details, maybe the ID was invalid
						} else {
							String type = "Invalid ID";
							String message = "The goal ID is invalid.";
							request.setAttribute("errType", type);
							request.setAttribute("errMsg", message);
							request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
						}
						
					// They are already subscribed, show an error.
					} else {
						String type = "Already Subscribed";
						String message = "You are already subscribed to this goal.";
						request.setAttribute("errType", type);
						request.setAttribute("errMsg", message);
						request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					}
					
				// The user has 3 or more goals and therefore cannot subscribe to it.
				} else {
					String type = "Too many goals";
					String message = "You are already subscribed to 3 goals. Please complete/delete one first.";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
				}
				
			// The goal ID was not valid
			} else {
				String type = "Goal ID not valid";
				String message = "Goal ID not valid!";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
			
		// The user isn't logged in, send them back to the login page
		} else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
