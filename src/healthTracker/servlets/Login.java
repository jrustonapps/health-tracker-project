package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (request.getQueryString() != null) {
			/* If the user has specified an action to perform, do it here. */
			if (request.getParameter("do").equals("logout")) {
				/* Log the user out. */
				HttpSession session = request.getSession();
				session.invalidate();
				request.getRequestDispatcher("/Index").forward(request, response);
			}
		} else {
			/* If they didn't, just show the login page. */
			request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("email");
		String password = request.getParameter("password");
		
		// Did the user enter both fields?
		if ((username != null && !username.isEmpty()) 
				&& (password != null && !password.isEmpty())) {
			
			// If this is true, the login details entered were correct
			if (UserManager.checkCredentials(username, password)) {
				// Valid login, method is needed to add it to a session.
				SystemUser user = UserManager.findUserByEmail(username);
				
				if (user.getVerified()) {
					HttpSession userSession = request.getSession(true);
					userSession.setAttribute("user", user);
					
					// Check if their current height and weight is correct
					if (UserManager.hasEnteredDiet(user.getUserID())) {
						request.getRequestDispatcher("Index").forward(request, response);
						
					// It isn't, so they haven't fully completed the reg. process
					} else {
						response.sendRedirect("RegisterBMI");
					}
				} else {
					String type = "Account Unverified";
					String message = "You have not yet verified your account. Please check your e-mails.";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
				}
				
			} else {
				// The login details were incorrect.
				String type = "Incorrect details!";
				String message = "The login details entered were not correct.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
		} else {
			// The user didn't enter either their username or password, so show an error.
			String type = "Incomplete information!";
			String message = "Required field was not entered.";
			request.setAttribute("errType", type);
			request.setAttribute("errMsg", message);
			request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
		}
	}

}
