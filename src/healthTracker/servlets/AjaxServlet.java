package healthTracker.servlets;

import healthTracker.captures.DietCapture;
import healthTracker.captures.Exercise;
import healthTracker.captures.ExerciseCapture;
import healthTracker.captures.Food;
import healthTracker.group.GroupController;
import healthTracker.group.GroupDetails;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AjaxServlet
 */
@WebServlet(description = "A Servlet containing all the Ajax related functionality.", urlPatterns = { "/AjaxServlet" })
public class AjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String result = "";
		
		String query = request.getParameter("groupSearch");
				
		if (query == null || query.isEmpty()) {
			query = "";
		} else {
			ArrayList<GroupDetails> groups = GroupController.getGroupsFromSearch(query);			
			for (GroupDetails group : groups) {
				StringBuilder groupBox = new StringBuilder();
				groupBox.append("\t<div class=\"groupBox\">\n");
				groupBox.append("\t\t<a href=\"ViewGroup?id=" + group.getGroupID() + "\" title=\"View " + group.getGroupName() + "\">\n");
				groupBox.append("\t\t\t<img src=\"img/users.png\" alt=\"" + group.getGroupName() + " Logo\" />\n");
				groupBox.append(group.getGroupName() + "\n");
				groupBox.append("\t\t</a>\n");
				groupBox.append("\t\t</div>\n");
				
				result += groupBox.toString();
			}
		}
		
		query = request.getParameter("fSearch");
		
		if (query == null || query.isEmpty()) {
			query = "";
		} else {
			String date = request.getParameter("date");
			ArrayList<Food> food = DietCapture.searchFoodItems(query);			
			for (Food f : food) {
				StringBuilder exerciseRow = new StringBuilder();
				exerciseRow.append("\t<tr>\n");
				exerciseRow.append("\t\t<td>" + f.getName() + "</td>\n");
				exerciseRow.append("\t\t<td>" + f.getCals() + "</td>");
				exerciseRow.append("\t\t<td>" + f.getDescription() + "</td>");
				exerciseRow.append("\t\t<td>");
				exerciseRow.append("\t\t\t<form action=\"\" method=\"get\">");
				exerciseRow.append("\t\t\t\t<input type=\"hidden\" name=\"do\" value=\"addFood\" />");
				exerciseRow.append("\t\t\t\t<input type=\"hidden\" name=\"d\" value=\"" + date + "\" />");
				exerciseRow.append("\t\t\t\t<input type=\"hidden\" name=\"fItem\" value=\"" + f.getFoodID() + "\" />");
				exerciseRow.append("\t\t\t\t<select name=\"mType\" id=\"mType\">");
				exerciseRow.append("\t\t\t\t\t<option value=\"b\">Breakfast</option>");
				exerciseRow.append("\t\t\t\t\t<option value=\"l\">Lunch</option>");
				exerciseRow.append("\t\t\t\t\t<option value=\"d\">Dinner</option>");
				exerciseRow.append("\t\t\t\t\t<option value=\"s\">Snack</option>");
				exerciseRow.append("\t\t\t\t<select>");
				exerciseRow.append("\t\t\t\t<input type=\"text\" name=\"fQty\" />");
				exerciseRow.append("\t\t\t\t<input type=\"submit\" value=\"Add\" class=\"submit\" />\n");
				exerciseRow.append("\t\t\t</form>\n\t\t</td>\n");
				exerciseRow.append("\t</tr>\n");
				
				result += exerciseRow.toString();
			}
		}
		
		query = request.getParameter("exSearch");
		
		if (query == null || query.isEmpty()) {
			query = "";
		} else {
			String date = request.getParameter("date");
			ArrayList<Exercise> exercises = ExerciseCapture.searchApproved(query);			
			for (Exercise ex : exercises) {
				StringBuilder exerciseRow = new StringBuilder();
				exerciseRow.append("\t<tr>\n");
				exerciseRow.append("\t\t<td>" + ex.getName() + "</td>\n");
				exerciseRow.append("\t\t<td>" + ex.getCalsPerUnit() + "</td>");
				exerciseRow.append("\t\t<td>");
				exerciseRow.append("\t\t\t<form action=\"\" method=\"get\">");
				exerciseRow.append("\t\t\t\t<input type=\"hidden\" name=\"do\" value=\"addExercise\" />");
				exerciseRow.append("\t\t\t\t<input type=\"hidden\" name=\"d\" value=\"" + date + "\" />");
				exerciseRow.append("\t\t\t\t<input type=\"hidden\" name=\"eType\" value=\"" + ex.getExID() + "\" />");
				exerciseRow.append("\t\t\t\t<input type=\"text\" name=\"eDur\" />");
				exerciseRow.append("\t\t\t\t<input type=\"submit\" value=\"Add\" class=\"submit\" />\n");
				exerciseRow.append("\t\t\t</form>\n\t\t</td>\n");
				exerciseRow.append("\t</tr>\n");
				
				result += exerciseRow.toString();
			}
		}
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
