package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import healthTracker.utils.Security;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PersonalSettings
 */
@WebServlet("/PersonalSettings")
public class PersonalSettings extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatch;

		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {

			SystemUser usr = (SystemUser) request.getSession().getAttribute(
					"user");

			// Did they fully complete the register process?
			if (UserManager.hasEnteredDiet(usr.getUserID())) {
				dispatch = getServletContext().getRequestDispatcher(
						"/WEB-INF/personal_details.jsp");
				dispatch.forward(request, response);

				// If not, we need their current weight and height
			} else {
				response.sendRedirect("RegisterBMI");
			}

			// The user isn't logged in, so send them to the login page
		} else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// If they aren't logged in, send them to the login page
		if (request.getSession().getAttribute("user") == null) {
			response.sendRedirect("Login");
		} else {
			// Check if the confirm delete button was pressed
			String isDeletingAccount = request.getParameter("confirm");

			SystemUser usr = (SystemUser) request.getSession().getAttribute(
					"user");

			// If this is not null, they want to delete their account
			if (isDeletingAccount != null) {

				// Delete their account and remove the session
				UserManager.removeUser(usr.getUserID());
				HttpSession session = request.getSession();
				session.invalidate();
				request.getRequestDispatcher("/Index").forward(request,
						response);

			} else {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd");
				Calendar dateOfBirth = null;
				
				// They don't want to delete their account
				String password = request.getParameter("password");

				if ((password != null && !password.isEmpty())
						|| UserManager.checkCredentials(usr.getEmail(),
								password)) {

					String firstName = request.getParameter("first_name");
					String surname = request.getParameter("surname");
					String dob = request.getParameter("dob");
					String county = request.getParameter("county");
					String email = request.getParameter("email");
					String sex = request.getParameter("sex");
					String newPass = request.getParameter("newPassword");
					String newPassC = request.getParameter("newPasswordC");
					boolean isMale = false;

					// Check if all of the fields were entered
					if (firstName == null || firstName.isEmpty()) {
						firstName = usr.getForename();
					}
					if (surname == null || surname.isEmpty()) {
						surname = usr.getForename();
					}
					if (dob == null || dob.isEmpty()) {
						usr.getDateOfBirth();
					} else {
						// Check if the date is in the correct format
						try {
							dateFormat.parseObject(dob);
							dateOfBirth = dateFormat.getCalendar();
						} catch (ParseException e) {
							dateOfBirth = null;
						}
					}
					if (county == null || county.isEmpty()) {
						county = usr.getCounty();
					}
					if (email == null || email.isEmpty()) {
						email = usr.getEmail();
					}
					if (sex == null || sex.isEmpty()) {
						isMale = usr.getSex();
					} else {
						if (sex.equals("m")) {
							isMale = true;
						} else {
							isMale = false;
						}
					}
					if ((newPass == null || newPass.isEmpty())
							&& (newPassC == null || newPassC.isEmpty())) {
						password = usr.getPassword();
					} else {
						password = Security.hashSHA1(newPass);
					}

					/*
					 * If this is not null, then the date of birth was entered
					 * in the correct format.
					 */
					if (dateOfBirth != null) {
						// Check if the email already exists in the database
						if (usr.getEmail().equals(email)
								|| !UserManager.doesEmailExist(email)) {

							// Update the user's details
							UserManager.updateUserDetails(usr.getUserID(),
									email, password, firstName, surname,
									county, dateOfBirth, isMale);
							
							HttpSession userSession = request.getSession(true);
							SystemUser user = UserManager.getUserFromID(usr
									.getUserID());
							userSession.setAttribute("user", user);
							
							response.sendRedirect("Profile");

							// The email does exist, so show an error message.
						} else {
							String type = "E-Mail Already Exists";
							String message = "An account already exists with this e-mail address.";
							request.setAttribute("errType", type);
							request.setAttribute("errMsg", message);
							request.getRequestDispatcher("WEB-INF/error.jsp")
									.forward(request, response);
						}
					} else {
						/* It was null so DOB was wrong. Show an error
					 		message. */
						String type = "Invalid Date of Birth";
						String message = "The date of birth entered was not valid.";
						request.setAttribute("errType", type);
						request.setAttribute("errMsg", message);
						request.getRequestDispatcher("WEB-INF/error.jsp")
								.forward(request, response);
					}
				} else {
					/* One (or more) of the required fields was blank, show
						an error message. */
					String type = "Incorrect Password!";
					String message = "You must enter the correct old password to make changes!";
					request.setAttribute("errType", type);
					request.setAttribute("errMsg", message);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(
							request, response);
				}
			}
		}
	}
}
