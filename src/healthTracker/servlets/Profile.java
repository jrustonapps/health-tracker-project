package healthTracker.servlets;

import healthTracker.captures.DietCapture;
import healthTracker.goal.Goal;
import healthTracker.goal.GoalManager;
import healthTracker.user.SystemUser;
import healthTracker.user.UserActivities;
import healthTracker.user.UserHistory;
import healthTracker.user.UserManager;
import healthTracker.utils.Conversions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {

			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");

			// Did they fully complete the register process?
			if (UserManager.hasEnteredDiet(usr.getUserID())) {
				/*
				 * If the User has completed everything and this is a GET
				 * request. Work out which User they want to view.
				 */
				SystemUser view = null;
				String action = request.getParameter("do");
				if (action != null && !action.isEmpty()
						&& action.equals("viewUser")) {
					String userID = request.getParameter("id");
					if (userID.isEmpty() || userID == null) {
						/* If an unknown ID is used. */
						String errType = "No User ID Provided!";
						String errMsg = "You must provide a user ID to view their profile.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("/WEB-INF/error.jsp")
								.forward(request, response);
						return;
					}
					/* Attempt to find a user with this ID. */
					view = UserManager.getUserFromID(Integer.parseInt(userID));
					if (view == null) {
						/* If an unknown ID is used. */
						String errType = "Unkown User!";
						String errMsg = "The user ID you provided is not valid. "
								+ "Please try a valid user ID.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						request.getRequestDispatcher("/WEB-INF/error.jsp")
								.forward(request, response);
						return;
					} else if (view.getUserID() == usr.getUserID()) {
						/* Just in case the specified user is the current user. */
						view = usr;
					}
				} else {
					/* If there are no arguments, show the users own profile. */
					view = usr;
				}
				/* Start getting profile details now. */
				UserHistory userHistory = UserManager.getLastHistory(view.getUserID());

				if (userHistory == null) {
					/* If there is no UserHistory, their profile is invalid. */
					String errType =  view.getForename() + " " + view.getSurname();
					String errMsg = "This user hasn't completed their profile yet, so there's nothing to show.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
					return;
				}
				
				double weight = userHistory.getWeight();
				
				// Calculate their current BMI
				double bmi = Conversions.roundTwoDecimals(
						Conversions.calculateBMI(userHistory.getWeight(),
								userHistory.getHeight() / 100));

				double needsToLose;
				String wordToUse = "", timePeriod = "";

				// Is the user underweight?
				if (bmi < 18.5) {

					// Find out how much they need to gain
					needsToLose = userHistory.getWeight()
							- Conversions.bmiAndHeightToWeight(18.5,
									userHistory.getHeight() / 100);
					wordToUse = "gain";
					needsToLose *= -1;
					request.setAttribute("needsToChangeWeight", true);

					// Maybe they're overweight?
				} else if (bmi > 24.9) {

					// Find out how much they need to lose
					needsToLose = userHistory.getWeight()
							- Conversions.bmiAndHeightToWeight(24.9,
									userHistory.getHeight() / 100);
					wordToUse = "lose";
					request.setAttribute("needsToChangeWeight", true);

					// They aren't either, so their weight is fine.
				} else {

					// They don't need to lose anything
					needsToLose = 0;
					request.setAttribute("needsToChangeWeight", false);
				}

				Calendar calendar = Calendar.getInstance();
				int hours = calendar.get(Calendar.HOUR_OF_DAY);

				// Is it currently the morning?
				if (hours > 0 && hours < 12) {
					timePeriod = "morning";

					// Maybe it's the afternoon?
				} else if (hours >= 12 && hours < 17) {
					timePeriod = "afternoon";

					// No, it's the evening then.
				} else {
					timePeriod = "evening";
				}

				String measurementType = usr.getMeasurementPreference();
				
				/* Get the last seven measurements the user took of their
					weight. */
				ArrayList<Double> previousWeight = UserManager.getPreviousWeights(view.getUserID(), 7);
				
				/* Work out the users previous BMI readings before we fix the
				 	weights. */
				ArrayList<Double> previousBMI = new ArrayList<Double>();

				/* Calculate the BMI of the user based on these seven 
					measurements. */
				for (int i = 0; i < previousWeight.size(); i++) {
					previousBMI.add(Conversions.calculateBMI(previousWeight.get(i),
							UserManager.getLastHistory(view.getUserID()).getHeight() / 100));
				}

				// If the user uses imperial, we need to convert our
				// measurements to it
				if (measurementType.contains("i")) {

					// Calculate the total number of pounds the user weights
					int totalPounds = (int) Math.round(Conversions.kgToPound(weight));

					// Work out the number of st + lbs.
					int pounds = totalPounds % 14;
					int stone = (totalPounds - pounds) / 14;

					// Again, calculate the number of pounds the user needs to lose
					int totalPoundsToLose = (int) Math.round(Conversions.kgToPound(needsToLose));

					// Work out the number in st + lbs
					int poundsToLose = totalPoundsToLose % 14;
					int stoneToLose = (totalPoundsToLose - poundsToLose) / 14;

					// Set those attributes, homie.
					request.setAttribute("weight", Conversions.roundTwoDecimals(stone) 
							+ "st " + Conversions.roundTwoDecimals(pounds));
					request.setAttribute("needsToLose", Conversions.roundTwoDecimals(stoneToLose) 
							+ "st " + Conversions.roundTwoDecimals(poundsToLose));

					/* Convert the weight for the graph to the right unit. */
					for (int i = 0; i < previousWeight.size(); i++) {
						double convWeight = Conversions.kgToPound(previousWeight.get(i));
						previousWeight.set(i, Conversions.roundTwoDecimals(convWeight));
					}

					measurementType = "lbs";
				} else {
					measurementType = "kg"; // Set the display text for this unit.
					request.setAttribute("weight", Conversions.roundTwoDecimals(weight));
					request.setAttribute("needsToLose", Conversions.roundTwoDecimals(needsToLose));
				}

				/* Send all these measurements to the profile. */
				request.setAttribute("bmi", bmi);
				request.setAttribute("wordToUse", wordToUse);
				request.setAttribute("timePeriod", timePeriod);
				request.setAttribute("measurementType", measurementType);

				/* Reverse the collections so they can be used in the correct
				 	order. */
				Collections.reverse(previousBMI);
				Collections.reverse(previousWeight);
				/* Work out the smallest and largest BMI for graphing purposes. */
				double smallestBMI, largestBMI;
				smallestBMI = largestBMI = previousBMI.get(0);
				double smallestWeight, largestWeight;
				smallestWeight = largestWeight = previousWeight.get(0);
				
				for (int i = 1; i < previousBMI.size(); i++) {
					if (previousBMI.get(i) < smallestBMI) {
						smallestBMI = previousBMI.get(i);
					} else if (previousBMI.get(i) > largestBMI) {
						largestBMI = previousBMI.get(i);
					}
				}
				smallestBMI = Conversions.roundTwoDecimals(smallestBMI - 5);
				largestBMI = Conversions.roundTwoDecimals(largestBMI + 5);
				/* Work out the same thing for weights. */
				for (int i = 1; i < previousWeight.size(); i++) {
					if (previousWeight.get(i) < smallestWeight) {
						smallestWeight = previousWeight.get(i) - 5;
					} else if (previousWeight.get(i) > largestWeight) {
						largestWeight = previousWeight.get(i) + 5;
					}
				}
				smallestWeight = Conversions.roundTwoDecimals(smallestWeight - 5);
				largestWeight = Conversions.roundTwoDecimals(largestWeight + 5);
				
				/* Send the measurements and BMI calculated for them to the
				 	profile. */
				request.setAttribute("sWeight", smallestWeight);
				request.setAttribute("lWeight", largestWeight);
				request.setAttribute("previousWeight", previousWeight);
				request.setAttribute("sBMI", smallestBMI);
				request.setAttribute("lBMI", largestBMI);
				request.setAttribute("previousBMI", previousBMI);
				request.setAttribute("viewUser", view);

				/* Get the active goals for this user. */
				ArrayList<Goal> allGoals = GoalManager.getUserGoals(view.getUserID());
				ArrayList<Goal> activeGoals = new ArrayList<Goal>();
				Date today = Calendar.getInstance().getTime();
				for (Goal g : allGoals) {
					/* Check if the Goal is active or not. */
					if (!g.isCompleted() && !g.getGoalDate().before(today)) {
						g = healthTracker.goal.GoalManager.updateGoal(g);
						/* Format the values inside the goal properly. */
						if (g.isWeightGoal()) {
							/* Convert the values of weight goals to the right units. */
							if (usr.getMeasurementPreference().contains("i")) {
								g.setStart(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getStart())));
								g.setTarget(Conversions.roundTwoDecimals(Conversions.kgToPound(g.getTarget())));
							} else {
								g.setStart(Conversions.roundTwoDecimals(g.getStart()));
								g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
							}
						} else {
							/* Convert the units for exercise goals. */
							g.setStart(Conversions.roundTwoDecimals(g.getStart()));
							g.setTarget(Conversions.roundTwoDecimals(g.getTarget()));
						}
						
						activeGoals.add(g); // Add the goal to activeGoals.
					}
				}

				ArrayList<UserActivities> lastConsumed = new ArrayList<UserActivities>();

				for (Date date : UserManager.getDatesLastEnteredInUserActivities(view.getUserID(), 5)) {
					lastConsumed.add(DietCapture.getFoodAndExerciseOnDate(date, view.getUserID()));
				}

				request.setAttribute("activeGoals", activeGoals);
				request.setAttribute("lastConsumed", lastConsumed);

				request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
			} else {
				// If not, we need their current weight and height
				response.sendRedirect("RegisterBMI");
			}
			// The user isn't logged in, so send them to the login page
		} else {
			response.sendRedirect("Login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// No POST method for this Servlet.
	}
}
