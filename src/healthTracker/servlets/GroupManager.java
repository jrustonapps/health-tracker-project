package healthTracker.servlets;

import healthTracker.group.GroupController;
import healthTracker.user.SystemUser;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GroupManager
 */
@WebServlet("/GroupManager")
public class GroupManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
			
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
							
			request.setAttribute("myGroups", GroupController.getGroupsFromUser(usr.getUserID()));
					
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/group_manager.jsp");
			dispatch.forward(request, response);
				
		// The user isn't logged in, send them to the admin page
		} else {
			response.sendRedirect("Login");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/group_manager.jsp");
		dispatch.forward(request, response);
	}

}
