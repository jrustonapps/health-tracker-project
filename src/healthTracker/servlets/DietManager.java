package healthTracker.servlets;

import healthTracker.captures.DietCapture;
import healthTracker.user.SystemUser;
import healthTracker.user.UserActivities;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DietManager
 * 
 * @author 6266215
 * @version 20/03/2013
 */
@WebServlet("/DietManager")
public class DietManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		/* Check if the user is logged in or not. */
		if (request.getSession().getAttribute("user") == null) {
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
			dispatch.forward(request, response);
		} else {
			/* If they are, they can use this feature. First get the User */
			SystemUser user = (SystemUser)request.getSession().getAttribute("user");
			/* Get the current date, this can be overridden later. */
			Date sqlDate = new Date(Calendar.getInstance().getTime().getTime());
			String action = request.getParameter("do");
			if (action != null && !action.isEmpty()) {
				/* Work out which action the user is performing. */
				if (action.equals("show")) {
					/* If the user is trying to display the details for a date, get the date. */
					String date = request.getParameter("d");
					/* Ensure a date was actually provided. */
					if (date != null && !date.isEmpty()) {
						
						try {
							dateFormat.parse(date);
						} catch (ParseException e) {
							/* If the date fails to parse for some reason. */
							String errType = "Invalid Date Format!";
							String errMsg = "The date you entered could not be understood, please use 'dd/mm/yyyy'.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
							dispatch.forward(request, response);
							return;
						}
						sqlDate = new Date(dateFormat.getCalendar().getTime().getTime());
						Date today = new Date(Calendar.getInstance().getTime().getTime());
						if (sqlDate.after(today)) {
							/* Should the user enter a future date. */
							boolean futDate = true;
							request.setAttribute("futDate", futDate);
							sqlDate = today;
						}
					} else {
						/* If a date isn't provided, show this error. */
						String errType = "No date provided!";
						String errMsg = "You must select a date to view dietary history.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
				} else if (action.equals("addFood")) {
					
					String mealType = request.getParameter("mType");
					String fItem = request.getParameter("fItem");
					String quant = request.getParameter("fQty");
					String day = request.getParameter("d");
					
					int foodItem, quantity;
					
					try {
						foodItem = Integer.valueOf(fItem);
					} catch (Exception e) {
						String errType = "Food doesn't exist!";
						String errMsg = "The food you chose doesn't seem to exist.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}					
					try {
						quantity = Integer.valueOf(quant);
					} catch (Exception e) {
						String errType = "Incorrect quantity!";
						String errMsg = "You must use valid numbers for the portions eaten!";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
					
		            Calendar dateOfEntry;
		    
		            // Check if the date is in the correct format
		            try {
						dateFormat.parseObject(day);
						dateOfEntry = dateFormat.getCalendar();
						sqlDate = new Date(dateOfEntry.getTime().getTime());
					} catch (ParseException e) {
						String errType = "Invalid date!";
						String errMsg = "The date being used it invalid..";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
					
					if ((mealType!=null && !mealType.isEmpty()) &&
							(foodItem>0) && (quantity>0)) {
						
						int userActivitiesID = DietCapture.getUserActivitiesForDate(user.getUserID(), dateOfEntry);						
						// Is this part of a meal
						if (mealType.equals("b") || mealType.equals("l") || mealType.equals("d")) {
							
							int mealID = DietCapture.getSetMealForDetails(userActivitiesID, mealType);							
							DietCapture.addItemToSetMeal(foodItem, mealID, quantity, user.getUserID(), sqlDate);							
						// It must be a snack then, so it's individual
						} else {
							DietCapture.addFoodItemConsumed(foodItem, sqlDate, user.getUserID(), quantity);
						}
					} else {
						String errType = "Error!";
						String errMsg = "Something broke.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
				} else if (action.equals("remove")) {
									
					String id = request.getParameter("id");
					String date = request.getParameter("d");
					String meal = request.getParameter("m");
					
					if (meal==null || meal.isEmpty()) {
						meal = "";
					}
					
					if (id != null && !id.isEmpty()
							&& date != null && !date.isEmpty()) {
						int foodID = Integer.parseInt(id);
						try {
							dateFormat.parse(date);
						} catch (ParseException e) {
							/* If the date fails to parse for some reason. */
							String errType = "Invalid Date Format!";
							String errMsg = "The date you entered could not be understood, please use 'dd/mm/yyyy'.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
							dispatch.forward(request, response);
							return;
						}
						sqlDate = new Date(dateFormat.getCalendar().getTime().getTime());
						DietCapture.removeFoodItem(foodID, sqlDate, user.getUserID(), meal);
					}
				}
			}
			UserActivities act = DietCapture.getFoodOnDate(sqlDate, user.getUserID());
			/* Just check that the UserActivities actually exists. */
			if (act != null) {
				request.setAttribute("foodHist", act);
			} else {
				/* If the UserActivities is null for some reason. */
				String errType = "Could Not Get History!";
				String errMsg = "There was an error retrieving your user history, try again later.";
				request.setAttribute("errType", errType);
				request.setAttribute("errMsg", errMsg);
				RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
				dispatch.forward(request, response);
				return;
			}
			request.setAttribute("date", sqlDate);
			
			request.setAttribute("foodItems", DietCapture.getFoodItems());
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/diet_manager.jsp");
			dispatch.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("do");
		
		if (action != null && !action.isEmpty()) {
			if (action.equals("add")) {
			
				String foodName = request.getParameter("foodName");
				String measurement = request.getParameter("measurement");
				String portionS = request.getParameter("portion");
				String desc = request.getParameter("desc");
				String calsS = request.getParameter("calories");
				
				int portionSize, cals;
				
				try {
					portionSize = Integer.valueOf(portionS);
				} catch (NumberFormatException e) {
					String errType = "Invalid portion size!";
					String errMsg = "Please only enter numbers for portions.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				
				try {
					cals = Integer.valueOf(calsS);
				} catch (NumberFormatException e) {
					String errType = "Invalid calories!";
					String errMsg = "Please only enter whole numbers for calories.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				
				// Were all of the fields entered correctly?
				if ((foodName != null && !foodName.isEmpty()) &&
						(measurement != null && !measurement.isEmpty()) &&
						(cals > 0) && (portionSize > 0)) {
					
					foodName = foodName+" ("+portionSize+measurement+")";
				
					DietCapture.addFoodItem(foodName, cals, desc);
				} else {
					/* No action actually provided. */
					String errType = "Empty field!";
					String errMsg = "You must enter something in all required fields.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
			}
		} else {
			/* No action actually provided. */
			String errType = "Invalid action!";
			String errMsg = "You provided an action we don't know. Sorry!";
			request.setAttribute("errType", errType);
			request.setAttribute("errMsg", errMsg);
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
			dispatch.forward(request, response);
			return;
		}
		response.sendRedirect("DietManager");
	}

}
