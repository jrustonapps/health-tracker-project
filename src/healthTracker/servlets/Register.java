package healthTracker.servlets;

import healthTracker.user.SystemUser;
import healthTracker.user.UserManager;
import healthTracker.utils.Mail;
import healthTracker.utils.Security;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Register
 */
@WebServlet(description = "Register page for the Health Tracker.", urlPatterns = { "/Register" })
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/register.jsp");
		dispatch.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String firstName = request.getParameter("firstName");
		String surname = request.getParameter("surname");
		String dob = request.getParameter("dob");
		String county = request.getParameter("county");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("PasswordConfirmation");
		String sex = request.getParameter("sex");
		boolean isMale = false;
		
		if (sex != null && !sex.isEmpty()) {
			if (sex.equals("m")) {
				isMale = true;
			} else {
				isMale = false;
			}
		}
		
		// Check if all of the fields were entered
		if ((firstName != null && !firstName.isEmpty()) &&
				(surname != null && !surname.isEmpty()) &&
				(dob != null && !dob.isEmpty()) &&
				(county != null & !county.isEmpty()) &&
				(email != null && !email.isEmpty()) &&
				(password != null && !password.isEmpty()) &&
				(passwordConfirmation != null && !password.isEmpty()) &&
				(sex != null && !sex.isEmpty())) {
			
			// Does the password entered match the password confirmation?
			if (password.equals(passwordConfirmation)) {
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Calendar dateOfBirth;
        
                // Check if the date is in the correct format
                try {
					dateFormat.parseObject(dob);
					dateOfBirth = dateFormat.getCalendar();
				} catch (ParseException e) {
					dateOfBirth = null;
				}
                
                // If this is not null, then the date of birth was entered in the
                // Correct format
                if (dateOfBirth != null) {
                
                	// Check if the email already exists in the database
                	if (!UserManager.doesEmailExist(email)) {
                		
                		// If the e-mail address is in the valid format, we can proceed
                		if (Mail.isValidEmailAddress(email)) {
                			// Add the user to the database, but we still need weight + height.
                    		// This is done on another form.
                    		UserManager.createUser(email, password, firstName, 
                    				surname, county, dateOfBirth, isMale);
                    		
                    		SystemUser user = UserManager.findUserByEmail(email);
                    		
                    		String hashedEmail = Security.hashSHA1(email);
            				
                    		// Prepare our e-mail message
                    		String[] emailSubject = {"Health Tracker Verification"};
                    		String[] emailMessage = {"Dear "+firstName+",\n\nThank you for "+
                    		"creating an account on the health tracker system. Now all you "+
                    		"need to do is verify your account. To do so, click the link "+
                    		"below:\n\n http://localhost:8080/Health_Tracker/VerifyEmail?id="+user.getUserID()+"&key="+hashedEmail+
                    		"\n\nThanks,\nSystem Admins"};
                    		String[] emailArray = {email};
                    		
                    		// Send the user an e-mail
                    		Mail.sendMail(emailArray, emailSubject, emailMessage);
                    		
                    		// Show the confirmation page
                    		String type = "Account Created!";
            				String message = "Your account has been created. Please check your e-mails for the confirmation e-mail. Please also check your junk e-mails.";
            				request.setAttribute("errType", type);
            				request.setAttribute("errMsg", message);
            				request.getRequestDispatcher("WEB-INF/success.jsp").forward(request, response);
            				
            			// The e-mail address is invalid, so show an error.
                		} else {
                			String type = "Invalid E-Mail";
            				String message = "The e-mail address entered is invalid.";
            				request.setAttribute("errType", type);
            				request.setAttribute("errMsg", message);
            				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
                		}
                		
                	// The email does exist, so show an error message	
                	} else {
                		String type = "E-Mail Already Exists";
        				String message = "An account already exists with this e-mail address.";
        				request.setAttribute("errType", type);
        				request.setAttribute("errMsg", message);
        				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
                	}
                	
                // It was null so DOB was wrong. Show an error message.
                } else {
                	String type = "Invalid Date of Birth";
    				String message = "The date of birth entered was not valid.";
    				request.setAttribute("errType", type);
    				request.setAttribute("errMsg", message);
    				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
                }
				
			// They don't match, so show an error message	
			} else {
				String type = "Passwords do not match!";
				String message = "The password and the password confirmation do not match.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
			}
 			
		// One (or more) of the required fields was blank, show an error message.	
		} else {
			String type = "Incomplete information!";
			String message = "Required field was not entered.";
			request.setAttribute("errType", type);
			request.setAttribute("errMsg", message);
			request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
		}
		
	}

}
