package healthTracker.servlets;

import healthTracker.Administration;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PerformAdminAction
 */
@WebServlet("/PerformAdminAction")
public class PerformAdminAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String doAction = request.getParameter("do");
		String tempId = request.getParameter("id");
		int id;
		
		try {
			id = Integer.parseInt(tempId);
		} catch (Exception e) {
			id = 0;
		}
		
		// Check to see if the action and the ID is not valid
		if ((doAction==null || doAction.isEmpty()) &&
				(id > 0)) {
			String type = "Invalid Request";
			String message = "The ID or action was invalid.";
			request.setAttribute("errType", type);
			request.setAttribute("errMsg", message);
			request.getRequestDispatcher("WEB-INF/error.jsp").forward(
					request, response);
			
		// They are valid, so continue
		} else {
			
			// Does the admin want to delete a user?
			if (doAction.equals("removeUser")) {
				Administration.deleteUser(id);
				
			// Maybe they want to delete a group instead?
			} else if (doAction.equals("removeGroup")) {
				Administration.deleteGroup(id);
				
			// Accepting a new food item?
			} else if (doAction.equals("acceptFood")) {
				Administration.approveFoodItem(id);
				
			// Rejecting a new food item?
			} else if (doAction.equals("removeFood")) {
				Administration.removeFoodItem(id);
				
			// Accepting a new exercise item?
			} else if (doAction.equals("acceptExercise")) {
				Administration.approveExerciseItem(id);
				
			// Rejecting a new exercise item?
			} else if (doAction.equals("removeExercise")) {
				Administration.removeExerciseItem(id);
			}
			
			response.sendRedirect("AdminPanel");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}
