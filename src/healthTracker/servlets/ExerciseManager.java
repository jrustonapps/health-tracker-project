package healthTracker.servlets;

import healthTracker.captures.Exercise;
import healthTracker.captures.ExerciseCapture;
import healthTracker.user.SystemUser;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DietManager
 * 
 * @author 6102581
 * @version 20/03/2013
 */
@WebServlet("/ExerciseManager")
public class ExerciseManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		/* Check if the user is logged in or not. */
		if (request.getSession().getAttribute("user") == null) {
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
			dispatch.forward(request, response);
		} else {
			/* If they are, they can use this feature. First get the User */
			SystemUser user = (SystemUser)request.getSession().getAttribute("user");
			/* Get the current date, this can be overridden later. */
			Date sqlDate = new Date(Calendar.getInstance().getTime().getTime());
			String action = request.getParameter("do");
			if (action != null && !action.isEmpty()) {
				if (action.equals("show")){
					String chosenDate = request.getParameter("d");
					if (chosenDate != null && !chosenDate.isEmpty()) {	
						try {
							dateFormat.parse(chosenDate);
						} catch (ParseException e) {
							/* If the date fails to parse for some reason. */
							String errType = "Invalid Date Format!";
							String errMsg = "The date you entered could not be understood, please use 'dd/mm/yyyy'.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
							dispatch.forward(request, response);
							return;
						}
						sqlDate = new Date(dateFormat.getCalendar().getTime().getTime());
						Date today = new Date(Calendar.getInstance().getTime().getTime());
						if (sqlDate.after(today)) {
							/* Should the user enter a future date. */
							boolean futDate = true;
							request.setAttribute("futDate", futDate);
							sqlDate = today;
						}
					} else {
						/* If a date isn't provided, show this error. */
						String errType = "No date provided!";
						String errMsg = "You must select a date to view dietary history.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
				} else if (action.equals("remove")) {
					/* If the user wants to remove an existing exercise */
					String id = request.getParameter("id");

					int actID = Integer.parseInt(id);
					/* Get the date to remove the exercise from. */
					String chosenDate = request.getParameter("d");
					if (chosenDate != null && !chosenDate.isEmpty()) {
						try {
							dateFormat.parse(chosenDate);
						} catch (ParseException e) {
							/*If the date is in fact, not a thing*/
							String errType = "No date provided!";
							String errMsg = "You must select a date to view dietary history.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
							dispatch.forward(request, response);
							return;
						}
					}
					sqlDate = new Date(dateFormat.getCalendar().getTime().getTime());
					/* Remove the specified exercise. */
					ExerciseCapture.removeExerciseWithID(sqlDate, actID);

				} else if (action.equals("addExercise")){
					/* If a user wants to add a new exercise using the options */
					String chosenDate = request.getParameter("d");
					if (chosenDate != null && !chosenDate.isEmpty()) {
						try {
							dateFormat.parse(chosenDate);
						} catch (ParseException e) {
							/*If the date is in fact, not a thing*/
							String errType = "No date provided!";
							String errMsg = "You must select a date to view dietary history.";
							request.setAttribute("errType", errType);
							request.setAttribute("errMsg", errMsg);
							RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
							dispatch.forward(request, response);
							return;
						}
					}
					sqlDate = new Date(dateFormat.getCalendar().getTime().getTime());
					
					String exerciseID = request.getParameter("eType");
					String exerciseDur = request.getParameter("eDur");
					
					int exID = 0;
					float units = 0;
					
					try {
						exID = Integer.valueOf(exerciseID);
					} catch (NumberFormatException e) {
						String errType = "No exercise chosen!";
						String errMsg = "You must choose an exercise to add!";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
					
					try {
						units = Float.valueOf(exerciseDur);
					} catch (NumberFormatException e) {
						String errType = "Invalid value!";
						String errMsg = "Did you actually provide a number?";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
					/* Make sure the number is positive, can't use a negative can we now. */
					if (units <= 0) {
						String errType = "Negative value!";
						String errMsg = "You can't use negative values here. Please enter positive numbers.";
						request.setAttribute("errType", errType);
						request.setAttribute("errMsg", errMsg);
						RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
						dispatch.forward(request, response);
						return;
					}
					
					ExerciseCapture.addPerformedExercise(sqlDate, user.getUserID(), exID, units);
				}
			}
			/* Get the list of exercises for the current day. */
			ArrayList<Exercise> fullList = ExerciseCapture.getExerciseOnDate(sqlDate, user.getUserID());
			
			request.setAttribute("date", sqlDate);
			request.setAttribute("exerciseHist", fullList);
			request.setAttribute("exerciseList", ExerciseCapture.getAllApproved());

			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/exercise_manager.jsp");
			dispatch.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String action = request.getParameter("do");
		if (action != null && !action.isEmpty()) {
			/* What action has been given? */
			if (action.equals("add")) {
				/* Check the exercise name has been provided. */
				String name = request.getParameter("exerciseName");
				if (name == null || name.isEmpty()) {
					String errType = "No name provided!";
					String errMsg = "You must give an exercise a name.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				/* Check a calorie burn was provided. */
				String cals = request.getParameter("calories");
				if (cals == null || cals.isEmpty()) {
					String errType = "No calories provided!";
					String errMsg = "You must give a new exercise a calorie amount.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				
				int calsI = 0;
				/* Attempt to convert this to a usable number. */
				try {
					calsI = Integer.valueOf(cals);
				} catch (NumberFormatException e) {
					String errType = "Invalid number!";
					String errMsg = "Did you provide an actual number?";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				/* Just check that a positive value was entered here, */
				if (calsI < 0) {
					String errType = "Negative value!";
					String errMsg = "You can't use negative values for caloric burn.";
					request.setAttribute("errType", errType);
					request.setAttribute("errMsg", errMsg);
					RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
					dispatch.forward(request, response);
					return;
				}
				/* Get a description if one exists, we don't mind if it doesn't. */
				String description = request.getParameter("desc");
				
				ExerciseCapture.addExerciseItem(name, description, calsI, false); // Store the exercise.
			}
		} else {
			/* No action actually provided. */
			String errType = "Invalid action!";
			String errMsg = "You provided an action we don't know. Sorry!";
			request.setAttribute("errType", errType);
			request.setAttribute("errMsg", errMsg);
			RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
			dispatch.forward(request, response);
			return;
		}
		
		response.sendRedirect("ExerciseManager");
	}

}
