package healthTracker.servlets;

import healthTracker.Administration;
import healthTracker.group.GroupController;
import healthTracker.user.SystemUser;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminPanel
 */
@WebServlet("/AdminPanel")
public class AdminPanel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Check if the user is logged in
		if (request.getSession().getAttribute("user") != null) {
					
			SystemUser usr = (SystemUser) request.getSession().getAttribute("user");
			
			// Is the user an admin?
			if (usr.getAdmin()) {
				
				request.setAttribute("unapprovedFood", Administration.getUnapprovedFoods());
				request.setAttribute("unapprovedExercises", Administration.getUnapprovedExercises());
				request.setAttribute("users", Administration.getUsers());
				request.setAttribute("groups", GroupController.getGroupsFromSearch(""));
				
				RequestDispatcher dispatch = getServletContext().getRequestDispatcher("/WEB-INF/new_administration.jsp");
				dispatch.forward(request, response);
				
			// They aren't an admin, get them out of here!
			} else {
				String type = "Invalid Permissions";
				String message = "You do not have valid permissions to access this area.";
				request.setAttribute("errType", type);
				request.setAttribute("errMsg", message);
				request.getRequestDispatcher("WEB-INF/error.jsp").forward(
						request, response);
			}
		
		// The user isn't logged in, send them to the admin page
		} else {
			response.sendRedirect("Login");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendRedirect("new_administration.jsp");
	}

}
