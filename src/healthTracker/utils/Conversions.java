package healthTracker.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * A class of conversion methods to convert between imperial and metric 
 * measurements of various things.
 * 
 * @author 6266215
 * @version 17/03/2013
 */
public class Conversions {
	
	/**
	 * Calculates the Body Mass Index (BMI) for this User. Requires the weight 
	 * and height to be in KG and M respectively before computing.
	 * @return The users' BMI as a double.
	 */
	public static double calculateBMI(double weight, double height) {
		return roundTwoDecimals((weight/Math.pow(height, 2)));
	}
	
	/**
	 * Converts centimeters to inches.
	 * @param cm The length in centimeters as a double.
	 * @return The length in inches as a double.
	 */
	public static double cmToInch(double cm) {
		
		return roundTwoDecimals((cm * 0.3937));
	}
	
	/**
	 * Converts inches to centimeters.
	 * @param inch The length in inches as a double.
	 * @return The length in centimeters as a double.
	 */
	public static double inchToCM(double inch) {
		
		return roundTwoDecimals((inch * 2.54));
	}
	
	/**
	 * Converts to feet to centimeters.
	 * @param ft The length in feet as a double.
	 * @return The length in centimeters as a double.
	 */
	public static double feetToCM(double ft) {
		
		return roundTwoDecimals((ft * 30.48));
	}
	
	/**
	 * Converts centimeters to feet.
	 * @param cm The length in centimeters as a double.
	 * @return The length in feet as a double.
	 */
	public static double cmToFeet(double cm) {
		
		return roundTwoDecimals((cm * 0.032808));
	}
	
	/**
	 * Converts from Kilograms to Pounds.
	 * @param kg The weight in kilograms as a double.
	 * @return The weight in pounds as a double.
	 */
	public static double kgToPound(double kg) {
		
		return roundTwoDecimals((kg * 2.2046));
	}
	
	/**
	 * Converts from Pounds to Kilograms.
	 * @param pound The weight in points as a double.
	 * @return The weight in kilograms as a double.
	 */
	public static double poundToKg(double pound) {
		
		return roundTwoDecimals((pound / 2.2046));
	}
	
	/**
	 * Converts kilograms to stones.
	 * @param kg The weight in kilograms as a double.
	 * @return The weight in stones as a double.
	 */
	public static double kgToStone(double kg) {
		
		return roundTwoDecimals((kg * 0.15747));
	}
	
	/**
	 * Converts stones to kilograms.
	 * @param st The weight in stones as a double.
	 * @return The weight in kilograms as a double.
	 */
	public static double stoneToKG(double st) {
		
		return roundTwoDecimals((st / 0.15747));
	}
	
	/**
	 * Find the recommended weight based on a height and a BMI
	 * @param bmi The BMI to use
	 * @param height The height to use
	 * @return The recommended weight
	 */
	public static double bmiAndHeightToWeight(double bmi, double height) {
		double weight = bmi*Math.pow(height, 2);
		return roundTwoDecimals(weight);
	}
	
	/**
	 * Round a number to 2 decimal places
	 * @param d The number to round
	 * @return The resulting number
	 */
	public static double roundTwoDecimals(double d) {
		
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        twoDForm.setRoundingMode(RoundingMode.HALF_EVEN);
        return Double.valueOf(twoDForm.format(d));
	}
}
