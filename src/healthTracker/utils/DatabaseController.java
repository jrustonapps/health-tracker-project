package healthTracker.utils;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 * DatabaseController.java
 * 
 * Contains methods allowing a direct connection to the database. Using this class,
 * A connection to a PostgresSQL database can be made and queries can be executed.
 * 
 * @author 6183891, 6266215
 * @version 19/03/2013
 */
public class DatabaseController {
	
	Connection connection;
	
	/**
	 * Creates a new DatabaseController by initiating a connection to the database,
	 * thereby allowing us to run queries.
	 */
	public DatabaseController() {
		
		// Do we have the postgresSQL driver attached to the project?
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.err.println("No PostgresSQL Driver");
			e.printStackTrace();
		}
		
		connection = null;
		
		// Attempt a connection to the database using the login details supplied
		// Throw an exception if there is a problem.
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/postgres",
					"user", "");
		} catch (SQLException e) {
			System.err.println("Couldn't connect to database");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get the connection this DatabaseController is using.
	 * @return A Connection object for the connection.
	 */
	public Connection getConnection() {
		
		return this.connection;
	}
	
	/**
	 * Method to run an SQL query, with a result. Normally used for SELECT statements.
	 * @param query A Java PreparedStatement for the query to be run.
	 * @return Result obtained by running the SQL query.
	 */
	public ResultSet runQuery(PreparedStatement query) {
		
		ResultSet result = null;
		
		try {
			result = query.executeQuery();
		} catch (SQLException e) {
			System.err.println("Could not execute query");
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Method to run an update to the database with an SQL query. Used for UPDATE or
	 * INSERT statements. No result returned.
	 * @param query A Java PreparedStatement for the query to be run.
	 */
	public void runUpdate(PreparedStatement query) {
		
		try {
			query.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Could not execute update");
			e.printStackTrace();
		}
	}
	
	/**
     * Method to count the number of rows returned by a query
     * @param query A Java PreparedStatement for the query to be run.
     * @return The number of rows returned
     */
	public int numRows(PreparedStatement query) {
		
        ResultSet result = runQuery(query);
        int numOfRows = 0;
        
        try {
            while (result.next()) {
                numOfRows ++;
            }            
            result.close();
        } catch (SQLException e) {
            System.err.println("Could not count rows");
            e.printStackTrace();
        } finally {
        	result = null;
        }
        
        return numOfRows;
    }
	
	/**
	 * Method to close the SQL connection so that no more queries can be executed.
	 */
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			System.err.println("Unable to close connection!");
			e.printStackTrace();
		} finally {
			connection = null;
		}
	}

}
