package healthTracker.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Security.java
 * 
 * Contains static methods for security purposes in the system. Provides methods
 * for hashing, sanitising SQL input and form input.
 * 
 * @author 6266215
 * @version 17/03/2013
 */
public class Security {

	/**
	 * Generic hashing method that accepts any algorithm. The byte[] must be 
	 * handled elsewhere.
	 * @param input The String to be hashed.
	 * @param alg The algorithm to hash the String with.
	 * @return A byte array of the hashed String.
	 */
	public static byte[] hash(String input, String alg) {

		byte[] stringBytes = null;
		try {
			/* We need to make sure that whatever we put in uses a certain 
			   encoding, otherwise we get a party when converting to bytes. */
			stringBytes = input.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			/* Throw this at the off chance our system doesn't support UTF-8 */
			System.err.println("UTF-8 encoding not supported!");
			System.err.println("Falling back to ASCII encoding...");
			try {
				/* Fall back to ASCII (should be supported everywhere. */
				stringBytes = input.getBytes("ASCII");
			} catch (UnsupportedEncodingException e2) {
				/* Just give up if ASCII can't be used. The system should not run on this host. */
				System.err.println("ASCII encoding not supported, shutting down.");
				e2.printStackTrace();
				System.exit(-1);
			}
		}

		MessageDigest digest = null;
		if (stringBytes != null) {
			/* Check we can use the specified hashing algorithm. */
			try {
				digest = MessageDigest.getInstance(alg);
				digest.reset();
				digest.update(stringBytes);
			} catch (NoSuchAlgorithmException e) {
				/* If the hashing algorithm is not supported, return null. */
				System.err.println(alg + " algorithm not supported!");
				e.printStackTrace();
			}
		}

		byte[] digested = null;
		if (digest != null) {
			/* Hash the String. */
			digested = digest.digest();
			return digested;
		}
		/* Something, somewhere, went horribly wrong. Return null. */
		return null;
	}

	/**
	 * Hashes the provided String using MD5. Avoid using for password storage.
	 * @param input The String to hash with MD5.
	 * @return The MD5 hashed String.
	 */
	public static String hashMD5(String input) {

		byte[] hash = hash(input, "MD5"); // Hash the String.
		if (hash != null) {
			BigInteger bigInt = new BigInteger(1, hash);
			String text = bigInt.toString(16); // Convert to a String.
		
			while (text.length() < 32) {
				/* Keep appending hash until 32 characters is reached. */
				text += "0"+text;
			}
			return text; // Return the hashed String.
		}
		return null; // Something went wrong, return null.
	}

	/**
	 * Hashes the provided String using SHA-1.
	 * @param input The String to hash with SHA-1.
	 * @return The SHA-1 hashed String.
	 */
	public static String hashSHA1(String input) {

		byte[] hash = hash(input, "SHA-1");
		if (hash != null) {
			BigInteger bigInt = new BigInteger(1, hash);
			String text = bigInt.toString(16); // Convert to a String.
		
			while (text.length() < 32) {
				/* Keep appending hash until 32 characters is reached. */
				text += "0"+text;
			}
			return text; // Return the hashed String.
		}
		return null; // Something went wrong, return null.
	}
}
