package healthTracker.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

/**
 * Populates the database with dummy data for testing purposes.
 * 
 * @author 
 */
public class Populate {	
	
	static String[] maleForenames = {"Jake", "Jordan", "Thomas", "Willem", "Barry", "Paul", "Phil", "Henry", "Bob", "Sam"};
	static String[] femaleForenames = {"Jane", "Jackie", "Paula", "Helen", "Patricia", "Sam", "Emily", "Kat", "Philipa", "Anna"};
	static String[] surNames = {"Woerner", "Ruston", "Logan", "Lukins", "Phillips", "Pearce", "Jones", "Hellin-Fellson", "McCool-Name", "Butler"};
	static String[] food = {"Cake", "Chocolate", "Boiled Potato", "Sausage", "Beef", "Pork", "Chicken", "Tea", "Coffee", "Milk", "Biscuit"};
	static String[] exercise = {"Running", "Swimming", "Cycling", "Parkour", "Sumo", "Weight lifting", "Jogging", "Walking", "Rock Climbing", "Rowing"};
	
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws SQLException, ParseException {
		DatabaseController db = new DatabaseController();		
		Scanner input = new Scanner(System.in);
		List<Integer> UserIds = new ArrayList<Integer>();
		List<Integer> GroupIds = new ArrayList<Integer>();
		List<Integer> GoalIds = new ArrayList<Integer>();
		List<Integer> FoodIds = new ArrayList<Integer>();
		List<Integer> ExerciseIds = new ArrayList<Integer>();
		List<Integer> MealIds = new ArrayList<Integer>();
		List<Integer> UserActivityIds = new ArrayList<Integer>();
		ResultSet rs;
		Random random = new Random();
		PreparedStatement sql = null;
		String uuid;
		DateFormat df = DateFormat.getDateInstance();

		System.out.println("How many groups should be created?");
		int toAdd = input.nextInt();
		
		// Adds Groups
		for(int i = 0; i<toAdd; i++){
			uuid = UUID.randomUUID().toString().substring(0, 4);
			sql = db.getConnection().prepareStatement("INSERT INTO GroupDetails(groupName, groupDesc) VALUES (?,'A Test Group') RETURNING groupID;");
			sql.setString(1, uuid);
			rs = db.runQuery(sql);
			rs.next();
			GroupIds.add(Integer.valueOf(rs.getInt("groupID")));
		}
		
		System.out.println("Groups Successfully Added");

		System.out.println("How many users should entries be generated for?");
		toAdd = input.nextInt();
		
		// Adds Users
		for(int i = 0; i<toAdd; i++){
			String name = "";
			boolean male = true;
			if(random.nextInt(2) == 1){
				male = false;
			}
			if (male) {
				name = maleForenames[random.nextInt(maleForenames.length-1)];
			} else {
				name = femaleForenames[random.nextInt(femaleForenames.length-1)];
			}
			boolean verified = true;
			if(random.nextInt(2) == 1){
				verified = false;
			}
			uuid = Security.hashSHA1(UUID.randomUUID().toString().substring(0, 4));
			// Generates random date
			int day = random.nextInt(28) + 1;
			int month = random.nextInt(12) + 1;
			int year = random.nextInt(75) + 1900;
			String date = (day + "-" + month + "-" + year);
			
			java.util.Date d1 = new java.util.Date();
			java.sql.Date d2 = new java.sql.Date(d1.getTime());
			 DateFormat DOB = new SimpleDateFormat("dd-MM-yyy");
			    java.sql.Date convertedDate=null;
			    try {
			        convertedDate = new java.sql.Date(DOB.parse(date).getTime());
			    } catch (ParseException ex) {
			        ex.printStackTrace();
			    }
			    
			String email = ("DummyEmail@"+random.nextInt(10000)+".com");
			sql = db.getConnection().prepareStatement("INSERT INTO SystemUser(email,password,forename,surname,DoB,sexMale,county, verified) VALUES (?,?,?,?,?,?,'Norwich',?" +
					") RETURNING userID");
			String surname = surNames[random.nextInt(surNames.length-1)];
			sql.setString(1, email);
			sql.setString(2, uuid);
			sql.setString(3, name);
			sql.setString(4, surname);
			sql.setDate(5, convertedDate);
			sql.setBoolean(6, male);
			sql.setBoolean(7, verified);

			rs = db.runQuery(sql);
			rs.next();
			
			int currentID =Integer.valueOf(rs.getInt("userID"));
			UserIds.add(currentID);
			
			for(int j = 0; j<random.nextInt(3)+1; j++){
			int groupSelector =random.nextInt(GroupIds.size());
			int groupID = GroupIds.get(groupSelector);
			sql = db.getConnection().prepareStatement("INSERT INTO GroupUser VALUES(?,?);");
			sql.setInt(1, groupID);
			sql.setInt(2, currentID);
		    db.runUpdate(sql);
			}
			// Generates random date
		    day = random.nextInt(28) + 1;
			month = random.nextInt(12) + 1;
			year = random.nextInt(20) + 1990;
			date = (day + "-" + month + "-" + year);
			    try {
			        convertedDate = new java.sql.Date(DOB.parse(date).getTime());
			    } catch (ParseException ex) {
			        ex.printStackTrace();
			    }
			
			sql = db.getConnection().prepareStatement("INSERT INTO Goal(userID,goalDate,complete) VALUES (?,?,?) RETURNING goalID;");
			sql.setInt(1, currentID);
			sql.setDate(2, convertedDate);
			sql.setBoolean(3, !male);
		    rs = db.runQuery(sql);
		    rs.next();
		    
		    int currentGoalID = Integer.valueOf(rs.getInt("goalID"));
		    // Randomises male again, this boolean is seeing a lot of use
		    if(random.nextInt(2) == 1){
				male = false;
			}
		    
		    // Assign the goal as either a weight goal or a exercise goal
		    if(male){
		    	int startWeight = random.nextInt(200) + 50;
		    	int endWeight = startWeight - 50;
		    	sql = db.getConnection().prepareStatement("INSERT INTO WeightGoal(goalID,startingweight,goalweight) " +
		    			"VALUES (?,?,?);");
		    	sql.setInt(1, currentGoalID);
		    	sql.setInt(2, startWeight);
		    	sql.setInt(3, endWeight);
		    	db.runUpdate(sql);
		    }else{
		    	int goalDistance = random.nextInt(100) + 50;
		    	int goalProgress = goalDistance - 20;
		    	sql = db.getConnection().prepareStatement("INSERT INTO ExerciseGoal(goalID,exerciseProgress,goalDistance) VALUES (?,?,?);");
		    	sql.setInt(1, currentGoalID);
		    	sql.setInt(2, goalProgress);
		    	sql.setInt(3, goalDistance);
		    	db.runUpdate(sql);
		    }
		    
		    //Assign random goals to random groups
	    	 if(random.nextInt(2) == 1){
	    		 int groupID = GroupIds.get(random.nextInt(GroupIds.size()));
	    		 sql = db.getConnection().prepareStatement("INSERT INTO groupGoal VALUES (?,?);");
			    	sql.setInt(1, groupID);
			    	sql.setInt(2, currentGoalID);
	    		 db.runUpdate(sql);
				}
	    	 
	    	 //Generates three history entries for each user
	    	 for(int j = 0; j<3; j++){
	    		//Generates random date
	 			day = random.nextInt(28) + 1;
	 			month = random.nextInt(12) + 1;
	 			year = random.nextInt(75) + 1900;
	 			date = (day + "-" + month + "-" + year);
	 		    try {
			        convertedDate = new java.sql.Date(DOB.parse(date).getTime());
			    } catch (ParseException ex) {
			        ex.printStackTrace();
			    }
			
	 			int weight = random.nextInt(200) + 50;
	 			int height = random.nextInt(10) + 120;
	 			sql = db.getConnection().prepareStatement("INSERT INTO userhistory VALUES(?,?,?,?)");
	 			sql.setInt(1, currentID);
	 			sql.setDate(2, convertedDate);
	 			sql.setInt(3, weight);
	 			sql.setInt(4, height);
	 			db.runUpdate(sql);
	    	 }
	    	 
	    	 //Generates user activities
	    	//Generates random date
	    	 for(int j=0; j<random.nextInt(3)+1; j++){
	 			day = random.nextInt(28) + 1;
	 			month = random.nextInt(12) + 1;
	 			year = random.nextInt(20) + 1990;
	 			date= (day + "-" + month + "-" + year);
	 		    try {
			        convertedDate = new java.sql.Date(DOB.parse(date).getTime());
			    } catch (ParseException ex) {
			        ex.printStackTrace();
			    }
			
	 			sql = db.getConnection().prepareStatement("INSERT INTO useractivities(userid,actdate) VALUES(?,?)RETURNING actID;");
	 			sql.setInt(1, currentID);
	 			sql.setDate(2, convertedDate);		
				rs = db.runQuery(sql);
				rs.next();			
				int actID =Integer.valueOf(rs.getInt(1));
				UserActivityIds.add(actID);
	    	 }
		}
		System.out.println("**Users Added Successfully**");
		System.out.println("**Goals Added Successfully**");
		System.out.println("**Groups-Goal Added Successfully**");
		System.out.println("**Three UserHistory entries per user added**");
		System.out.println("**Random Activity events added added**");
		
		System.out.println("Hows many food items should be generated?");
		toAdd = input.nextInt();
		
		for(int i = 0; i<toAdd; i++){
			int cals = random.nextInt(1000) + 50;
			String name = food[random.nextInt(food.length-1)];
			String desc = ("This is a generated food item");
			boolean approved = true;
			if(random.nextInt(2) == 1){
				approved = false;
			}
			//Adds random food items
			sql = db.getConnection().prepareStatement("INSERT INTO food(name,cals,description,approved) VALUES (?,?,?,?) RETURNING foodID;");
			sql.setString(1, name);
			sql.setInt(2,cals);
			sql.setString(3, desc);
			sql.setBoolean(4, approved);
			rs = db.runQuery(sql);
			rs.next();
			FoodIds.add(rs.getInt(1));
		}
		
		System.out.println("**Food Added Successfully**");
		System.out.println("Hows many exercise items should be generated?");
		toAdd = input.nextInt();
		

		for(int i = 0; i<toAdd; i++){
			String name = exercise[random.nextInt(exercise.length-1)];
			int cals = random.nextInt(1000) + 50;
			String desc = ("This is a generated exercise item");
			boolean approved = true;
			if(random.nextInt(2) == 1){
				approved = false;
			}
			//Adds random exercise items
			sql = db.getConnection().prepareStatement("INSERT INTO exercise(name,calsperunit,description,approved) VALUES (?,?,?,?) RETURNING exerciseID;");
			sql.setString(1, name);
			sql.setInt(2,cals);
			sql.setString(3, desc);
			sql.setBoolean(4, approved);
			rs = db.runQuery(sql);
			rs.next();
			ExerciseIds.add(rs.getInt(1));
		}
		
		System.out.println("**Exercise Added Successfully**");
		System.out.println("Hows many meal items should be generated?");
		toAdd = input.nextInt();
		//Adds meal items
		for(int i = 0; i<toAdd; i++){
			uuid = UUID.randomUUID().toString().substring(0, 4);
			sql = db.getConnection().prepareStatement("INSERT INTO setmeal(name,description) VALUES(?,'This is a generated meal') RETURNING mealID;");
			sql.setString(1,uuid);
			
			rs = db.runQuery(sql);
			rs.next();
			MealIds.add(rs.getInt(1));
		}
		//Then assigns food to the meals
		//This might throw some error if it picks the same food item twice for one meal
		//This has no effect on the program, a meal will always have at least one item
		System.out.println("**Meals Added Successfully**");
		for(int i = 0; i < MealIds.size(); i++){
			for(int j = 0; j < random.nextInt(FoodIds.size() + 1); j++){
				int quantity = random.nextInt(5)+1;
				int mealID = MealIds.get(i);
				int foodID = FoodIds.get(random.nextInt(FoodIds.size()));
				sql = db.getConnection().prepareStatement("INSERT INTO foodinmeal VALUES(?,?,?);");
				sql.setInt(1, mealID);
				sql.setInt(2, foodID);
				sql.setInt(3, quantity);
				db.runUpdate(sql);			
			}
		}
		
		System.out.println("**Meals populated**");
		
		for(int x : UserActivityIds){
			//Associates random items with each user activity
			int foodID = FoodIds.get(random.nextInt(FoodIds.size()));
			int exerciseID = ExerciseIds.get(random.nextInt(ExerciseIds.size()));
			int mealID = MealIds.get(random.nextInt(MealIds.size()));
			int units = random.nextInt(5)+1;
			
			sql = db.getConnection().prepareStatement("INSERT INTO actfood VALUES(?,?,?);");
			sql.setInt(1, x);
			sql.setInt(2, foodID);
			sql.setInt(3, units);
			db.runUpdate(sql);
			
			units = random.nextInt(5)+1;
			sql = db.getConnection().prepareStatement("INSERT INTO actexercise VALUES(?,?,?);");
			sql.setInt(1, x);
			sql.setInt(2, exerciseID);
			sql.setInt(3, units);
			db.runUpdate(sql);	
			
			sql = db.getConnection().prepareStatement("INSERT INTO actmeal VALUES(?,?);");
			sql.setInt(1, x);
			sql.setInt(2,mealID);
			db.runUpdate(sql);	
		}
		
		input.close();
		
		System.out.println("**User Activity-Food Added**");
		System.out.println("**User Activity-Exercise Added**");
		System.out.println("**User Activity-Meal Added**");
		
		System.out.println("***** Program Successfully Completed *****");
	}

}
