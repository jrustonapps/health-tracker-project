package healthTracker.utils;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * A class to send some beautiful electronic mail messages to anyone delightful
 * Enough to join our website. It can in theory be used to send as many e-mails as we
 * Want to any recipient, including Joost if he decides to give us a low mark.
 * 
 * If you're reading this Joost, please ignore the comment above.
 * 
 * @author 6183891
 *
 */
public class Mail {

	/**
	 * This is a method which sends an e-mail through the Gmail account created.
	 * @param to The e-mail address to send to
	 * @param subject The subject of the e-mail
	 * @param message The body of the e-mail
	 * @return
	 */
	public static boolean sendMail(String[] to, String[] subject, String[] message) {
		
		final class EmailThread extends Thread {
			
			Session sess;
			String emailTo, subject, message;
			
			public EmailThread(Session sess, String emailTo, String subject, String message) {
				this.sess = sess;
				this.emailTo = emailTo;
				this.subject = subject;
				this.message = message;
			}
			
			public void run() {
				try {
					MimeMessage msg = new MimeMessage(sess);
	                msg.setText(message);
	                msg.setSubject(subject);
	                msg.setFrom(new InternetAddress("healthtrackersystem@gmail.com"));
	                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
	                
	                // Up, up and away!
	                Transport.send(msg);
				} catch (Exception e) {
		        	e.printStackTrace();
				}
			}
		}
		
		try {
			
			// Set up the server details
            Properties props = new Properties();
            props.setProperty("mail.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "true");

            // Login details for the e-mail
            Authenticator auth = new SMTPAuthenticator("healthtrackersystem@gmail.com", "JoostFTW");

            Session session = Session.getInstance(props, auth);
            int i=0;
            
            EmailThread[] threads = new EmailThread[to.length];
            
            // Loop through our users
            for (String emailTo : to) {
            	
            	EmailThread thread = new EmailThread(session, emailTo, subject[i], message[i]);
            	
            	threads[i] = thread;
            	threads[i].start();
            	i ++;
            }
            
            for (int j=0; j<to.length; j++) {
            	threads[j].join();
            }

        // Catch all the exceptions!
        } catch (Exception ex) {
        	ex.printStackTrace();
            return false;
        }
            
		return true;
	}
	
	/**
	 * This is just a static inner class used to manage the login details
	 * Of the e-mail account
	 * @author 6183891
	 *
	 */
	private static class SMTPAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
	
	/**
	 * Method to check if an email address is valid
	 * @param email
	 * @return
	 */
	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (Exception e) {
			result = false;
		}
		return result;
	}
}
