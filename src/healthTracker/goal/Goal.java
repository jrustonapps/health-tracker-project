package healthTracker.goal;

import java.util.Date;

/**
 * Goal.java
 * 
 * Stores information regarding a goal so it can be extended - The ID of the goal,
 * The date it must be completed by and whether it is completed or not is also stored.
 * 
 * @author 6183891
 */
public class Goal {
	
	private int id, userID;
	private Date creationDate, goalDate;
	private double start, target;
	private boolean canSubscribe;
	
	// Not in the class diagram, but says whether the goal is completed or not.
	// Added in SQL - 6102581
	private boolean completed, weightGoal;
	
	/**
	 * Creates a new Goal with no attributes.
	 */
	public Goal() {
		
		id = 0;
		userID = 0;
		creationDate = new Date();
		goalDate = new Date();
		start = 0.0;
		target = 0.0;
		completed = false;
		weightGoal = false;
		canSubscribe = true;
	}
	
	/**
	 * Creates a Goal with the specified attributes.
	 * @param id The GoalID for this Goal.
	 * @param creationDate When the Goal was created as a Date.
	 * @param goalDate Target date for the Goal as a Date.
	 * @param startVal The starting value for this Goal.
	 * @param targetVal The target value for this Goal.
	 * @param completed Is the Goal completed or not?
	 * @param goalType True if this Goal is a weight goal, false otherwise.
	 */
	public Goal(int id, int userID, Date creationDate, Date goalDate, 
			double startVal, double targetVal,
			boolean completed, boolean goalType) {
		
		this.id = id;
		this.userID = userID;
		this.creationDate = creationDate;
		this.start = startVal;
		this.target = targetVal;
		this.goalDate = goalDate;
		this.completed = completed;
		this.weightGoal = goalType;
		canSubscribe = true;
	}
	
	/**
	 * Gets the Goal ID number.
	 * @return The Goal ID as an integer.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Gets the UserID associated with this Goal.
	 * @return The UserID as an Integer.
	 */
	public int getUserID() {
		return userID;
	}
	
	/**
	 * Gets the Date this Goal was created.
	 * @return The creation date as a Date object.
	 */
	public Date getCreationDate() {
		return creationDate;
	}
		
	/**
	 * Gets the target date for this Goal.
	 * @return The target date as a Date object.
	 */
	public Date getGoalDate() {
		return goalDate;
	}
	
	/**
	 * Returns the starting value of this Goal.
	 * @return The starting value as a double.
	 */
	public double getStart() {
		return start;
	}
	
	/**
	 * Gets the Goal target.
	 * @return The target value as a double.
	 */
	public double getTarget() {
		return target;
	}	
	
	/**
	 * Gets whether the User has subscribed to this Group Goal or not.
	 * @return Returns true if the user has subscribed, false otherwise.
	 */
	public boolean getCanSubscribe() {
		return canSubscribe;
	}

	/**
	 * Checks if the Goal is completed or not.
	 * @return Returns true if the Goal is completed, false otherwise.
	 */
	public boolean isCompleted() {
		return completed;
	}
	
	/**
	 * Checks if this is a Weight Goal or not.
	 * @return Returns true if this is a weight goal, false otherwise.
	 */
	public boolean isWeightGoal() {
		return weightGoal;
	}
	
	/**
	 * Sets the ID for this Goal.
	 * @param id The Goal ID as an integer.
	 */
	public void setGoalID(int id) {
		this.id = id;
	}
	
	/**
	 * Sets the UserID for the User associated with this Goal.
	 * @param userID The new UserID as an integer.
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	/**
	 * Sets the starting value for this Goal.
	 * @param start The starting value as a double.
	 */
	public void setStart(double start) {
		this.start = start;
	}
	
	/**
	 * Sets the target value for this goal.
	 * @param goalDistance The new target as an double.
	 */
	public void setTarget(double goalDistance) {
		this.target = goalDistance;
	}
	
	/**
	 * Sets the date this Goal was created.
	 * @param creationDate The new date as a Date object.
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
		
	/**
	 * Sets the target date for this Goal.
	 * @param goalDate The new target as a Date object.
	 */
	public void setGoalDate(Date goalDate) {
		this.goalDate = goalDate;
	}	
	
	/**
	 * Sets whether or not the Goal has been achieved.
	 * @param completed True if the Goal is achieved, false otherwise.
	 */
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	
	/**
	 * Sets the type of Goal this is.
	 * @param type Set to true if this is a weight Goal, false otherwise.
	 */
	public void setGoalType(boolean type) {
		this.weightGoal = type;
	}
	
	/**
	 * Sets whether this Group Goal is subscribed to or not.
	 * @param canSubscribe True if the goal is to be subscribed, false otherwise.
	 */
	public void setCanSubscribe(boolean canSubscribe) {
		this.canSubscribe = canSubscribe;
	}
	
}
