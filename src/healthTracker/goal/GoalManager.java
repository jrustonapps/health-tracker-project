package healthTracker.goal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import healthTracker.user.SystemUser;
import healthTracker.user.UserActivities;
import healthTracker.user.UserHistory;
import healthTracker.user.UserManager;
import healthTracker.utils.DatabaseController;
import healthTracker.captures.DietCapture;
import healthTracker.captures.Exercise;
import healthTracker.captures.Food;
import healthTracker.goal.Goal;

/**
 * GoalManager.java
 * 
 * Static methods to create a goal (individual + group) as well as completing or
 * Deleting them. Information from the database can also be retrieved.
 * 
 * @author 6183891, 6266215
 * @version 19/03/2013
 */
public class GoalManager {

	/**
	 * Method to create a goal and add it to the database.
	 * @param date The date of when it was created
	 * @param goal The date it is to be completed by
	 * @param type The type of goal
	 * @param user The ID of the user who created it.
	 * @param target either the weight, or amount of exercise they are aiming for
	 * @return The ID of the goal
	 */
	public static int createGoal(int userID, Date date, Date goal, boolean type, double start, double target) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int goalID = 0;
		
		try{
			/* Attempt to insert this goal into the generic "Goal" table. */
			query = db.getConnection().prepareStatement("INSERT INTO Goal(userID, creationDate, goalDate, complete) " +
					"VALUES(?, ?, ?, false) RETURNING goalID");
			java.sql.Date sqlDate = new java.sql.Date(date.getTime());
			java.sql.Date sqlGoalDate = new java.sql.Date(goal.getTime());
			query.setInt(1, userID);
			query.setDate(2, sqlDate);
			query.setDate(3, sqlGoalDate);
			result = db.runQuery(query);
			/* Based on the goal type, insert more detail into the specialised tables. */
			if(!type){
				/* Attempt to insert a exercise goal. */
				try {
					if (result.next()){
						goalID = result.getInt("goalID");
						query = db.getConnection().prepareStatement("INSERT INTO ExerciseGoal VALUES(?, ?, ?)");
						query.setInt(1, goalID);
						query.setDouble(2, start);
						query.setDouble(3, target);
						db.runUpdate(query);
					}
				}
				catch(SQLException e){
					System.err.println("Unable to fetch goal ID");
				}
			}else{
				/* Or attempt to insert a weight goal. */
				try{
					if (result.next()){
						goalID = result.getInt("goalID");
						//0 is being inserted into the current weight column here, must replace with a retrieval of the users current weight
						query = db.getConnection().prepareStatement("INSERT INTO WeightGoal VALUES(?, ?, ?)");
						query.setInt(1, goalID);
						query.setDouble(2, start);
						query.setDouble(3, target);
						db.runUpdate(query);
					}
				}
				catch(SQLException e){
					System.err.println("Unable to fetch goal ID");
				}
			}
		} catch (SQLException e) {
			System.err.println("Failed to create new Goal!");
			System.err.println(e.getMessage());
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}
		return goalID;
	}
	
	/**
	 * Method to create a group goal and add it to the database.
	 * @param goalID The ID of the goal
	 * @param groupID The ID of the group to add it to
	 * @return
	 */
	public static boolean createGroupGoal(int goalID, int groupID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		/* Try inserting a Group goal into the table. */
		try {
			query = db.getConnection().prepareStatement("INSERT INTO GroupGoal VALUES(?, ?)");
			query.setInt(1, groupID);
			query.setInt(2, goalID);
			db.runUpdate(query);
		}  catch (SQLException e) {
			System.err.println("Failed to create group goal!");
			System.err.println(e.getMessage());
			return false; // if it fails, return false.
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
		return true;
	}
	
	/**
	 * Method to remove a group goal
	 * @param id The ID of the goal to remove
	 */
	public static void removeGroupGoal(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		
		try {
			/* Delete any records related to the provided ID. */
			query = db.getConnection().prepareStatement("DELETE FROM Goal WHERE goalId = ?");
			query.setInt(1, id);
			db.runUpdate(query);
			query = db.getConnection().prepareStatement("DELETE FROM GroupGoal WHERE goalID = ?");
			query.setInt(1, id);
			db.runUpdate(query);
		}  catch (SQLException e) {
			System.err.println("Could not remove Group Goal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
	}
	
	public static void editGoal(Goal g) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		
		try {
			query = db.getConnection().prepareStatement("UPDATE Goal SET goalDate = ? WHERE goalID = ?");
			/* Convert the date to an SQL compatible one. */
			java.sql.Date d = new java.sql.Date(g.getGoalDate().getTime());
			query.setDate(1, d);
			query.setInt(2, g.getId());
			db.runUpdate(query);
			/* Update the correct table based on goal type. */
			if (g.isWeightGoal()) {
				query = db.getConnection().prepareStatement("UPDATE WeightGoal SET goalWeight = ? WHERE goalID = ?");
			} else {
				query = db.getConnection().prepareStatement("UPDATE ExerciseGoal SET goalDistance = ? WHERE goalID = ?");
			}
			query.setDouble(1, g.getTarget());
			query.setInt(2, g.getId());
			db.runUpdate(query);
		} catch (SQLException e) {
			System.err.println("Failed to edit Goal!");
			System.err.println(e.getMessage());
		} finally {
			/* Close DB assets */
			try {
				query.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
			} finally {
				db.closeConnection();
				query = null;
				db = null;
			}
		}
	}
	
	/**
	 * Method to complete a goal
	 * @param id The ID of the goal to complete
	 */
	public static void completeGoal(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		
		try {
			/* Try and set the specified goal completion status to true. */
			query = db.getConnection().prepareStatement("UPDATE Goal SET complete = true WHERE goalid = ?");
			query.setInt(1, id);
			db.runUpdate(query);
		}  catch (SQLException e) {
			System.err.println("Could not complete goal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
	}
	
	/**
	 * Method to remove an individual goal
	 * @param id The ID of the goal to remove
	 */
	public static void removeGoal(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		
		try {
			/* Remove the specified goal from the databse. */
			query = db.getConnection().prepareStatement("DELETE FROM Goal WHERE goalID = ?");
			query.setInt(1, id);
			db.runUpdate(query);
		}  catch (SQLException e) {
			System.err.println("Failed to remove Goal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
	}
	
	/**
	 * Method to get a Goal object from a specific ID
	 * @param id The ID of the goal
	 * @return A Goal object
	 */
	public static Goal getGoalFromID(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		ResultSet r2 = null;
		Goal goal = null;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM Goal WHERE goalID = ?");
			query.setInt(1, id);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				goal = new Goal();
				goal.setGoalID(result.getInt("goalID"));
				goal.setUserID(result.getInt("userID"));
				goal.setCreationDate(result.getDate("creationDate"));
				goal.setGoalDate(result.getDate("goalDate"));
				goal.setCompleted(result.getBoolean("complete"));
				query = db.getConnection().prepareStatement("SELECT * FROM WeightGoal WHERE goalID = ?");
				query.setInt(1, goal.getId());
				r2 = db.runQuery(query);
				if (r2.next()) {
					goal.setStart(r2.getInt("startingWeight"));
					goal.setTarget(r2.getInt("goalWeight"));
					goal.setGoalType(true);
				} else {
					query = db.getConnection().prepareStatement("SELECT * FROM ExerciseGoal WHERE goalID = ?");
					query.setInt(1, goal.getId());
					r2 = db.runQuery(query);
					if (r2.next()) {
						goal.setStart(r2.getInt("exerciseProgress"));
						goal.setTarget(r2.getInt("goalDistance"));
						goal.setGoalType(false);
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
			e.printStackTrace();
			goal = null; // If it fails, just ensure this Goal is null.
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}
		/* Return the Goal, it could be null so check for that later. */
		return goal;
	}
	
	/**
	 * Get the goals the user has created
	 * @param id The ID of the user
	 * @return An ArrayList of the goals
	 */
	public static ArrayList<Goal> getUserGoals(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		ResultSet r2 = null;
		ArrayList<Goal> goals = new ArrayList<Goal>();
		Goal goal = null;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM Goal WHERE userID = ?");
			query.setInt(1, id);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				goal = new Goal();
				goal.setGoalID(result.getInt("goalID"));
				goal.setUserID(result.getInt("userID"));
				goal.setCreationDate(result.getDate("creationDate"));
				goal.setGoalDate(result.getDate("goalDate"));
				goal.setCompleted(result.getBoolean("complete"));
				query = db.getConnection().prepareStatement("SELECT * FROM WeightGoal WHERE goalID = ?");
				query.setInt(1, goal.getId());
				r2 = db.runQuery(query);
				if (r2.next()) {
					goal.setStart(r2.getInt("startingWeight"));
					goal.setTarget(r2.getInt("goalWeight"));
					goal.setGoalType(true);
				} else {
					query = db.getConnection().prepareStatement("SELECT * FROM ExerciseGoal WHERE goalID = ?");
					query.setInt(1, goal.getId());
					r2 = db.runQuery(query);
					if (r2.next()) {
						goal.setStart(r2.getInt("exerciseProgress"));
						goal.setTarget(r2.getInt("goalDistance"));
						goal.setGoalType(false);
					}
				}
				goals.add(goal);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
			System.err.println(e.getMessage());
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
				if (r2 != null) {
					r2.close();
				}
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					r2 = null;
					query = null;
				}
			}
		}
		/* Return the Goal, it could be null so check for that later. */
		return goals;
	}
	
	/**
	 * Get the goals a group has created
	 * @param id The ID of the group
	 * @param userID The ID of the local user
	 * @return An ArrayList of the goals
	 */
	public static ArrayList<Goal> getGroupGoals(int id, int userID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		ResultSet r2 = null;
		ArrayList<Goal> goals = new ArrayList<Goal>();
		Goal goal = null;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM Goal WHERE goalID IN (SELECT goalID FROM GroupGoal WHERE groupID = ?);");
			query.setInt(1, id);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				goal = new Goal();
				goal.setGoalID(result.getInt("goalID"));
				goal.setUserID(result.getInt("userID"));
				goal.setCreationDate(result.getDate("creationDate"));
				goal.setGoalDate(result.getDate("goalDate"));
				goal.setCompleted(result.getBoolean("complete"));
				query = db.getConnection().prepareStatement("SELECT * FROM WeightGoal WHERE goalID = ?");
				query.setInt(1, goal.getId());
				r2 = db.runQuery(query);
				if (r2.next()) {
					goal.setStart(r2.getInt("startingWeight"));
					goal.setTarget(r2.getInt("goalWeight"));
					goal.setGoalType(true);
				} else {
					query = db.getConnection().prepareStatement("SELECT * FROM ExerciseGoal WHERE goalID = ?");
					query.setInt(1, goal.getId());
					r2 = db.runQuery(query);
					if (r2.next()) {
						goal.setStart(r2.getInt("exerciseProgress"));
						goal.setTarget(r2.getInt("goalDistance"));
						goal.setGoalType(false);
					}
				}
				
				// Now we need to check if the user can subscribe or not. By default, we can
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				Calendar today = Calendar.getInstance();
				
				if (GoalManager.isSubscribedToGroupGoal(userID, goal.getId()) ||
						!goal.getGoalDate().after(today.getTime())) {
					goal.setCanSubscribe(false);
				} else {
					goal.setCanSubscribe(true);
				}
				
				goals.add(goal);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
			System.err.println(e.getMessage());
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
				if (r2 != null) {
					r2.close();
				}
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					r2 = null;
					query = null;
				}
			}
		}
		/* Return the Goal, it could be null so check for that later. */
		return goals;
	}
	
	/**
	 * Updates the progress of the specified Goal.
	 * @param g A Goal to update the progress of.
	 */
	public static Goal updateGoal(Goal g) {
		
		if (g.isWeightGoal()) {
			UserHistory weightHist = UserManager.getLastHistory(g.getUserID());
			/* Check if the starting weight minus the Users current weight is >= the target weight. */
			if (g.getTarget() < 0) {
				/* If the goal weight is lower than zero we need to check differently. */
				if((weightHist.getWeight() - g.getStart()) <= g.getTarget()) {
					completeGoal(g.getId());
				}
			} else {
				
				if((weightHist.getWeight() - g.getStart()) >= g.getTarget()) {
					completeGoal(g.getId());
				}
			}
		} else {
			double kCal = 0.0;
			Calendar oldDate = Calendar.getInstance();
			oldDate.setTime(g.getCreationDate());
			Calendar day = Calendar.getInstance();
			while (!day.before(oldDate)) {
				/* For every day since the Goal started, get the food and exercise details. */
				UserActivities calIn = DietCapture.getFoodAndExerciseOnDate(oldDate.getTime(), g.getUserID());
				if (calIn != null) {
					int i = 0;
					boolean run = true;
					while (run) {
						
						if (i < calIn.getFood().size()) {
							Food f = calIn.getFood().get(i);
							/* Add the total KCal consumed. */
							kCal += (f.getCals() * f.getQuantity());
						}
						if (i < calIn.getExercise().size()) {
							Exercise ex = calIn.getExercise().get(i);
							/* Remove the total KCal burned. */
							kCal -= (ex.getCalsPerUnit() * ex.getQuantity());
						}
						
						/* Work out if the target has been reached or not. */
						double result = g.getStart() + kCal;
						if (g.getTarget() >= 0.0) {
							if (result >= g.getTarget()) {
								completeGoal(g.getId());
								run = false;
							}
						} else if (g.getTarget() < 0.0) {
							if (result <= g.getTarget()) {
								completeGoal(g.getId());
								run = false;
							}
						}	
						
						/* If there is nothing left to iterate over, end it now. */
						if (i >= calIn.getFood().size() && i >= calIn.getExercise().size()) {
							run = false;							
						}
						i++;
					}
				}
				oldDate.add(Calendar.DATE, 1);  // number of days to add
			}
			/* If we get this far, the goal hasn't completed, so store the progress. */
			g.setStart(kCal);
			DatabaseController db = null;
			PreparedStatement sql = null;
			try {
				db = new DatabaseController();
				sql = db.getConnection().prepareStatement(
						"UPDATE ExerciseGoal SET exerciseProgress = ? WHERE goalID = ?");
				sql.setDouble(1, g.getStart());
				sql.setInt(2, g.getId());
				
				db.runUpdate(sql);
			} catch (SQLException e) {
				System.err.println("Could not update Goal in database!");
			} finally {
				try {
					sql.close();
				} catch (SQLException e) {
					System.err.println("Failed to close PreparedStatement!");
				} finally {
					db.closeConnection();
					sql = null;
					db = null;
				}
			}
		}
		
		return g;
	}
	
	/**
	 * Method to check if a user has subscribed to a group goal
	 * @param userID The ID of the user
	 * @param groupGoalID The ID of the group goal
	 * @return True if they've subscribed, false otherwise.
	 */
	public static boolean isSubscribedToGroupGoal(int userID, int groupGoalID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		ResultSet result2 = null;
		boolean hasSubscribed = false;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM UserInGroupGoal WHERE groupGoalID = ? AND individualGoalID IN (SELECT goalID FROM Goal WHERE userID = ?);");
			query.setInt(1, groupGoalID);
			query.setInt(2, userID);
			result = db.runQuery(query);
			// Are they subscribed to the goal?
			if (result.next()) {
				hasSubscribed = true;
				
			// Doesn't appear that they are, but we better check to see if they actually created the goal
			} else {
				query = db.getConnection().prepareStatement("SELECT * FROM GroupGoal WHERE goalID = ? AND goalID IN (SELECT goalID FROM Goal WHERE userID = ?);");
				query.setInt(1, groupGoalID);
				query.setInt(2, userID);
				result2 = db.runQuery(query);
				
				if (result2.next()) {
					hasSubscribed = true;
				}
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return hasSubscribed;
	}
	
	/**
	 * Method to subscribe a user to a group goal
	 * @param individualGoalID The individual goal ID
	 * @param groupGoalID The group goal ID
	 */
	public static void subscribeUserToGroupGoal(int individualGoalID, int groupGoalID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		
		try {
			/* Remove the specified goal from the database. */
			query = db.getConnection().prepareStatement("INSERT INTO UserInGroupGoal VALUES (?, ?);");
			query.setInt(1, groupGoalID);
			query.setInt(2, individualGoalID);
			db.runUpdate(query);
		}  catch (SQLException e) {
			System.err.println("Failed to insert Goal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
	}
	
	/**
	 * Method to remove the user who created the goal from a group goal safely, without deleting it for everyone.
	 * If nobody else is subscribed to the goal, the goal is deleted.
	 * @param groupGoalID The ID of the group goal
	 */
	public static void removeUserIDInGroupGoal(int groupGoalID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		
		try {
			
			query = db.getConnection().prepareStatement("SELECT userID FROM Goal WHERE goalID IN (SELECT individualGoalID FROM UserInGroupGoal WHERE groupGoalID = ? LIMIT 1);");
			query.setInt(1, groupGoalID);
			result = db.runQuery(query);
			
			// Are results present?
			if (result.next()) {
				
				int user = result.getInt("userID");
				// Change the ID of the user in the database so the goal is kept safe for other users
				query = db.getConnection().prepareStatement("UPDATE Goal SET userID = ? WHERE goalID = ?;");
				query.setInt(1, user);
				query.setInt(2, groupGoalID);
				db.runUpdate(query);
				
			// If no other users are subscribed to the goal, we can just delete it
			} else {
				query = db.getConnection().prepareStatement("DELETE FROM GroupGoal WHERE goalID = ?; DELETE FROM Goal WHERE goalID = ?");
				query.setInt(1, groupGoalID);
				query.setInt(2, groupGoalID);
				db.runUpdate(query);
			}
			
		}  catch (SQLException e) {
			System.err.println("Failed to change Goal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
	}
	
	/**
	 * Method to check if this goal ID is in fact the main one for a group.
	 * This is done so we can remove users from group goals safely.
	 * @param individualGoalID The ID of the individual goal
	 * @return True if this is the main one for the group, false otherwise.
	 */
	public static boolean didCreateGroupGoal(int individualGoalID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		boolean didCreate = false;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM GroupGoal WHERE goalID = ?;");
			query.setInt(1, individualGoalID);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				didCreate = true;
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return didCreate;
	}
	
	/**
	 * Method to unsubscribe a user from a group goal (when they didn't create the goal).
	 * This must be done before the user can delete their entry in the Goal table.
	 * @param groupGoalID The group goal ID
	 * @param individualGoalID The individual goal ID
	 */
	public static void unsubscribeUserFromGroupGoal(int groupGoalID, int individualGoalID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		
		try {
			query = db.getConnection().prepareStatement("DELETE FROM UserInGroupGoal WHERE groupGoalID = ? AND individualGoalID = ?;");
			query.setInt(1, groupGoalID);
			query.setInt(2, individualGoalID);
			db.runUpdate(query);
		}  catch (SQLException e) {
			System.err.println("Failed to unsubscribe from Goal!");
			System.err.println(e.getMessage());
		} finally {
			try {
				query.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				query = null;
			}
		}
	}
	
	/**
	 * Method to check if this goal is part of a group goal
	 * @param individualGoalID The goal ID to check
	 * @return True if this is part of a group goal, false otherwise.
	 */
	public static int isPartOfGroupGoal(int individualGoalID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int groupGoal = 0;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM UserInGroupGoal WHERE individualGoalID = ?;");
			query.setInt(1, individualGoalID);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				groupGoal = result.getInt("groupGoalID");
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return groupGoal;
	}
	
	/**
	 * Get the number of active goals the user is involved in
	 * @param userID The ID of the user
	 * @return Integer of number of active goals
	 */
	public static int getNumberOfActiveGoals(int userID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		int activeGoals = 0;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM Goal WHERE userID = ? AND complete = false;");
			query.setInt(1, userID);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				activeGoals ++;
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return activeGoals;
	}
	
	/**
	 * Get all of the users which are participated in a group goal
	 * @param groupGoalID The ID of the overall group goal
	 * @return An ArrayList of all of the individual goals
	 */
	public static ArrayList<Goal> getGroupGoalProgress(int groupGoalID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		ResultSet r2 = null;
		ArrayList<Goal> goals = new ArrayList<Goal>();
		Goal goal = null;
		
		try {
			query = db.getConnection().prepareStatement("(SELECT * FROM Goal WHERE goalID IN (SELECT individualGoalID FROM UserInGroupGoal " +
					"WHERE groupGoalID IN (SELECT goalID FROM GroupGoal WHERE goalID = ?))) " +
					"UNION (SELECT * FROM Goal WHERE goalID IN (SELECT goalID FROM GroupGoal WHERE goalID = ?));");
			query.setInt(1, groupGoalID);
			query.setInt(2, groupGoalID);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				goal = new Goal();
				goal.setGoalID(result.getInt("goalID"));
				goal.setUserID(result.getInt("userID"));
				goal.setCreationDate(result.getDate("creationDate"));
				goal.setGoalDate(result.getDate("goalDate"));
				goal.setCompleted(result.getBoolean("complete"));
				query = db.getConnection().prepareStatement("SELECT * FROM WeightGoal WHERE goalID = ?");
				query.setInt(1, goal.getId());
				r2 = db.runQuery(query);
				if (r2.next()) {
					goal.setStart(r2.getInt("startingWeight"));
					goal.setTarget(r2.getInt("goalWeight"));
					goal.setGoalType(true);
				} else {
					query = db.getConnection().prepareStatement("SELECT * FROM ExerciseGoal WHERE goalID = ?");
					query.setInt(1, goal.getId());
					r2 = db.runQuery(query);
					if (r2.next()) {
						goal.setStart(r2.getInt("exerciseProgress"));
						goal.setTarget(r2.getInt("goalDistance"));
						goal.setGoalType(false);
					}
				}
				
				goals.add(goal);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through goals");
			System.err.println(e.getMessage());
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
				if (r2 != null) {
					r2.close();
				}
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					r2 = null;
					query = null;
				}
			}
		}
		/* Return the Goal, it could be null so check for that later. */
		return goals;
	}
	
	/**
	 * Get the emails of all of the members in a group. 1 user can be ignored from this list.
	 * This is used when sending group members an email about a group.
	 * @param groupID The ID of the group
	 * @param ignoreThisUser The ID of the user to ignore (so a user doesn't email themselves)
	 * @return An ArrayList of email addresses
	 */
	public static ArrayList<SystemUser> getEmailsOfGroupUsers(int groupID, int ignoreThisUser) {
		DatabaseController db = new DatabaseController();
		PreparedStatement query = null;
		ResultSet result = null;
		ArrayList<SystemUser> users = new ArrayList<SystemUser>();
		SystemUser user = null;
		
		try {
			query = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE userID IN " +
					"(SELECT userID FROM GroupUser WHERE groupID = ? AND userID != ?);");
			query.setInt(1, groupID);
			query.setInt(2, ignoreThisUser);
			result = db.runQuery(query);
			// This should contain one row with all of the goal's information
			while (result.next()) {
				user = new SystemUser();
				user.setUserID(result.getInt("userID"));
				user.setPassword(result.getString("password"));
				user.setEmail(result.getString("email"));
				user.setForename(result.getString("forename"));
				user.setSurname(result.getString("surname"));
				user.setDateOfBirth(result.getDate("DoB"));
				user.setSex(result.getBoolean("sexMale"));
				user.setCounty(result.getString("county"));
				user.setAdmin(result.getBoolean("admin"));
				user.setVerified(result.getBoolean("verified"));
				user.setMeasurementPreference(result.getString("measurementPreference"));
				
				users.add(user);
			}
		} catch (SQLException e) {
			System.err.println("Unable to loop through users/goals");
			e.printStackTrace();
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					query.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					query = null;
				}
			}
		}

		return users;
	}
}
