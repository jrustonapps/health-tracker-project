package healthTracker.goal;

import healthTracker.goal.Goal;

/**
 * GroupGoal.java
 * 
 * Extends the Goal class to provide a group ID and a group goal ID.
 * 
 * @author 6183891
 */
public class GroupGoal extends Goal {

	protected int group, groupGoal;
	
	/**
	 * Default constructor to create a GroupGoal object
	 */
	public GroupGoal() {
		super();
		group = 0;
		groupGoal = 0;
	}
	
	/**
	 * Constructor to create a GroupGoal object with a group ID and a goal ID.
	 * @param group
	 * @param groupGoal
	 */
	public GroupGoal(int group, int groupGoal) {
		super();
		this.group = group;
		this.groupGoal = groupGoal;
	}
	
	/**
	 * Method to get the group ID.
	 * @return group ID
	 */
	public int getGroup() {
		return group;
	}
	
	/**
	 * Method to get the group goal ID
	 * @return group goal ID
	 */
	public int getGroupGoal() {
		return groupGoal;
	}
	
	/**
	 * Method to set the group ID
	 * @param group The group ID
	 */
	public void setGroup(int group) {
		this.group = group;
	}
	
	/**
	 * Method to set the group goal ID
	 * @param groupGoal The group goal ID
	 */
	public void setGroupGoal(int groupGoal) {
		this.groupGoal = groupGoal;
	}
	
}
