package healthTracker.group;

import java.util.Date;

/**
 * Group.java
 * 
 * Stores details about a Group - The ID, the person who created it as well as the name,
 * Description and the date it was created.
 * 
 * @author 6183891
 */
public class GroupDetails {
	
	protected int groupID;
	protected String groupName, groupDescription;
	protected Date creationDate;
	
	/**
	 * Creates a new Group with blank details.
	 */
	public GroupDetails() {
		groupID = 0;
		groupName = "";
		groupDescription = "";
		creationDate = new Date();
	}
	
	/**
	 * Creates a new Group with the specified details.
	 * @param groupID The group ID for this group as an integer.
	 * @param groupOwner The user ID of the User who created this group.
	 * @param groupName The name of this group as a String.
	 * @param groupDescription The description for this group as a String.
	 * @param creationDate The date this group was created as a Date.
	 */
	public GroupDetails(int groupID, String groupName,
			String groupDescription, Date creationDate) {
		this.groupID = groupID;
		this.groupName = groupName;
		this.groupDescription = groupDescription;
		this.creationDate = creationDate;
	}
	
	/**
	 * Returns the group ID for this Group.
	 * @return The group ID as an integer.
	 */
	public int getGroupID() {
		return groupID;
	}
	
	
	/**
	 * Returns the group name for this Group.
	 * @return The group name as a String.
	 */
	public String getGroupName() {
		return groupName;
	}
	
	/**
	 * Returns the group description for this Group.
	 * @return The description as a String.
	 */
	public String getGroupDescription() {
		return groupDescription;
	}
	
	/**
	 * Returns the date this Group was created.
	 * @return The date as a Date object.
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the ID for this group to the specified value.
	 * @param groupID The new ID as an integer.
	 */
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	
	
	/**
	 * Sets the desired name of this group.
	 * @param groupName The name as a String.
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	/**
	 * Sets the description for this Group.
	 * @param groupDescription The description as a String.
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	
	/**
	 * Sets the date this Group was created.
	 * @param creationDate The new date as a Date object.
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
