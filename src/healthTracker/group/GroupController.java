package healthTracker.group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.lang.String;

import healthTracker.utils.DatabaseController;
import healthTracker.group.GroupDetails;
import healthTracker.user.SystemUser;

/**
 * GroupManager.java
 * 
 * Static methods to create groups, add users to groups as well as retrieving info
 * From the database.
 * 
 * @author 6183891, 6266215
 * @version 19/03/2013
 */
public class GroupController {
	
	/**
	 * Method to create a group and add it to the database.
	 * @param name The name of the group
	 * @param description Description of the group
	 * @param date the current date to be recorded in 'datecreated'
	 * @return
	 */
	public static boolean createGroup(String name, String description, int owner) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		boolean complete = false;
		
		Date date = new Date();
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO GroupDetails(groupName, groupDesc, creationDate) "
					+ "VALUES(?, ?, ?) RETURNING groupID");
			sql.setString(1, name);
			sql.setString(2, description);
			sql.setDate(3, sqlDate);
			result = db.runQuery(sql);
			
			while (result.next()) {
				sql = db.getConnection().prepareStatement("INSERT INTO GroupUser(groupID, userID) "
						+ "VALUES(?, ?)");
				sql.setInt(1, result.getInt("groupID"));
				sql.setInt(2, owner);
				db.runUpdate(sql);
			}
			
			complete = true;
		} catch (SQLException e) {
			System.err.println("Could not insert new Group!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
		return complete;
	}
	
	/**
	 * Method to add a user to the group
	 * @param groupID The ID of the group to add the user into
	 * @param userID The user to add
	 */
	public static void addUser(int groupID, int userID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO GroupUser VALUES(?, ?)");
			sql.setInt(1, groupID);
			sql.setInt(2, userID);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not insert new Group!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
	}
	
	/**
	 * Method for a user to leave a group
	 * @param groupID The group ID for the user to be removed from
	 * @param userID The user to remove
	 */
	public static void leaveGroup(int groupID, int userID) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		
		try {
			sql = db.getConnection().prepareStatement("DELETE FROM GroupUser WHERE groupID = ? AND userID = ?");
			sql.setInt(1, groupID);
			sql.setInt(2, userID);
			db.runUpdate(sql);
			
			sql = db.getConnection().prepareStatement("DELETE FROM GroupDetails WHERE groupID = ?;");
			sql.setInt(1, groupID);
			db.runUpdate(sql);
		} catch (SQLException e) {
			System.err.println("Could not delete Group!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
	}
	
	/**
	 * Method to get a Group object from an ID.
	 * @param id The ID of the group to retrieve.
	 * @return
	 */
	public static GroupDetails getGroupFromID(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		GroupDetails group = null;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM GroupDetails WHERE groupID = ?");
			sql.setInt(1, id);
			result = db.runQuery(sql);
			// This should contain a row with all of the group information.
			while (result.next()) {
				int groupID = result.getInt("groupID");
				String groupName = result.getString("groupName");
				String groupDescription = result.getString("groupDesc");
				Date creationDate = result.getDate("creationDate");
				
				group = new GroupDetails(groupID, groupName,
						groupDescription, creationDate);
			}
		} catch (SQLException e) {
			System.err.println("Could not get group!");
			System.err.println(e.getMessage());
			group = null;
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					sql = null;
				}
			}
		}
		/* Return the Group we found. It could be null so we should check this later. */
		return group;
	}
	
	/**
	 * Get all of the groups that a user is associated with.
	 * @param userID The ID of the user
	 * @return An ArrayList of groups the user has joined.
	 */
	public static ArrayList<GroupDetails> getGroupsFromUser(int userID) {
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		GroupDetails group = null;
		ArrayList<GroupDetails> groups = new ArrayList<GroupDetails>();
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM GroupDetails WHERE groupID IN (SELECT groupID FROM GroupUser WHERE userID = ?)");
			sql.setInt(1, userID);
			result = db.runQuery(sql);
			// This should contain a row with all of the group information.
			while (result.next()) {
				int groupID = result.getInt("groupID");
				String groupName = result.getString("groupName");
				String groupDescription = result.getString("groupDesc");
				Date creationDate = result.getDate("creationDate");
				
				group = new GroupDetails(groupID, groupName,
						groupDescription, creationDate);
				
				groups.add(group);
			}
		} catch (SQLException e) {
			System.err.println("Could not get group!");
			System.err.println(e.getMessage());
			group = null;
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					sql = null;
				}
			}
		}
		/* Return the Groups we found. It could be null so we should check this later. */
		return groups;
	}
	
	/**
	 * Get all of the groups based on a search query that the user has entered
	 * @param searchQuery The query to search for.
	 * @return An ArrayList of all of the groups
	 */
	public static ArrayList<GroupDetails> getGroupsFromSearch(String searchQuery) {
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		GroupDetails group = null;
		ArrayList<GroupDetails> groups = new ArrayList<GroupDetails>();
		searchQuery = searchQuery.toLowerCase();
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM GroupDetails WHERE LOWER(groupName) LIKE ?");
			sql.setString(1, "%"+searchQuery+"%");
			result = db.runQuery(sql);
			// This should contain a row with all of the group information.
			while (result.next()) {
				int groupID = result.getInt("groupID");
				String groupName = result.getString("groupName");
				String groupDescription = result.getString("groupDesc");
				Date creationDate = result.getDate("creationDate");
				
				group = new GroupDetails(groupID, groupName,
						groupDescription, creationDate);
				
				groups.add(group);
			}
		} catch (SQLException e) {
			System.err.println("Could not get group!");
			System.err.println(e.getMessage());
			group = null;
		} finally {
			/* Try closing all open database assets. */
			try {
				result.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					result = null;
					sql = null;
				}
			}
		}
		/* Return the Groups we found. It could be null so we should check this later. */
		return groups;
	}
	
	/**
	 * Checks if a group name has already been taken
	 * @param groupName The name to check
	 * @return True if it has been taken, false otherwise.
	 */
	public static boolean doesGroupNameExist(String groupName) {
		boolean doesGroupExist = false;
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM GroupDetails WHERE GroupName = ?");
			sql.setString(1, groupName);
			result = db.runQuery(sql);
			
			while (result.next()) {
				doesGroupExist = true;
			}
		} catch (SQLException e) {
			System.err.println("Could not check if group exists");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
		
		return doesGroupExist;
	}
	
	public static boolean doesGroupIDExist(int groupID) {
		boolean doesGroupExist = false;
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM GroupDetails WHERE GroupID = ?");
			sql.setInt(1, groupID);
			result = db.runQuery(sql);
			
			while (result.next()) {
				doesGroupExist = true;
			}
		} catch (SQLException e) {
			System.err.println("Could not check if group exists");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
		
		return doesGroupExist;
	}
	
	/**
	 * Method to get a list of all of the members who are joined to a group
	 * @param id The ID of the group
	 * @return An ArrayList containing all of the SystemUsers.
	 */
	public static ArrayList<SystemUser> getGroupMembersFromGroupID(int id) {
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet rs = null;
		SystemUser user;
		ArrayList<SystemUser> users = new ArrayList<SystemUser>();
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM SystemUser WHERE userID IN (SELECT userID FROM GroupUser WHERE groupID = ?)");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			// This should contain a row with all of the group information.
			while (rs.next()) {
				user = new SystemUser();
				user.setUserID(rs.getInt("userID"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
				user.setForename(rs.getString("forename"));
				user.setSurname(rs.getString("surname"));
				user.setDateOfBirth(rs.getDate("DoB"));
				user.setSex(rs.getBoolean("sexMale"));
				user.setCounty(rs.getString("county"));
				user.setAdmin(rs.getBoolean("admin"));
				user.setVerified(rs.getBoolean("verified"));
				user.setMeasurementPreference(rs.getString("measurementPreference"));
				
				users.add(user);
			}
		} catch (SQLException e) {
			System.err.println("Could not get group users!");
			System.err.println(e.getMessage());
		} finally {
			/* Try closing all open database assets. */
			try {
				rs.close();
			} catch (SQLException ex) {
				System.err.println("Could not close ResultSet!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					sql.close();
				} catch (SQLException exc) {
					System.err.println("Failed to close PreparedStatement!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					db = null;
					rs = null;
					sql = null;
				}
			}
		}
		/* Return the Group we found. It could be null so we should check this later. */
		return users;
	}
	
	/**
	 * Method to check if a user has joined a group
	 * @param userID The ID of the user
	 * @param groupID The ID of the group
	 * @return True if the user has joined the group, false otherwise.
	 */
	public static boolean hasJoinedGroup(int userID, int groupID) {
		boolean hasJoinedGroup = false;
		
		DatabaseController db = new DatabaseController();
		PreparedStatement sql = null;
		ResultSet result = null;
		
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM GroupUser WHERE groupID = ? AND userID = ?");
			sql.setInt(1, groupID);
			sql.setInt(2, userID);
			result = db.runQuery(sql);
			
			while (result.next()) {
				hasJoinedGroup = true;
			}
		} catch (SQLException e) {
			System.err.println("Could not check if user exists in group");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException exc) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(exc.getMessage());
			} finally {
				db.closeConnection();
				db = null;
				sql = null;
			}
		}
		
		return hasJoinedGroup;
	}
}
