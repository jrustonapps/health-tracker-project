<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Diet Manager</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="script/tcal.js"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<fmt:formatDate value="${date}" pattern="dd/MM/yyyy" var="fmtDate" />
    <script>
    	/* Use AJAX to grab a listing of all groups related to the search terms. */
		$(document).ready(function() {
			$('#searchQuery').keyup(function() {
				var searchString = $('#searchQuery').val();
				$.get('AjaxServlet', {fSearch: searchString, date: '${fmtDate}'}, function(response) {
					$('#searchArea').hide().html(response).fadeIn();
				}, 'html');
			});
			$('#searchQuery').keyup();
			/* Stop the user submitting the form with enter, it's annoying. */
			$('#searchQuery').keydown(function(event) {
				if (event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});
		});
	</script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" />${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager" class="current">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
    
    	<h2>Group Goal Progress</h2>
    	
		<table class="center">
			<tr>
				<th>First Name</th>
				<th>Surname</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Goal Target</th>
				<th>Start</th>
				<th>Completed</th>
			</tr>
			<c:set var="groupUserNum" scope="request" value="${0}"/>
			
			<c:forEach items="${goalProgress}" var="groupGoal">
			<tr>
				<td><c:out value="${goalUsers[groupUserNum].forename}" /></td>
				<td><c:out value="${goalUsers[groupUserNum].surname}" /></td>
				<td><fmt:formatDate value="${groupGoal.creationDate}" pattern="dd/MM/yyyy" /></td>
				<td><fmt:formatDate value="${groupGoal.goalDate}" pattern="dd/MM/yyyy" /></td>
				
				<%-- Check if the Goal is a weight goal, show the right units. --%>
				<c:choose>
				<c:when test="${groupGoal.weightGoal}">
				<td>${groupGoal.start + groupGoal.target} ${wUnit}</td>
				<td>${groupGoal.start} ${wUnit}</td>
				</c:when>
				<c:otherwise>
				<td>${groupGoal.target} kCal</td>
				<td>${groupGoal.start} kCal</td>
				</c:otherwise>
				</c:choose>
				
				<%-- If the Goal is completed, show a tick. --%>
				<c:if test="${groupGoal.completed}">
				<td><img src="img/tick.png" alt="Completed" /></td>
				</c:if>
				<%-- If the Goal is failed, show a cross. --%>
				<c:if test="${!groupGoal.completed}">
				<td><img src="img/cross.png" alt="Failed" /></td>
				</c:if>
			</tr>
			
			<c:set var="groupUserNum" scope="request" value="${groupUserNum+1}"/>
			</c:forEach>
			
		</table>
		<p align="center">Only members which have subscribed to the goal are listed here.</p>
		<p align="center"><a href="javascript:history.back();" title="Go back">Go back</a></p>
	</div>
</body>
</html>
