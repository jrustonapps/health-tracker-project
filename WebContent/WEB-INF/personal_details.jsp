<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - My Details</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
				<img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                    <c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index" class="current">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
	<h2>Personal Details</h2>
		<form action="PersonalSettings" method="post" id="personaldetails">
			<fieldset class="fields">
				<label for="first_name">First Name:</label>
				    <input type="text" name="first_name" id="first_name" value="${user.forename}" />
				<label for="surname">Surname:</label>
					<input type="text" name="surname" id="surname" value="${user.surname}" />
				<label for="dob">D.O.B:</label>
					<input type="text" name="dob" id="dob" value="${user.dateOfBirth}" />
				<label for="county">County:</label>
					<input type="text" name="county" id="county" value="${user.county}" />
				<label for="gender">Gender:</label>
					<select name="sex" id="registerID">
						<option value="m" <c:if test="${user.sex==true}">selected</c:if>>Male</option>
						<option value="f" <c:if test="${user.sex==false}">selected</c:if>>Female</option>
					</select>
				<label for="email">e-Mail:</label>
					<input type="text" name="email" id="email" value="${user.email}" />
				<label for="password">Old Password: <span class="red"><sup>*</sup></span></label>
					<input type="password" name="password" id="password" />
				<label for="newPassword">New Password:</label>
					<input type="password" name="newPassword" id="newPassword" />
				<label for="newPasswordC">New Password Confirmation:</label>
					<input type="password" name="newPasswordC" id="newPasswordC" />
				<input type="submit" value="Edit" name="submit" class="button" />
				<script type="text/javascript">
                    function AskAndSubmit(){
                        var answer = confirm("Continuing will confirm your acceptance of closure for this account! Are you sure you want to do this?");
                        if (answer){
                            document.personaldetails.submit();
                        }
                    }</script>
				<input type="submit" value="Close Account" name="confirm" class="button"/>
			</fieldset>
			<p><span class="red"><sup>*</sup></span> Means the field is required!</p>
		</form>
    </div>
</body>
</html>
