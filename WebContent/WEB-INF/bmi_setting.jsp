<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - BMI Settings</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="headerWrapper">
		<div id="header">
			<div id="logo">
				<img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
		<div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile" class="current">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
</div>
<div id="bodyCenter">
	<h2>BMI Settings</h2>
	<p>To update your BMI, height and weight details please make the necessary changes on this form.</p>
	<p>These can be in either metric or imperial, so don't worry about converting yourself.</p>
	<form action="" method="post" id="first_measure">
			<fieldset class="unit fields">
				<select id="measurementType" name="measurementType">
					<option value="1" selected="selected">Metric</option>
					<option value="2">Imperial</option>
				</select>
				<fieldset id="metricFields">
					<label for="heightValMeters">Height</label>
					<input type="text" name="heightValMeters" id="heightValMeters" />Meters
					<label for="weightValKilograms">Weight</label>
					<input type="text" name="weightValKilograms" id="weightValKilograms" />Kilograms
				</fieldset>
				<fieldset id="imperialFields">
					<label for="heightValFeet">Height</label>
					<input type="text" name="heightValFeet" id="heightValFeet" />Feet
					<input type="text" name="heightValInches" id="heightVaInches" />Inches
					<label for="weightValStone">Weight</label>
					<input type="text" name="weightValStone" id="weightValStone" />Stone
					<input type="text" name="weightValPounds" id="weightValPounds" />Pounds
				</fieldset>
				<input type="hidden" name="submitted" value="1" /><input type="submit" value="Submit" class="button" />
				<input type="reset" value="Clear" class="button" />
			</fieldset>
		</form>
		<script>
	    	window.onload = function() {
	    		var measurementType = document.getElementById('measurementType');
	    		measurementType.value = 1;
	    		$('#imperialFields').hide();
	    	};
	    	
	    	$('#measurementType').change(function() {
	    		var typeVal = $('#measurementType').val();
	    		
	    		if (typeVal == 1) {
	    			$('#imperialFields').hide();
	    			$('#metricFields').show();
	    			$('#scottGrandison').hide();
	    		} else if (typeVal == 2) {
	    			$('#metricFields').hide();
	    			$('#imperialFields').show();
	    			$('#scottGrandison').hide();
	    		}
	    	});
    	</script>
    </div>
</body>
</html>
