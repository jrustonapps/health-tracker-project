<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Goal Manager</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="script/tcal.js"></script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" />${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager" class="current">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<h2>Goals</h2>
		<p>You can view and set your goals here. Active goals will update whenever you visit this page. 
		Inactive goals are just here for you to see. Nobody else will be able to view those.</p>
		<jsp:useBean id="active" type="java.util.ArrayList<healthTracker.goal.Goal>" scope="request" />
		<jsp:useBean id="inActive" type="java.util.ArrayList<healthTracker.goal.Goal>" scope="request" />
		<div class="profilePanel">
			<h3>Active Goals:</h3>
			<p>You currently have ${fn:length(active)} active goal(s)!</p>
			<table class="goalTable">
				<tr>
					<th>Started</th>
					<th>End Date</th>
					<th>Goal Target</th>
					<th>Start</th>
					<th>Options</th>
				</tr>
				<c:forEach items="${active}" var="goal">
				<fmt:formatDate value="${goal.creationDate}" pattern="dd/MM/yyyy" var="fmtDate1" />
				<fmt:formatDate value="${goal.goalDate}" pattern="dd/MM/yyyy" var="fmtDate2" />
				<tr>
					<td>${fmtDate1}</td>
					<td>${fmtDate2}</td>
					<c:choose>
					<%-- Check if the Goal is a weight goal, show the right units. --%>
					<c:when test="${goal.weightGoal}">
					<td>${goal.start + goal.target} ${wUnit}</td>
					<td>${goal.start} ${wUnit}</td>
					</c:when>
					<c:otherwise>
					<td>${goal.target} kCal</td>
					<td>${goal.start} kCal</td>
					</c:otherwise>
					</c:choose>
					<td class="options">
							<a href="GoalManager?do=edit&amp;id=${goal.id}" title="Edit goal">
								<img src="img/pencil.png" alt="Edit goal" />
							</a>
							<a href="GoalManager?rGoal=${goal.id}" title="Delete goal">
								<img src="img/bin.png" alt="Delete goal" />
							</a>
					</td>
				</tr></c:forEach>
			</table>
			<c:if test="${empty active or fn:length(active) < 3}">
			<div class="clearFix">
				<form action="" method="post">
					<fieldset class="goalSetter">
						<legend>Add New Goal:</legend>
						<input type="hidden" name="do" value="add" />
						<label for="goalType">Goal Type:</label>
						<select name="goalType" id="goalType">
							<option value="1">Weight</option>
							<option value="2">Diet</option>
						</select>
						<select name="goalWeight" id="goalWeight">
							<option value="1">Gain</option>
							<option value="2">Lose</option>
						</select>
						<select name="goalDiet" id="goalDiet">
							<option value="1">Keep</option>
							<option value="2">Burn</option>
						</select>
						<label for="goalVal">Target:</label>
						<input type="text" name="goalVal" id="goalVal" />
						<c:choose>
						<%-- Pick the right units to show if the user has set it. --%>
						<c:when test="${user.measurementPreference eq 'i'}">
							<select name="unit" class="unit">
								<option value="1">lb</option>
								<option value="2">st</option>
							</select>
						</c:when>
						<c:otherwise>
							<select name="unit" class="unit">
								<option value="1">g</option>
								<option value="2">kg</option>
							</select>
						</c:otherwise>
						</c:choose>
						<label for="goalDate">Goal Date:</label>
						<input type="text" class="tcal" name="goalDate" id="goalDate" />
						<input type="submit" value="Create Goal" class="clearFix butt" />
					</fieldset>
				</form>
			</div>
			<script>			
				/* When the user changes their goal type, remove uneeded elements. */
				$('#goalType').change(function() {
					var type = $('#goalType').val();
					if (type == 1) {
						$('#goalWeight').show();
						$('.unit').show();
						$('#goalDiet').hide();
					} else if (type == 2) {
						$('#goalWeight').hide();
						$('.unit').hide();
						$('#goalDiet').show();
					}
				});
				/* Make sure the default goal type is weight, and diet stuff is hidden. */
				window.onload = function() {
					var type = document.getElementById('goalType');
					type.value = 1;
					$('#goalWeight').show();
					$('.unit').show();
					$('#goalDiet').hide();
				};
			</script>
			</c:if>
		</div>
		<div class="profilePanel">
			<h3>Finished Goals:</h3>
			<p>These are your past goals: </p>
			<table class="goalTable">
				<tr>
					<th>Started</th>
					<th>End Date</th>
					<th>Goal Target</th>
					<th>Completed</th>
				</tr>
				<c:forEach items="${inActive}" var="goal">
				<fmt:formatDate value="${goal.creationDate}" pattern="dd/MM/yyyy" var="fmtDate1" />
				<fmt:formatDate value="${goal.goalDate}" pattern="dd/MM/yyyy" var="fmtDate2" />
				<tr>
					<td>${fmtDate1}</td>
					<td>${fmtDate2}</td>
					<c:choose>
					<c:when test="${goal.weightGoal}">
					<td>${goal.target} ${wUnit}</td>
					</c:when>
					<c:otherwise>
					<td>${goal.target} kCal</td>
					</c:otherwise>
					</c:choose>
					<%-- If the Goal is completed, show a tick. --%>
					<c:if test="${goal.completed}">
					<td><img src="img/tick.png" alt="Completed" /></td>
					</c:if>
					<%-- If the Goal is failed, show a cross. --%>
					<c:if test="${!goal.completed}">
					<td><img src="img/cross.png" alt="Failed" /></td>
					</c:if>
				</tr></c:forEach>
			</table>
		</div>
	</div>
</body>
</html>