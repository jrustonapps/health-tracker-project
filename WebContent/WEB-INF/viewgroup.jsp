<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - View Group</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/tcal.css" />
    <script type="text/javascript" src="script/tcal.js"></script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
            	<c:choose>
            	<c:when test="${user eq viewUser}">
                <li><a title="My Profile" href="Profile" class="current">My Profile</a></li>
                </c:when>
                <c:otherwise>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                </c:otherwise>
                </c:choose>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager" class="current">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
    	<jsp:useBean id="group" type="healthTracker.group.GroupDetails" scope="request" />
		<h2>Groups</h2>
		<div id="myGroupsView" class="floatLeft">
			<h3>Group - ${group.groupName}</h3>			
			<p>Group Name: ${group.groupName}</p>			
			<p>Group Description: ${group.groupDescription}</p>
			<c:choose>
            	<c:when test="${hasJoinedGroup}">
                <p>You are a member of this group.</p>                
                <form action="ViewGroup" method="post">
                	<fieldset>
						<input type="hidden" name="leaveGroup" value="${group.groupID}" class="button" />
						<input type="submit" value="Leave Group" />
					</fieldset>
				</form>
                </c:when>
                <c:otherwise>
                <p>You are not currently a member of this group.</p>                
                <form action="ViewGroup" method="post">
                	<fieldset>
						<input type="hidden" name="joinGroup" value="${group.groupID}" class="button" />
						<input type="submit" value="Join Group" />
					</fieldset>
				</form>
                </c:otherwise>
            </c:choose>            			
            </div>
            <div id="myFriends" class="floatLeft">
            <h3>Invite Friends</h3>			
			<p>To invite a friend to this group, simply enter their e-mail address below.</p>			
			<form action="InviteFriends" method="post">
				<fieldset class="fields">
					<label for="emailAddress">E-Mail Address</label>
					<input type="text" name="emailAddress" />
					<input type="hidden" name="groupID" value="${group.groupID}" />
					<input type="submit" value="Invite Friend" class="button" />
				</fieldset>
			</form>			
			</div>			
			<hr />			
			<div>
				<h3 class="center-groupProgress">Group Members</h3>			
				<c:set var="groupUserNum" scope="request" value="${0}"/>			
				<table class="usersTable">
				<c:forEach items="${users}" var="groupUser">
					<c:if test="${groupUserNum==0}"><tr></c:if>				
					<td><a href="Profile?do=viewUser&amp;id=${groupUser.userID}">${groupUser.forename} ${groupUser.surname}</a></td>				
					<c:if test="${groupUserNum==4}"></tr></c:if>				
				<c:if test="${groupUserNum==443}">
				<c:set var="groupUserNum" scope="request" value="${groupUserNum+1}"/>
				</c:if>
				
				<c:set var="groupUserNum" scope="request" value="${groupUserNum+1}"/>
				<c:if test="${groupUserNum==5}">
				<c:set var="groupUserNum" scope="request" value="${0}"/>
				</c:if>			
				</c:forEach>			
				<c:if test="${groupUserNum!=0}"></tr></c:if>
				</table>
			</div>			
			<hr />
			<c:if test="${hasJoinedGroup}">
			<h3 class="center-groupProgress">Group Goals</h3>			
			<c:forEach items="${groupGoals}" var="groupGoal">
				<p class="center-groupProgress">
				<c:if test="${groupGoal.weightGoal}">
					<c:if test="${groupGoal.target>0}">+</c:if>${groupGoal.target} ${wUnit}
				</c:if>
				<c:if test="${!groupGoal.weightGoal}">
					<c:if test="${groupGoal.target>0}">+</c:if>${groupGoal.target} kCal
				</c:if>
				by <fmt:formatDate value="${groupGoal.goalDate}" pattern="dd/MM/yyyy" /> - <a href="ViewGroupProgress?gID=${group.groupID}&glID=${groupGoal.id}">View Group's Progress</a>
				<c:if test="${groupGoal.canSubscribe}">
					- <a href="AddGroupGoal?id=${groupGoal.id}">Subscribe To Goal</a>
				</c:if>				
				</p>			
			</c:forEach>			
			<c:if test="${empty active or fn:length(active) < 3}">
			<div class="clearFix">
				<form action="GoalManager" method="post">
					<fieldset class="goalSetter">
						<legend>Add New Goal:</legend>
						<input type="hidden" name="do" value="add" />
						<label for="goalType">Goal Type:</label>
						<select name="goalType" id="goalType">
							<option value="1">Weight</option>
							<option value="2">Diet</option>
						</select>
						<select name="goalWeight" id="goalWeight">
							<option value="1">Gain</option>
							<option value="2">Lose</option>
						</select>
						<select name="goalDiet" id="goalDiet">
							<option value="1">Keep</option>
							<option value="2">Burn</option>
						</select>
						<label for="goalVal">Target:</label>
						<input type="text" name="goalVal" id="goalVal" />
							<select name="unit" class="unit">
								<option value="1">g</option>
								<option value="2">kg</option>
							</select>
						<label for="goalDate">Goal Date:</label>
						<input type="text" class="tcal" name="goalDate" id="goalDate" />
						<input type="hidden" name="groupID" value="${group.groupID}" />
						<input type="submit" value="Create Goal" class="clearFix butt" />
					</fieldset>
				</form>				
				<script>			
				/* When the user changes their goal type, remove uneeded elements. */
				$('#goalType').change(function() {
					var type = $('#goalType').val();
					if (type == 1) {
						$('#goalWeight').show();
						$('.unit').show();
						$('#goalDiet').hide();
					} else if (type == 2) {
						$('#goalWeight').hide();
						$('.unit').hide();
						$('#goalDiet').show();
					}
				});
				/* Make sure the default goal type is weight, and diet stuff is hidden. */
				window.onload = function() {
					var type = document.getElementById('goalType');
					type.value = 1;
					$('#goalWeight').show();
					$('.unit').show();
					$('#goalDiet').hide();
				};
			</script>
			</div>			
			<p class="center-groupProgress">Members of this group will have the option to decide if they would like to participate in the group goal or not.</p>
			</c:if>
			<c:if test="${fn:length(active) >= 3}">
				<p class="center-groupProgress">You are already subscribed to 3 goals. Please complete/delete one.</p>
			</c:if>
			</c:if>
		</div>
	</body>
</html>
