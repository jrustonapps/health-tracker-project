<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Diet Manager</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="script/tcal.js"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<fmt:formatDate value="${date}" pattern="dd/MM/yyyy" var="fmtDate" />
    <script>
    	/* Use AJAX to grab a listing of all groups related to the search terms. */
		$(document).ready(function() {
			$('#searchQuery').keyup(function() {
				var searchString = $('#searchQuery').val();
				$.get('AjaxServlet', {fSearch: searchString, date: '${fmtDate}'}, function(response) {
					$('#searchArea').hide().html(response).fadeIn();
				}, 'html');
			});
			$('#searchQuery').keyup();
			/* Stop the user submitting the form with enter, it's annoying. */
			$('#searchQuery').keydown(function(event) {
				if (event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});
		});
	</script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" />${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager" class="current">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<!-- This is here just in case JavaScript is disabled. -->
		<noscript><p>You need JavaScript enabled in your browser to use this page!</p></noscript>
		<h2>Diet Manager</h2>
		<c:if test="${futDate}">
		<h3 class="center">You cannot use future dates!</h3>
		</c:if>
		<div class="center">
			<p><strong>Select the date for dietary entry or editing!</strong></p>
			<form action="" method="get">
				<fieldset class="captureForm">
					<%-- Add class="tcal" to your input field to use TCal. --%>
					<input type="text" name="d" class="tcal" value="${fmtDate}" />
					<input type="hidden" name="do" value="show" />
					<input type="submit" value="Show Date" class="submit" />
				</fieldset>
			</form>
		</div>
		<jsp:useBean id="foodHist" type="healthTracker.user.UserActivities" scope="request" />
		<table class="center">
			<tr>
				<th>Meal</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Calories (per unit)</th>
				<th>Total Calories</th>
				<th>Options</th>
			</tr>
			<c:forEach items="${foodHist.food}" var="food">
			<tr>
				<c:if test="${food.meal=='b'}">
				<td>Breakfast</td>
				</c:if>
				<c:if test="${food.meal=='l'}">
				<td>Lunch</td>
				</c:if>
				<c:if test="${food.meal=='d'}">
				<td>Dinner</td>
				</c:if>
				<c:if test="${food.meal=='s'}">
				<td>Snack</td>
				</c:if>
				<td>${food.name}</td>
				<td>${food.quantity}</td>
				<td>${food.cals}</td>
				<td>${food.cals*food.quantity}</td>
				<td class="options">
					<a href="" title="Edit Food">
						<img src="img/pencil.png" />
					</a>
					<a href="?do=remove&amp;id=${food.foodID}&amp;d=${fmtDate}&amp;m=${food.meal}" title="Remove Food">
						<img src="img/bin.png" />
					</a>
				</td>
			</tr>
			</c:forEach>
		</table>
		<hr />
		<div class="center">
			<p><strong>Search for food eaten, pick the meal you had it for and how much you had.</strong></p>
			<form action="" method="get">
				<fieldset>
					<input type="text" name="searchQuery" id="searchQuery" />
				</fieldset>
			</form>
			<table class="resultTableFood">
				<tr>
					<th>Name</th>
					<th>Calories</th>
					<th>Description</th>
					<th>Quantity</th>
				</tr>
			</table>
			<table class="resultTableFood" id="searchArea">
				<tr>
					<td>
						<%-- AJAX will push the group list here. --%>
						<noscript>You need JavaScript and an AJAX compatible browser to use this.</noscript>
					</td>
				</tr>
			</table>
			<hr />
			<form action="" method="post" id="food_form">
				<fieldset class="newItem">
					<legend>New Food:</legend>
					<input type="hidden" name="do" value="add" />
					<label for="foodName">Food Name: <span class="red"><sup>*</sup></span></label>
						<input type="text" name="foodName" id="foodName" />
					<label for="portion">Portion: <span class="red"><sup>*</sup></span></label>
						<input type="text" name="portion" id="portion" />
						<select name="measurement" id="measurement">
							<option value="g">g</option>
							<option value="kg">kg</option>
							<option value="lb">lb</option>
							<option value="oz">oz</option>
							<option value="ml">ml</option>
							<option value="l">l</option>
						</select>
					<label for="calories">Calories per portion (kCal)<span class="red"><sup>*</sup></span>:</label>
						<input type="text" name="calories" id="calories" />
					<label for="desc">Description:</label>
						<textarea name="desc" id="desc"></textarea>
					<input type="submit" value="Add Food" name="submit" class="button" />
					<input type="reset" value="Clear" name="reset" class="button" />
					<p><span class="red"><sup>*</sup></span> Means the field is required!</p>
				</fieldset>
			</form>
		</div>
	</div>
</body>
</html>
