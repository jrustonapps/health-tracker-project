<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" class="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>${pageContext.servletContext.servletContextName} - Home</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
</head>
<body>
	<div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" />${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index" class="current">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<h2>Welcome to ${pageContext.servletContext.servletContextName}!</h2>
		<div class="indexPanel">
			<h3>What is this?</h3>
			<p>Health tracker is a service dedicated to helping you manage your fitness, whether that be helping you plan an exercise regime, or helping you work out what you can cut from your diet, we are here to give a helping hand.</p>
		</div>
		<div class="indexPanel">
			<h3>How does it work?</h3>
			<p>It's simple really, you create an account, provide us with your weight and height, then every day you just need to update your food intake and exercise performed throughout the day. Don't worry about working out the hard stuff, we do that for you with our clever software to work out exactly how many calories you took in, and how many you burned! We then show this to you in a simple, understandable way!</p>
		</div>
		<div class="indexPanel">
			<h3>I'm interested, where do I start!</h3>
			<p>Awesome news! And starting couldn't be simpler, just <a href="Register" title="register">register</a> yourself with us, and that's it! Nothing more! Once you've registered all you need to do is provide details about your diet and exercise whenever you can (daily is best), and we'll work out all you need to know to make changes.</p>
		</div>
		<div>
			<h2 align=center>Disclaimer</h2>
			<p align=center>This application is not a commercial application and does not provide insurance. This is a study project that is part of a Computing Science module taught at the University of East Anglia, Norwich, UK. If you have any questions, please contact the module coordinator, Joost Noppen, at j.noppen@uea.ac.uk</p>
		</div>
    </div>
</body>
</html>