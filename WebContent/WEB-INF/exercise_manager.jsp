<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<fmt:formatDate value="${date}" pattern="dd/MM/yyyy" var="fmtDate" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Exercise Manager</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="script/tcal.js"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script>
    	/* Use AJAX to grab a listing of all groups related to the search terms. */
		$(document).ready(function() {
			$('#searchQuery').keyup(function() {
				var searchString = $('#searchQuery').val();
				$.get('AjaxServlet', {exSearch: searchString, date: '${fmtDate}'}, function(response) {
					$('#searchArea').hide().html(response).fadeIn();
				}, 'html');
			});
			$('#searchQuery').keyup();
			/* Stop the user submitting the form with enter, it's annoying. */
			$('#searchQuery').keydown(function(event) {
				if (event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});
		});
	</script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" />${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager" class="current">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<noscript><p>You need JavaScript enabled in your browser to use this page!</p></noscript>
		<h2>Exercise Manager</h2>
		<div class="center">
			<p><strong>Select the date for exercise entry or editing!</strong></p>
			<form action="" method="get">
				<fieldset class="captureForm">
					<%-- Add class="tcal" to your input field to use TCal. --%>
					<input type="text" name="d" class="tcal" value="${fmtDate}" />
					<input type="hidden" name="do" value="show" />
					<input type="submit" value="Show Date" class="submit" />
				</fieldset>
			</form>
		</div>
		<jsp:useBean id="exerciseHist" type="java.util.ArrayList<healthTracker.captures.Exercise>" scope="request" />
		<table class="center">
			<tr>
				<th>Name</th>
				<th>Calories per Hour</th>
				<th>Description</th>
				<th>Hours Completed</th>
				<th>Options</th>
			</tr>
			<c:forEach items="${exerciseHist}" var="exercise">
			<tr>
				<td>${exercise.name}</td>
				<td>${exercise.calsPerUnit}</td>
				<td>${exercise.description}</td>
				<td>${exercise.quantity}</td>
				<td class="options">
					<a href="?do=edit&amp;id=${exercise.exID}&amp;d=${fmtDate}" title="Edit Exercise">
						<img src="img/pencil.png" />
					</a>
					<a href="?do=remove&amp;id=${exercise.exID}&amp;d=${fmtDate}" title="Remove Exercise">
						<img src="img/bin.png" />
					</a>
				</td>
			</tr>
			</c:forEach>
		</table>
		<hr />
		<div class="center">
			<p><strong>Select an exercise and then select the time taken to complete that exercise!</strong></p>
			<form action="" method="get">
				<fieldset>
					<input type="text" name="searchQuery" id="searchQuery" />
				</fieldset>
			</form>
			<table class="resultTable">
				<tr>
					<th>Name</th>
					<th>Calories per Hour</th>
					<th>Hours</th>
				</tr>
			</table>
			<table class="resultTable" id="searchArea">
				<tr>
					<td>
						<%-- AJAX will push the group list here. --%>
						<noscript>You need JavaScript and an AJAX compatible browser to use this.</noscript>
					</td>
				</tr>
			</table>
			<hr />
			<form action="" method="post" id="exercise_form">
				<fieldset class="newItem">
					<legend>New Exercise:</legend>
					<input type="hidden" name="do" value="add" />
					<label for="exerciseName">Exercise Name: <span class="red"><sup>*</sup></span></label>
						<input type="text" name="exerciseName" id="exerciseName" />
					<label for="calories">Calories per hour (kCal): <span class="red"><sup>*</sup></span></label>
						<input type="text" name="calories" id="calories" />
					<label for="desc">Description:</label>
						<textarea name="desc" id="desc"></textarea>
					<input type="submit" value="Add Exercise" name="submit" class="button" />
					<input type="reset" value="Clear" name="reset" class="button" />
					<p><span class="red"><sup>*</sup></span> Means the field is required!</p>
				</fieldset>
			</form>
		</div>
    </div>
</body>
</html>