<?xml version="1.0" encoding="UTF-8" ?>
<%@page import="healthTracker.utils.Conversions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/199/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - My Profile</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
	<script src="script/rgraph/RGraph.common.core.js" ></script>
    <script src="script/rgraph/RGraph.common.dynamic.js" ></script>
    <script src="script/rgraph/RGraph.common.tooltips.js" ></script>
    <script src="script/rgraph/RGraph.common.effects.js" ></script>
    <script src="script/rgraph/RGraph.line.js" ></script>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
	<jsp:useBean id="viewUser" type="healthTracker.user.SystemUser" scope="request" />
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
            	<c:choose>
            	<c:when test="${user eq viewUser}">
                <li><a title="My Profile" href="Profile" class="current">My Profile</a></li>
                </c:when>
                <c:otherwise>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                </c:otherwise>
                </c:choose>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<!-- This is here just in case JavaScript is disabled. -->
		<noscript><p>You need JavaScript enabled in your browser to use this page!</p></noscript>
		<h2>${viewUser.forename} ${viewUser.surname}</h2>
		<div id="profileMain">
			<h3>Health Statistics</h3>
			<canvas id="cvs" width="525" height="250">
				Your browser doesn't support this.
				Upgrade to the latest version.
			</canvas>
			<input id="bmi_chart" class="graphButton leftRound" type="button" value="BMI" />
			<input id="weight_chart" class="rightRound graphButton" type="button" value="Weight" />
			<jsp:useBean id="previousBMI" type="java.util.ArrayList<java.lang.Double>" scope="request" />
			<jsp:useBean id="previousWeight" type="java.util.ArrayList<java.lang.Double>" scope="request" />
			<%-- Just a note, the errors picked up by Eclipse here aren't actually problems. Ignore them. --%>
			<script>
				$('#bmi_chart').click(function() {
					$('#bmi_chart').addClass("graphButton_a");
					$('#weight_chart').removeClass("graphButton_a");
					RGraph.Reset(document.getElementById('cvs'));
					var graph = new RGraph.Line('cvs', [<%
					for (int i = 0; i < previousBMI.size(); i++) {
						out.print(previousBMI.get(i));
					    if (i < previousBMI.size()-1) {
					    	out.print(",");
						}
					}
					%>]);
					graph.Set('chart.title', 'BMI History');
					graph.Set('chart.curvy', true);
					graph.Set('chart.curvy.tickmarks', true);
					graph.Set('chart.curvy.tickmarks.fill', null);
					graph.Set('chart.curvy.tickmarks.stroke', '#aaa');
					graph.Set('chart.curvy.tickmarks.stroke.linewidth', 2);
					graph.Set('chart.curvy.tickmarks.size', 5);
					graph.Set('chart.linewidth', 3);
					graph.Set('chart.hmargin', 5);
					graph.Set('chart.tickmarks', 'circle');
					graph.Set('chart.tooltips', [<%
					for (int i = 0; i < previousBMI.size(); i++) {
						out.print("'" + Conversions.roundTwoDecimals(previousBMI.get(i)) + "'");
						if (i < previousBMI.size()-1) {
							out.print(",");
						}
					}
					%>]);
					graph.Set('chart.ymin', ${sBMI});
					graph.Set('chart.ymax', ${lBMI});
					graph.Set('chart.colors', ['#f4d116']);
					RGraph.Effects.Line.jQuery.Trace(graph);
					graph.Draw();
				});
				
				$('#weight_chart').click(function() {
					$('#weight_chart').addClass("graphButton_a");
					$('#bmi_chart').removeClass("graphButton_a");
					RGraph.Reset(document.getElementById('cvs'));
					var graph = new RGraph.Line('cvs', [<%
                    for (int i = 0; i < previousWeight.size(); i++) {
						out.print(previousWeight.get(i));
						if (i < previousWeight.size()-1) {
							out.print(",");
						}
					}
					%>]);
					graph.Set('chart.title', 'Weight History (${measurementType})');
					graph.Set('chart.curvy', true);
					graph.Set('chart.curvy.tickmarks', true);
					graph.Set('chart.curvy.tickmarks.fill', null);
					graph.Set('chart.curvy.tickmarks.stroke', '#aaa');
					graph.Set('chart.curvy.tickmarks.stroke.linewidth', 2);
					graph.Set('chart.curvy.tickmarks.size', 5);
					graph.Set('chart.linewidth', 3);
					graph.Set('chart.hmargin', 5);
					graph.Set('chart.tickmarks', 'circle');
					graph.Set('chart.tooltips', [<%
					for (int i = 0; i < previousWeight.size(); i++) {
						out.print("'" + previousWeight.get(i) + "'");
						if (i < previousWeight.size()-1) {
							out.print(",");
						}
					}
					%>]);
					graph.Set('chart.colors', ['#000']);
					graph.Set('chart.ymin', ${sWeight});
					graph.Set('chart.ymax', ${lWeight});
					RGraph.Effects.Line.jQuery.Trace(graph);
					graph.Draw();
				});
				
				window.onload = $('#bmi_chart').click();
			</script>
			<div>
			<c:choose>
			<c:when test="${user eq viewUser}">
			<h4>Good ${timePeriod}, ${viewUser.forename}. Your current weight is ${weight} ${measurementType}, giving you a BMI of
			${bmi}.
			<c:choose>
				<c:when test="${needsToChangeWeight}">
			Based on your height, you need to ${wordToUse} ${needsToLose} ${measurementType} to reach a healthy
			BMI of 18.5 - 24.9.
				</c:when>
				<c:otherwise>
			Your BMI fits into the healthy band of 18.5 - 24.9, so you do not need to lose any weight.
				</c:otherwise>
				</c:choose></h4>
			<a href="BMISettings" title="BMI Settings" class="profileButton">Adjust Statistics</a>
			</c:when>
			<c:otherwise>
			<h4>Good ${timePeriod}, ${user.forename}. This isn't your profile. The stats for ${viewUser.forename} that you can see 
			won't be as complete as what you see normally.</h4>
			</c:otherwise>
			</c:choose>
			</div>
		</div>
		<hr />
		<div class="profilePanel">
			<h3>Active Goals</h3>
			<jsp:useBean id="activeGoals" type="java.util.ArrayList<healthTracker.goal.Goal>" scope="request" />
			<table class="profileTable">
                <tr>
                    <th>Target</th>
                    <th>End Date</th>
                </tr>
                <c:forEach items="${activeGoals}" var="goal">
                <tr>
                <c:if test="${goal.weightGoal}">
                	<c:choose>
	                	<c:when test="${goal.target > 0}">
	                		<td>Gain ${goal.target} ${measurementType}</td>
	                	</c:when>
	                	<c:otherwise>
	                		<td>Lose ${goal.target} ${measurementType}</td>
	                	</c:otherwise>
                	</c:choose>
                	<td><fmt:formatDate value="${goal.goalDate}" pattern="dd/MM/yyyy" /></td>
                </c:if>
                <c:if test="${!goal.weightGoal}">
                	<c:choose>
	                	<c:when test="${goal.target > 0}">
	                		<td>Keep ${goal.target} KCal</td>
	                	</c:when>
	                	<c:otherwise>
	                		<td>Burn ${goal.target} KCal in exercise</td>
	                	</c:otherwise>
                	</c:choose>
                	<td><fmt:formatDate value="${goal.goalDate}" pattern="dd/MM/yyyy" /></td>
                </c:if>
                </tr></c:forEach>
               
			</table>
		</div>
		<div class="profilePanel">
			<h3>Calorie Count</h3>
			<table class="profileTable">
                <tr>
                    <th>Date</th>
                    <th>Food</th>
                    <th>Exercise</th>
                    <th>Results</th>
                </tr>
                
                <c:forEach items="${lastConsumed}" var="userAct">
					<tr>
						<c:set var="totalEaten" scope="request" value="0"/>
						<c:set var="totalBurned" scope="request" value="1500"/>
						
						<c:forEach items="${userAct.food}" var="foodConsumed">
							<c:set var="totalEaten" scope="request" value="${totalEaten + (foodConsumed.cals*foodConsumed.quantity)}"/>
						</c:forEach>
						
						<c:forEach items="${userAct.exercise}" var="exerciseConsumed">
							<c:set var="totalBurned" scope="request" value="${totalBurned + (exerciseConsumed.calsPerUnit*exerciseConsumed.quantity)}"/>
						</c:forEach>
						
						<td><fmt:formatDate value="${userAct.date}" pattern="dd/MM/yyyy" /></td>
						<td>${totalEaten}</td>
						<td>${totalBurned}</td>
						<td>
							<c:if test="${(totalEaten-totalBurned)>0}">
								${totalEaten-totalBurned} cals gained
							</c:if>
							<c:if test="${(totalEaten-totalBurned)<=0}">
								${-1*(totalEaten-totalBurned)} cals lost
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</table>
			
		</div>
    </div>
    
</body>
</html>