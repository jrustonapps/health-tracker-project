<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Edit Goal</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
    <!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="script/tcal.js"></script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
            	<c:choose>
            	<c:when test="${user eq viewUser}">
                <li><a title="My Profile" href="Profile" class="current">My Profile</a></li>
                </c:when>
                <c:otherwise>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                </c:otherwise>
                </c:choose>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager" class="current">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
    	<h2>Edit Goal</h2>
    	<jsp:useBean id="goal" type="healthTracker.goal.Goal" scope="request" />
    	<form action="GoalManager" method="post">
			<fieldset class="goalSetter">
				<input type="hidden" name="do" value="editGoal" />
				<input type="hidden" name="goal" value="${goal.id}" />
				<input type="hidden" name="lose" value="${goal.target < 0}" />
				<label for="goalType">Goal Type:</label>
				<c:choose>
					<c:when test="${goal.weightGoal}">
						<c:choose>
							<c:when test="${goal.target < 0}">
				<p>Weight loss</p>
							</c:when>
							<c:otherwise>
				<p>Weight gain</p>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${goal.target < 0}">
				<p>Calories burned</p>
							</c:when>
							<c:otherwise>
				<p>Calories kept</p>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<label for="goalVal">Goal Target:</label>
				<input type="text" name="goalVal" id="goalVal" value="${val}" />
				<c:if test="${goal.weightGoal}">
				<c:choose>
				<%-- Pick the right units to show if the user has set it. --%>
				<c:when test="${user.measurementPreference eq 'i'}">
					<select name="unit" class="unit">
						<option value="1">lb</option>
						<option value="2" selected="selected">st</option>
					</select>
				</c:when>
				<c:otherwise>
					<select name="unit" class="unit">
						<option value="1">g</option>
						<option value="2" selected="selected">kg</option>
					</select>
				</c:otherwise>
				</c:choose>
				</c:if>
				<fmt:formatDate value="${goal.goalDate}" pattern="dd/MM/yyyy" var="fmtDate" />
				<label for="goalDate">Goal Date:</label>
				<input type="text" class="tcal" name="goalDate" id="goalDate" value="${fmtDate}" />
				<input type="submit" value="Edit Goal" class="clearFix butt" />
			</fieldset>
		</form>
    </div>
</body>
</html>