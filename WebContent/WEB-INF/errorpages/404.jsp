<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true" %>
    <% response.setStatus(404); %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/199/xhtml">
<head>
	<!-- Set this tag so that relative URLs actually point correctly. -->
	<base href="http://localhost:8080/Health_Tracker/" />
    <title>${pageContext.servletContext.servletContextName} - 404</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
	<script src="script/rgraph/RGraph.common.core.js" ></script>
    <script src="script/rgraph/RGraph.common.dynamic.js" ></script>
    <script src="script/rgraph/RGraph.common.tooltips.js" ></script>
    <script src="script/rgraph/RGraph.common.effects.js" ></script>
    <script src="script/rgraph/RGraph.line.js" ></script>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <li><a title="Guide" href="user_guide.htm">User Guide</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<h2>Page not found!</h2>
		<p>We're sorry, but the page you requested doesn't seem to exist.</p>
		<p>If you know it does, and this continues to happen, please contact us!</p>
		<a href="javascript:history.back();" title="Go back">Go back.</a>
    </div>
</body>
</html>