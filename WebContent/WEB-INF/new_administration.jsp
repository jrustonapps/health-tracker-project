<?xml version="1.0" encoding="UTF-8" ?>	
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Administration</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
            	<c:choose>
            	<c:when test="${user eq viewUser}">
                <li><a title="My Profile" href="Profile" class="current">My Profile</a></li>
                </c:when>
                <c:otherwise>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                </c:otherwise>
                </c:choose>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel" class="current">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<h2>Administration</h2>
		<div class="floatLeft">
			<jsp:useBean id="users" type="java.util.ArrayList<healthTracker.user.SystemUser>" scope="request" />
			<h3>Users</h3>
			<table class="adminHeader">
				<tr>
					<th>Username</th>
					<th>D.O.B</th>
					<th>Options</th>
				</tr>
			</table>
			<div class="tableScroll">
				<table class="adminCells">
					<c:forEach items="${users}" var="usr">
					<tr>
						<td>${usr.forename} ${usr.surname}</td>
						<td>${usr.dateOfBirth}</td>
						<td class="options">
							<a href="Profile?do=viewUser&amp;id=${usr.userID}" title="View User">
								<img src="img/magnifier.png" alt="View User" />
							</a>
							<a href="PerformAdminAction?do=removeUser&amp;id=${usr.userID}" title="Remove User">
								<img src="img/bin.png" alt="Remove User" />
							</a>
						</td>
					</tr></c:forEach>
				</table>
			</div>
		</div>
		<div class="floatRight">
			<h3>Groups</h3>
			<table class="adminHeader">
				<tr>
					<th>Group Name</th>
					<th>Options</th>
				</tr>
			</table>
			<div class="tableScroll">
				<table class="adminCells">
					<c:forEach items="${groups}" var="group">
					<tr>
						<td>${group.groupName}</td>
						<td class="options">
							<a href="PerformAdminAction?do=removeGroup&amp;id=${group.groupID}" title="Remove Group">
								<img src="img/bin.png" alt="Remove Group" />
							</a>
						</td>
					</tr>
					</c:forEach>
					
				</table>
			</div>
		</div>
		<hr />
		<div class="floatLeft">
			<jsp:useBean id="unapprovedExercises" type="java.util.ArrayList<healthTracker.group.GroupDetails>" scope="request" />
			<h3>New Exercises</h3>
			<table class="adminHeader">
				<tr>
					<th>Exercise</th>
					<th>KCal Burned</th>
					<th>Options</th>
				</tr>
			</table>
			<div class="tableScroll">
				<table class="adminCells">
					<c:forEach items="${unapprovedExercises}" var="ex">
					<tr>
						<td>${ex.name}</td>
						<td>${ex.calsPerUnit}</td>
						<td class="options">
							<a href="PerformAdminAction?do=acceptExercise&amp;id=${ex.exID}" title="Accept exercise">
								<img src="img/add.png" alt="Accept" />
							</a>
							<a href="PerformAdminAction?do=removeExercise&amp;id=${ex.exID}" title="Remove exercise">
								<img src="img/bin.png" alt="Remove exercise" />
							</a>
						</td>
					</tr></c:forEach>
				</table>
			</div>
		</div>
		<div class="floatRight">
			<jsp:useBean id="unapprovedFood" type="java.util.ArrayList<healthTracker.captures.Food>" scope="request" />
			<h3>New Food Items</h3>
			<table class="adminHeader foodCells">
				<tr>
					<th>Food</th>
					<th>KCal</th>
					<th>Options</th>
				</tr>
			</table>
			<div class="tableScroll">
				<table class="adminCells foodCells">
					<c:forEach items="${unapprovedFood}" var="food">
					<tr>
						<td>${food.name}</td>
						<td>${food.cals}</td>
						<td class="options">
							<a href="PerformAdminAction?do=acceptFood&amp;id=${food.foodID}" title="Accept exercise">
								<img src="img/add.png" alt="Accept" />
							</a>
							<a href="PerformAdminAction?do=removeFood&amp;id=${food.foodID}" title="Remove exercise">
								<img src="img/bin.png" alt="Remove exercise" />
							</a>
						</td>
					</tr></c:forEach>
				</table>
			</div>
		</div>
    </div>
</body>
</html>