<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Group Manager</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script>
    	/* Use AJAX to grab a listing of all groups related to the search terms. */
		$(document).ready(function() {
			$('#searchQuery').keyup(function() {
				var searchString = $('#searchQuery').val();
				$.get('AjaxServlet', {groupSearch: searchString}, function(response) {
					$('#searchArea').hide().html(response).fadeIn();
				}, 'html');
			});
			$('#searchQuery').keyup();
			/* Stop the user submitting the form with enter, it's annoying. */
			$('#searchQuery').keydown(function(event) {
				if (event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});
		});
	</script>
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index">Home</a></li>
            	<c:if test="${not empty user}">
            	<%-- Only users should see the rest of the tabs. --%>
            	<c:choose>
            	<c:when test="${user eq viewUser}">
                <li><a title="My Profile" href="Profile" class="current">My Profile</a></li>
                </c:when>
                <c:otherwise>
                <li><a title="My Profile" href="Profile">My Profile</a></li>
                </c:otherwise>
                </c:choose>
                <li><a title="Diet Manager" href="DietManager">Diet Manager</a></li>
                <li><a title="Exercise Manager" href="ExerciseManager">Exercise Manager</a></li>
                <li><a title="Goals" href="GoalManager">Goals</a></li>
                <li><a title="Groups" href="GroupManager" class="current">Groups</a></li>
                <c:if test="${user.admin}">
                <%-- Only administrators need to be able to see this tab. --%>
                <li><a title="Administration" href="AdminPanel">Administration</a></li>
                </c:if>	
                </c:if>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<noscript><p>You need JavaScript enabled in your browser to use this page!</p></noscript>
		<h2>Groups</h2>
		<div id="myGroups">
			<h3>My Groups</h3>			
			<c:choose>
			<c:when test="${empty myGroups}">
				<p>You are not part of any groups! Find some, or to create one <a href="CreateGroup">click here</a>.</p>
			</c:when>
			<c:otherwise>
				<p>To create a new group, <a href="CreateGroup">click here</a>.</p>
			</c:otherwise>
			</c:choose>
			<div class="outerGroupWrapper">
				<div class="innerGroupWrapper">
				<jsp:useBean id="myGroups" type="java.util.ArrayList<healthTracker.group.GroupDetails>" scope="request" />
				<c:forEach items="${myGroups}" var="group">
					<div class="groupBox">
						<a href="ViewGroup?id=${group.groupID}" title="View ${group.groupName}">
							<img src="img/users.png" alt="${group.groupName} Logo" />
							${group.groupName}
						</a>
					</div>
				</c:forEach>
				</div>
			</div>
		</div>
		<hr />
		<h3>Find Groups</h3>
		<p>Start typing things to find groups about them.</p>
		<div id="findGroups">
			<form action="" method="get">
				<fieldset>
					<input type="text" name="searchQuery" id="searchQuery" />
				</fieldset>
			</form>
			<div class="outerGroupWrapper">
				<div id="searchArea" class="innerGroupWrapper">
					<%-- AJAX will push the group list here. --%>
					<noscript>You need JavaScript and an AJAX compatible browser to use this properly.</noscript>
				</div>
			</div>
		</div>
    </div>
</body>
</html>