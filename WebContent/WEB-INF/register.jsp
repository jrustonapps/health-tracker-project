<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty user}">
    <jsp:useBean id="user" type="healthTracker.user.SystemUser" scope="session" />
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>${pageContext.servletContext.servletContextName} - Register</title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/desktop.css" type="text/css" media="screen" />
</head>
<body>
    <div id="headerWrapper">
        <div id="header">
            <div id="logo">
                <img src="img/logo.png" alt="${pageContext.servletContext.servletContextName}" />
				<h1>${pageContext.servletContext.servletContextName}</h1>
			</div>
            <div id="headerSubTabs">
                <ul>
                	<c:choose>
                	<c:when test="${not empty user}">
                	<li><a href="Profile" title="Profile">
                		<img src="img/user.png" /> ${user.forename} ${user.surname}
                	</a></li>
                	<li><a href="PersonalSettings" title="Settings">Settings</a></li>
                	<li><a href="Login?do=logout" title="Sign Out">Sign Out</a></li>
                	</c:when>
                	<c:otherwise>
                    <li><a href="Register">Register</a></li>
                    <li><a href="Login">Sign In</a></li>
                    </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <ul id="headerTabs">
                <li><a title="Home" href="Index" class="current">Home</a></li>
            </ul>
        </div>
    </div>
    <div id="bodyCenter">
		<h2>Register</h2>
		<c:choose>
		<c:when test="${not empty user}">
			<p>You already have an account.</p>
		</c:when>
		<c:otherwise>
		<form action="Register" method="post" id="register_form">
			<fieldset class="fields">
				<label for="registerFirstname">First Name: <span class="red"><sup>*</sup></span></label>
					<input type="text" name="firstName" id="registerFirstname" />
				<label for="registerSurname">Surname: <span class="red"><sup>*</sup></span></label>
					<input type="text" name="surname" id="registerSurname" />
				<label for="registerSex">Sex:</label>
					<select name="sex" id="registerID">
						<option value="m">Male</option>
						<option value="f">Female</option>
					</select>
				<label for="registerDOB">D.O.B: <span class="red"><sup>*</sup></span></label>
					<input type="text" name="dob" id="registerDOB" placeholder="dd/mm/yyyy" />
				<label for="registerCounty">County: <span class="red"><sup>*</sup></span></label>
					<input type="text" name="county" id="registerCounty" />
				<label for="registerEmail">Email: <span class="red"><sup>*</sup></span></label>
					<input type="text" name="email" id="registerEmail" />
				<label for="registerPassword">Password: <span class="red"><sup>*</sup></span></label>
					<input type="password" name="password" id="registerPassword" />
				<label for="registerPasswordConfirmation">Password Confirmation: <span class="red"><sup>*</sup></span></label>
					<input type="password" name="PasswordConfirmation" id="registerPasswordConfirmation" />
				<input type="submit" value="Continue" name="submit" class="button" />
				<input type="reset" value="Reset" name="reset" class="button" />
			</fieldset>
			<p><span class="red"><sup>*</sup></span> Means the field is required!</p>
		</form>
		</c:otherwise>
		</c:choose>
    </div>
</body>
</html>
