package healthTracker.captures;

import static org.junit.Assert.*;
import healthTracker.utils.DatabaseController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

public class ExerciseCaptureTest {
	
	DatabaseController db = new DatabaseController();
	PreparedStatement sql = null;
	ResultSet rs;

	@Test
	public void testAddExerciseItem() {
		assertTrue(ExerciseCapture.addExerciseItem("TESTEXERCISE", "For Exercising TEST"
				, 1000, true));
		
	}

	@Test
	public void testGetExerciseOnDate() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO userActivities(userid,actdate) VALUES(1,'01-01-1901') RETURNING actid");
			rs = db.runQuery(sql);
			rs.next();
			int actid = rs.getInt(1);
			sql = db.getConnection().prepareStatement("INSERT INTO actexercise VALUES(?,1,1)");
			sql.setInt(1, actid);
			db.runUpdate(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("01-01-1901").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(ExerciseCapture.getExerciseOnDate(inputDate, 1));
	}

	@Test
	public void testRemoveExerciseWithID() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO userActivities(userid,actdate) VALUES(1,'01-01-1901') RETURNING actid");
			rs = db.runQuery(sql);
			rs.next();
			int actid = rs.getInt(1);
			sql = db.getConnection().prepareStatement("INSERT INTO actexercise VALUES(?,1,1)");
			sql.setInt(1, actid);
			db.runUpdate(sql);
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			java.sql.Date inputDate = null;
			try {
				inputDate = new java.sql.Date(formatter.parse("01-01-1901").getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ExerciseCapture.removeExerciseWithID(inputDate, 1);
			
			
			fail("Check This Method");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAllApproved() {
		assertNotNull(ExerciseCapture.getAllApproved());
	}

	@Test
	public void testSearchApproved() {
		assertNotNull(ExerciseCapture.searchApproved("TESTEXERCISE"));
	}

	@Test
	public void testGetExerciseFromID() {
		assertNotNull(ExerciseCapture.getExerciseFromID(1));
	}

	@Test
	public void testAddPerformedExercise() {
		try {
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM actexercise WHERE exerciseid = 1");
			rs = db.runQuery(sql);
			rs.next();
			int originValue = rs.getInt(1);
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			java.sql.Date inputDate = null;
			try {
				inputDate = new java.sql.Date(formatter.parse("01-01-1901").getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ExerciseCapture.addPerformedExercise(inputDate, 1, 1, 1);
			
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM actexercise WHERE exerciseid = 1");
			rs = db.runQuery(sql);
			rs.next();
			int newValue = rs.getInt(1);
			
			assertTrue("If newValue is greater, a record has been added", newValue > originValue);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
