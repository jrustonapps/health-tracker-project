package healthTracker.captures;

import static org.junit.Assert.*;
import healthTracker.utils.DatabaseController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class DietCaptureTest {
	
	DatabaseController db = new DatabaseController();
	PreparedStatement sql = null;
	ResultSet rs;
	
	@Test
	public void testAddFoodItem() {
		int id = DietCapture.addFoodItem("TESTFOODITEM", 4321, "TEST FOOD ITEM");
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM Food WHERE foodid =?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Test to see if added food item exists", rs.getInt("foodID") == id);
			
			//clean up
			sql = db.getConnection().prepareStatement("DELETE FROM food WHERE foodid =?");
			sql.setInt(1, id);
			db.runUpdate(sql);
			
		} catch (SQLException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testAddFoodItemConsumed() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Food(name, cals, description, approved) VALUES('TESTITEM', 4321, 'TESTITEM', false) RETURNING foodID");
			rs = db.runQuery(sql);
			rs.next();
			int foodid = rs.getInt(1);
			
			sql = db.getConnection().prepareStatement("INSERT INTO SystemUser(email,password,forename,surname,DoB," +
					"sexMale,county) " +
					"VALUES('test','test','test','test','10-10-1990','true','test') RETURNING userID");
			rs = db.runQuery(sql);
			rs.next();
			int userid = rs.getInt(1);
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			java.sql.Date inputDate = null;
			try {
				inputDate = new java.sql.Date(formatter.parse("10-10-2010").getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DietCapture.addFoodItemConsumed(foodid, inputDate, userid, 1);
			sql = db.getConnection().prepareStatement("SELECT * FROM actFood WHERE foodid = ?");
			sql.setInt(1, foodid);
			rs = db.runQuery(sql);
			rs.next();
			
			assertTrue("If food ID exists in actfood, the method executed",rs.getInt("foodID") == foodid);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testAddItemToSetMeal() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("10-10-2010").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DietCapture.addItemToSetMeal(1, 1, 1, 1, inputDate);
		try {
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM foodinmeal WHERE mealid = 1 AND foodid = 1");
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) == 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}

	@Test
	public void testRemoveFoodItem() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO UserActivities(Userid,actdate) VALUES(1, '15-09-1999') RETURNING actID");
			rs = db.runQuery(sql);
			rs.next();
			int actid = rs.getInt(1);
			sql = db.getConnection().prepareStatement("INSERT INTO ActFood VALUES(?, 213, 4)");
			sql.setInt(1, actid);
			db.runUpdate(sql);
			
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			java.sql.Date inputDate = null;
			try {
				inputDate = new java.sql.Date(formatter.parse("15-09-1999").getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DietCapture.removeFoodItem(213, inputDate, 1, "snack");
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM ActFood WHERE actId = ? AND foodID = 213");
			sql.setInt(1, actid);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) == 0);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testAddSetMeal() {
		int [] test = new int[2];
		test[0] = 1;
		test[1] = 2;
		
		DietCapture.addSetMeal("Testmeal", "This is a test meal", test);
		
		try {
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM setmeal WHERE name = 'Testmeal' and description = 'This is a test meal'");
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) >= 1);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testAddIngredient() {
		try{
		sql = db.getConnection().prepareStatement("INSERT INTO setmeal(name,description) VALUES('Testmeal','This is a test meal') RETURNING mealid");
		rs = db.runQuery(sql);
		rs.next();
		int mealid = rs.getInt(1);
		
		DietCapture.addIngredient(mealid, 1, 1);
		sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM foodinmeal where mealID = ?");
		sql.setInt(1, mealid);
		rs = db.runQuery(sql);
		rs.next();
		assertTrue(rs.getInt(1) == 1);
		
		}catch( SQLException e){
			
		}
		

	}

	@Test
	public void testRemoveIngredient() {
		try{
			sql = db.getConnection().prepareStatement("INSERT INTO setmeal(name,description) VALUES('Testmeal','This is a test meal') RETURNING mealid");
			rs = db.runQuery(sql);
			rs.next();
			int mealid = rs.getInt(1);
			
			DietCapture.addIngredient(mealid, 1, 1);
			DietCapture.removeIngredient(mealid,1);
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM foodinmeal where mealID = ?");
			sql.setInt(1, mealid);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) == 0);
			
		}catch( SQLException e){
			
		}
	}

	@Test
	public void testRemoveSetMeal() {
		try{
			sql = db.getConnection().prepareStatement("INSERT INTO setmeal(name,description) VALUES('Testmeal','This is a test meal') RETURNING mealid");
			rs = db.runQuery(sql);
			rs.next();
			int mealid = rs.getInt(1);
			
			DietCapture.removeSetMeal(mealid);
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM Setmeal where mealID = ?");
			sql.setInt(1, mealid);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) == 0);
			
			
			
		}catch( SQLException e){
			
		}
	}

	@Test
	public void testGetFoodAndExerciseOnDate() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO userActivities(userid,actdate) VALUES(1,'01-01-1901') RETURNING actid");
			rs = db.runQuery(sql);
			rs.next();
			int actid = rs.getInt(1);
			sql = db.getConnection().prepareStatement("INSERT INTO actfood VALUES(?,1,1)");
			sql.setInt(1, actid);
			db.runUpdate(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("01-01-1901").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(DietCapture.getFoodAndExerciseOnDate(inputDate, 1));
	}

	@Test
	public void testGetFoodFromID() {
		assertNotNull(DietCapture.getFoodFromID(1));
	}

	@Test
	public void testGetFoodOnDate() {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("01-01-1901").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		assertNotNull(DietCapture.getFoodOnDate(inputDate, 1));
	}

	@Test
	public void testGetFoodItems() {
		assertNotNull(DietCapture.getFoodItems());
	}

	@Test
	public void testSearchFoodItems() {
		assertNotNull(DietCapture.searchFoodItems("TESTITEM"));
	}

	@Test
	public void testGetUserActivitiesForDate() {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYY");
	    try {
			cal.setTime(sdf.parse("01-01-1901"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		assertNotNull(DietCapture.getUserActivitiesForDate(1, cal));
	}

	@Test
	public void testGetSetMealForDetails() {
		assertNotNull(DietCapture.getSetMealForDetails(1, "Testmeal"));
	}

}
