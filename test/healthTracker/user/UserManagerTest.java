package healthTracker.user;

import static org.junit.Assert.*;

import healthTracker.goal.Goal;
import healthTracker.utils.DatabaseController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class UserManagerTest {
	
	DatabaseController db = new DatabaseController();
	PreparedStatement sql = null;
	ResultSet rs;
	int userid;
	
	
	public void getUserID(){
		try{
			sql = db.getConnection().prepareStatement("SELECT userid FROM Systemuser ORDER BY userid DESC limit 1");
			rs = db.runQuery(sql);
			rs.next();
			userid = rs.getInt(1);
			}catch( SQLException e){
			}
	}
	
	@Test
	public void testCreateUser() {
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYY");
	    try {
			cal.setTime(sdf.parse("01-01-1901"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    assertTrue(UserManager.createUser("TEST@TEST", "test", "test", "test", "test", cal, true));
	    
	}

	@Test
	public void testUpdateUserDetails() {
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYY");
	    try {
			cal.setTime(sdf.parse("01-01-1901"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    getUserID();
	    
		UserManager.updateUserDetails(userid, "TEST@TEST", "test", "test", "test", "test", cal, false);
		
		
		try {
			getUserID();
			sql = db.getConnection().prepareStatement("SELECT sexMale FROM SystemUser WHERE userID = ? ");
			sql.setInt(1,userid);
			rs = sql.executeQuery();
			rs.next();
			assertFalse(rs.getBoolean(1));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testUpdateMeasurementPreference() {
		getUserID();
		UserManager.updateMeasurementPreference(userid, "kg");
		try {
			sql = db.getConnection().prepareStatement("SELECT measurementPreference FROM SystemUser WHERE userID = ?");
			sql.setInt(1, userid);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getString(1).equals("kg"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testGetPreviousWeights() {
		getUserID();
		assertNotNull(UserManager.getPreviousWeights(userid, 1));
	}

	@Test
	public void testCheckCredentials() {
		getUserID();
		assertTrue(UserManager.checkCredentials("TEST@TEST", "test"));
	}

	@Test
	public void testHasEnteredDiet() {
		getUserID();
		assertFalse(UserManager.hasEnteredDiet(userid));
	}

	

	@Test
	public void testDoesEmailExist() {
		getUserID();
		assertTrue(UserManager.doesEmailExist("TEST@TEST"));
	}

	@Test
	public void testVerifyUser() {
		getUserID();
		UserManager.verifyUser(userid);
		try {
			sql = db.getConnection().prepareStatement("SELECT verified FROM SystemUser WHERE userID = ?");
			sql.setInt(1, userid);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getBoolean(1));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testUpdateInitialWeightAndHeight() {
		getUserID();
		UserManager.updateInitialWeightAndHeight(userid, 1, 1);
		try {
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM UserHistory WHERE userID = ?");
			sql.setInt(1, userid);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) >= 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetLastHistory() {
		getUserID();
		assertNotNull(UserManager.getLastHistory(userid-1));
	}

	@Test
	public void testGetUserFromID() {
		getUserID();
		assertNotNull(UserManager.getUserFromID(userid));
	}

	@Test
	public void testFindUserByEmail() {
		assertNotNull(UserManager.findUserByEmail("TEST@TEST"));
	}

	@Test
	public void testGetDatesLastEnteredInUserActivities() {
		getUserID();
		assertNotNull(UserManager.getDatesLastEnteredInUserActivities(userid, 1));
	}

	@Test
	public void testGetUsersFromGoals() {
		getUserID();
		try {
			sql = db.getConnection().prepareStatement("BEGIN;");
			sql.executeUpdate();
			sql = db.getConnection().prepareStatement("INSERT INTO goal(userID, goaldate,complete) VALUES(?,'10-10-3012',false) RETURNING goalID");
			sql.setInt(1, userid);
			rs = sql.executeQuery();
			rs.next();
			int goalID = rs.getInt(1);
			ArrayList<Goal> test = new ArrayList<Goal>();
			Goal testGoal = new Goal(goalID, userid, null, null, 
					1, 2,
					false, true);
			test.add(testGoal);
			assertNotNull(UserManager.getUsersFromGoals(test));
			sql = db.getConnection().prepareStatement("ROLLBACK;");
			sql.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUser() {
		getUserID();
		UserManager.removeUser(userid);
		try {
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM SystemUser WHERE userID = ?");
			sql.setInt(1, userid);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) == 0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
