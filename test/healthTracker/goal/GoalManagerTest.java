package healthTracker.goal;

import static org.junit.Assert.*;

import healthTracker.utils.DatabaseController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;

public class GoalManagerTest {
	
	
	int goalID;
	DatabaseController db = new DatabaseController();
	PreparedStatement sql = null;
	ResultSet rs;

	
	
	@Test
	public void testCreateGoal() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("01-01-1901").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		goalID =  (GoalManager.createGoal(1, inputDate, inputDate, true, 1, 2));
		try{
		sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM goal where goalID = ?");
		sql.setInt(1, goalID);
		rs = db.runQuery(sql);
		rs.next();
		assertTrue("Check goal now exists in database", rs.getInt(1) == 1);}
		
		catch( SQLException e){
			
		}
	
	}

	@Test
	public void testCreateGroupGoal() {
		
		
		try{
			sql = db.getConnection().prepareStatement("SELECT goalid FROM goal ORDER BY goalid DESC limit 1");
			sql.setInt(1, goalID);
			rs = db.runQuery(sql);
			rs.next();
			
			GoalManager.createGroupGoal(rs.getInt(1), 1);
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM groupgoal where goalID = ?");
			sql.setInt(1, goalID);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check goal now exists in groupgoal table",rs.getInt(1) == 1);}
			
			catch( SQLException e){
				
			}
	}

	@Test
	public void testRemoveGroupGoal() {
		try{
			sql = db.getConnection().prepareStatement("BEGIN;");
			db.runUpdate(sql);
			sql = db.getConnection().prepareStatement("SELECT goalid FROM goal ORDER BY goalid DESC limit 1");
			sql.setInt(1, goalID);
			rs = db.runQuery(sql);
			rs.next();
			goalID = rs.getInt(1);
			
			GoalManager.removeGroupGoal(goalID);
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM groupgoal,goal where groupgoal.goalID = ? OR goal.goalID = ?");
			sql.setInt(1, goalID);
			sql.setInt(2, goalID);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check goal is gone from group goal",rs.getInt(1) == 0);
		
			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);}
			catch( SQLException e){
				
			}
	}

	@Test
	public void testEditGoal() {
		try{
		sql = db.getConnection().prepareStatement("SELECT goalid FROM goal ORDER BY goalid DESC limit 1");
		rs = db.runQuery(sql);
		rs.next();
		goalID = rs.getInt(1);
		}catch( SQLException e){
			
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("01-01-1902").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Goal testGoal;
		try {
			sql = db.getConnection().prepareStatement("BEGIN;");
			db.runUpdate(sql);
			
			testGoal = new Goal(goalID,1,inputDate,inputDate,1,2,true,true);
			GoalManager.editGoal(testGoal);
			
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM goal WHERE goalID = ? AND goalDate = '01-01-1902'");
			sql.setInt(1, goalID);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) == 1);

			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Test
	public void testCompleteGoal() {
		try {
			sql = db.getConnection().prepareStatement("BEGIN;");
			db.runUpdate(sql);
			sql = db.getConnection().prepareStatement("SELECT goalid FROM goal ORDER BY goalid DESC limit 1");
			rs = db.runQuery(sql);
			rs.next();
			goalID = rs.getInt(1);
			GoalManager.completeGoal(goalID);
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM goal WHERE goalID = ? AND complete = true");
			sql.setInt(1, goalID);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) == 1);

			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);
			
			
		
			}catch( SQLException e){
				
			}
	}

	@Test
	public void testRemoveGoal() {
		try {
			sql = db.getConnection().prepareStatement("BEGIN;");
			db.runUpdate(sql);
			sql = db.getConnection().prepareStatement("SELECT goalid FROM goal ORDER BY goalid DESC limit 1");
			rs = db.runQuery(sql);
			rs.next();
			goalID = rs.getInt(1);
			GoalManager.removeGoal(goalID);
			sql = db.getConnection().prepareStatement("SELECT COUNT(*) FROM goal WHERE goalID = ?");
			sql.setInt(1, goalID);
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) == 0);
			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);
			}catch( SQLException e){
			}
	}

	@Test
	public void testGetGoalFromID() {
		try {
			sql = db.getConnection().prepareStatement("BEGIN;");
			db.runUpdate(sql);
			sql = db.getConnection().prepareStatement("SELECT goalid FROM goal ORDER BY goalid DESC limit 1");
			rs = db.runQuery(sql);
			rs.next();
			goalID = rs.getInt(1);
			Goal testGoal = GoalManager.getGoalFromID(goalID);
			assertTrue(testGoal.getUserID() == 1);
			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);
			}catch( SQLException e){
			}
	}

	@Test
	public void testGetUserGoals() {
		assertNotNull("We know there must be at least one goal for this user",GoalManager.getUserGoals(1));
		
	}

	@Test
	public void testGetGroupGoals() {
		assertNotNull("We know there must be at least one goal for this group too",GoalManager.getGroupGoals(1, 1));
	}

	@Test
	public void testUpdateGoal() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		java.sql.Date inputDate = null;
		try {
			inputDate = new java.sql.Date(formatter.parse("01-01-1902").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Goal testGoal;
		try {
			sql = db.getConnection().prepareStatement("BEGIN;");
			db.runUpdate(sql);
			testGoal = new Goal(goalID,1,inputDate,inputDate,1,2,true,true);
			GoalManager.updateGoal(testGoal);
			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);
			//Assuming it gets this far, it didn't break
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testIsSubscribedToGroupGoal() {
		assertFalse(GoalManager.isSubscribedToGroupGoal(1, 1));
	}

	@Test
	public void testSubscribeUserToGroupGoal() {
		try {
			sql = db.getConnection().prepareStatement("BEGIN; ");
			db.runUpdate(sql);
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GoalManager.subscribeUserToGroupGoal(1, 1);
		try {
			sql = db.getConnection().prepareStatement("SELECT * FROM UserInGroupGoal WHERE groupGoalID = 1 AND individualgoalid = 1;");
			rs = sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) == 1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testDidCreateGroupGoal() {
		assertTrue(GoalManager.didCreateGroupGoal(1));
	}

	@Test
	public void testUnsubscribeUserFromGroupGoal() {
		GoalManager.unsubscribeUserFromGroupGoal(1, 1);
		try {
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM UserInGroupGoal WHERE groupGoalID = 1 AND individualgoalid = 1;");
			rs =sql.executeQuery();
			rs.next();
			assertTrue(rs.getInt(1) == 0);
			sql = db.getConnection().prepareStatement("ROLLBACK;");
			db.runUpdate(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testIsPartOfGroupGoal() {
		assertNotNull(GoalManager.isPartOfGroupGoal(1));
	}

	@Test
	public void testGetNumberOfActiveGoals() {
		assertTrue(GoalManager.getNumberOfActiveGoals(1) > 0);
	}

	@Test
	public void testGetGroupGoalProgress() {
		assertNotNull(GoalManager.getGroupGoalProgress(1));
	}

	@Test
	public void testGetEmailsOfGroupUsers() {
		assertNotNull(GoalManager.getEmailsOfGroupUsers(1, 20));
	}
	
	@Test
	public void testRemoveUserIDInGroupGoal() {

			GoalManager.removeUserIDInGroupGoal(1);	
			//Assuming it gets this far, it didn't break

}
}