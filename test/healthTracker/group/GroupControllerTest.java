package healthTracker.group;

import static org.junit.Assert.*;

import healthTracker.utils.DatabaseController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

public class GroupControllerTest {

	DatabaseController db = new DatabaseController();
	PreparedStatement sql = null;
	ResultSet rs;
	int groupID;

	@Test
	public void testCreateGroup() {
		assertTrue(GroupController.createGroup("TEST GROUP",
				"TEST DESCRIPTION", 1));

	}

	@Test
	public void testAddUser() {
		try {
			sql = db.getConnection()
					.prepareStatement(
							"SELECT groupid FROM groupdetails ORDER BY groupid DESC limit 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			groupID = rs.getInt(1);
			GroupController.addUser(groupID, 1);

			sql = db.getConnection().prepareStatement(
					"SELECT COUNT(*) FROM groupuser where groupid = ?");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) == 1);
		} catch (SQLException e) {

		}
	}

	@Test
	public void testGetGroupsFromUser() {
		assertNotNull(GroupController.getGroupsFromUser(1));
	}
	
	@Test
	public void testLeaveGroup() {
		try {
			
			sql = db.getConnection()
					.prepareStatement(
							"SELECT groupid FROM groupdetails ORDER BY groupid DESC limit 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			groupID = rs.getInt(1);
			GroupController.leaveGroup(groupID, 1);

			sql = db.getConnection()
					.prepareStatement(
							"SELECT COUNT(*) FROM groupuser where groupid = ? AND userid = 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue(rs.getInt(1) == 0);

		} catch (SQLException e) {

		}
	}


	@Test
	public void testGetGroupsFromSearch() {
		assertNotNull(GroupController.getGroupsFromSearch("TEST GROUP"));
	}

	@Test
	public void testDoesGroupNameExist() {
		assertTrue(GroupController.doesGroupNameExist("TEST GROUP"));
	}

	@Test
	public void testDoesGroupIDExist() {
		try {
			
			sql = db.getConnection()
					.prepareStatement(
							"SELECT groupid FROM groupdetails ORDER BY groupid DESC limit 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			groupID = rs.getInt(1);
			assertTrue(GroupController.doesGroupIDExist(groupID));

		} catch (SQLException e) {

		}
	}

	@Test
	public void testGetGroupMembersFromGroupID() {
	try {
			
			sql = db.getConnection()
					.prepareStatement(
							"SELECT groupid FROM groupdetails ORDER BY groupid DESC limit 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			groupID = rs.getInt(1);
			assertNotNull(GroupController.getGroupMembersFromGroupID(groupID));

		} catch (SQLException e) {
		
		}
	}

	@Test
	public void testHasJoinedGroup() {
	try {
			
			sql = db.getConnection()
					.prepareStatement(
							"SELECT groupid FROM groupdetails ORDER BY groupid DESC limit 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			groupID = rs.getInt(1);
			assertTrue(GroupController.hasJoinedGroup(1, groupID));

		} catch (SQLException e) {
		
		}
	}
	
	@Test
	public void testGetGroupFromID() {
	try {
			
			sql = db.getConnection()
					.prepareStatement(
							"SELECT groupid FROM groupdetails ORDER BY groupid DESC limit 1");
			sql.setInt(1, groupID);
			rs = db.runQuery(sql);
			rs.next();
			groupID = rs.getInt(1);
		
			assertNotNull(GroupController.getGroupFromID(groupID));

		} catch (SQLException e) {

		}
	}

}
