package healthTracker.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConversionsTest {

	@Test
	public void testCalculateBMI() {
		assertTrue(Conversions.calculateBMI(200, 2) == 50);
	}

	@Test
	public void testCmToInch() {
		assertTrue(Conversions.cmToInch(100) == 39.37);
	}

	@Test
	public void testInchToCM() {

		assertTrue(Conversions.inchToCM(100) == 254);
	}

	@Test
	public void testFeetToCM() {
		
		assertTrue(Conversions.feetToCM(100) == 3048);
	}

	@Test
	public void testCmToFeet() {
		double test = Conversions.cmToFeet(100);
		assertTrue(Conversions.cmToFeet(100) == 3.28);
	}

	@Test
	public void testKgToPound() {
		assertTrue(Conversions.kgToPound(100) == 220.46);
	}

	@Test
	public void testPoundToKg() {
		assertTrue(Conversions.poundToKg(100) == 45.36);
	}

	@Test
	public void testKgToStone() {
		assertTrue(Conversions.kgToStone(100) == 15.75);
	}

	@Test
	public void testStoneToKG() {

		assertTrue(Conversions.stoneToKG(100) == 635.04);
	}

	@Test
	public void testBmiAndHeightToWeight() {
		assertTrue(Conversions.bmiAndHeightToWeight(50, 2) == 200);
	}

	@Test
	public void testRoundTwoDecimals() {
		assertTrue(Conversions.roundTwoDecimals(1.123) ==1.12);
	}

}
