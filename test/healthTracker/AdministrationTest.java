package healthTracker;

import static org.junit.Assert.*;
import healthTracker.captures.Exercise;
import healthTracker.captures.Food;
import healthTracker.utils.DatabaseController;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

public class AdministrationTest {
	
	DatabaseController db = new DatabaseController();
	PreparedStatement sql = null;
	ResultSet rs;

	@Test
	public void testDeleteGroup() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO groupdetails(groupname,groupdesc) " +
					"VALUES('A group for testing','A group for testing') RETURNING groupID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			Administration.deleteGroup(id);
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM groupdetails WHERE groupid = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check there are no groups of this id", rs.getInt(1) == 0);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteUser() {
try {
			
			sql = db.getConnection().prepareStatement("INSERT INTO SystemUser(email,password,forename,surname,DoB," +
					"sexMale,county) " +
					"VALUES('test','test','test','test','10-10-1990','true','test') RETURNING userID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			Administration.deleteUser(id);
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM SystemUser WHERE userid = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("User correctly removed", rs.getInt(1) == 0);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("SQL Failure");
		}
	}

	@Test
	public void testGetUnapprovedExercises() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Exercise(name,calsPerUnit,description,approved) VALUES('test',1,'test','false')" +
					" RETURNING exerciseID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			ArrayList<Exercise> retrievedList = new ArrayList<Exercise>();
			retrievedList = Administration.getUnapprovedExercises();
			boolean recordExists = false;
			for(Exercise e : retrievedList){
				if(e.getExID() == id){
					recordExists = true;
				}
			}
			assertTrue("Confirm record has been added", recordExists);
			sql = db.getConnection().prepareStatement("DELETE FROM exercise Where exerciseID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testGetUnapprovedFoods() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Food(name,cals,description,approved) VALUES('test',1,'test','false')" +
					" RETURNING foodID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			ArrayList<Food> retrievedList = new ArrayList<Food>();
			retrievedList = Administration.getUnapprovedFoods();
			boolean recordExists = false;
			for(Food f : retrievedList){
				if(f.getFoodID() == id){
					recordExists = true;
				}
			}
			assertTrue("Confirm record has been added", recordExists);
			sql = db.getConnection().prepareStatement("DELETE FROM Food WHERE foodID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testApproveFoodItem() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Food(name,cals,description,approved) VALUES('test',1,'test','false')" +
					" RETURNING foodID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			Administration.approveFoodItem(id);
			sql = db.getConnection().prepareStatement("SELECT * FROM Food WHERE foodid = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check Status has been changed", rs.getBoolean("approved"));
			sql = db.getConnection().prepareStatement("DELETE FROM Food WHERE foodID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testRemoveFoodItem() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Food(name,cals,description,approved) VALUES('test',1,'test','false')" +
					" RETURNING foodID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			Administration.removeFoodItem(id);
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM Food WHERE foodid = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check there is no food of the test id", rs.getInt(1) == 0);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testApproveExerciseItem() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Exercise(name,calsPerUnit,description,approved) VALUES('test',1,'test','false')" +
					" RETURNING exerciseID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			Administration.approveExerciseItem(id);
			sql = db.getConnection().prepareStatement("SELECT * FROM Exercise WHERE exerciseid = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check Status has been changed", rs.getBoolean("approved"));
			sql = db.getConnection().prepareStatement("DELETE FROM Exercise WHERE exerciseID = ?");
			sql.setInt(1, id);
			db.runUpdate(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testRemoveExerciseItem() {
		try {
			sql = db.getConnection().prepareStatement("INSERT INTO Exercise(name,calsPerUnit,description,approved) VALUES('test',1,'test','false')" +
					" RETURNING exerciseID");
			rs = db.runQuery(sql);
			rs.next();
			int id = rs.getInt(1);
			Administration.removeExerciseItem(id);
			sql = db.getConnection().prepareStatement("SELECT COUNT (*) FROM Exercise WHERE exerciseid = ?");
			sql.setInt(1, id);
			rs = db.runQuery(sql);
			rs.next();
			assertTrue("Check there is no exercise of the test id", rs.getInt(1) == 0);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetUsers() {
		
		assertNotNull(Administration.getUsers());
	}

}
