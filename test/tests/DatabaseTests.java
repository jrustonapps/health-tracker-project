package tests;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import healthTracker.utils.DatabaseController;

/**
 * DatabaseTests.java
 * 
 * Runs some tests on the DatabaseController to ensure it works as intended.
 * 
 * @author 6266215
 */
public class DatabaseTests {

	private static DatabaseController db = null;
	private static Connection con = null;
	private static PreparedStatement sql = null;
	private static ResultSet result = null;
	
	/**
	 * Connects to our PostgreSQL database.
	 */
	@BeforeClass
	public static void connectDB() {
		
		db = new DatabaseController();
		con = db.getConnection();
	}

	/**
	 * Test that we can connect to the database.
	 */
	@Test
	public void testConnect() {
		
		db = new DatabaseController();
		assertNotNull("DatabaseController object failed to create", db);
	}
	
	/**
	 * Test the database query function. Should return a ResultSet that is not 
	 * null.
	 */
	@Test
	public void testQuery() {
		
		try {
			sql = con.prepareStatement("SELECT * FROM SystemUser");
			result = db.runQuery(sql);
			
			assertNotNull("Null result returned", result);
			result.close();
		} catch (SQLException e) {
			System.err.println("Failed to close ResultSet.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes anything we may have left over in the class.
	 */
	@AfterClass
	public static void tearDown() {
	
		try {
			result.close();
		} catch (SQLException e) {
			System.err.println("Could not close ResultSet!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					con.close();
				} catch (SQLException exc) {
					System.err.println("Could not close connection!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					result = null;
					sql = null;
					con = null;
					db = null;
				}
			}
		}
	}
}
