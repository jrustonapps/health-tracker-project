package tests;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import healthTracker.user.UserManager;
import healthTracker.utils.DatabaseController;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserTests {
	
	private static DatabaseController db = null;
	private static Connection con = null;
	private static PreparedStatement sql = null;
	private static ResultSet result = null;
	
	/**
	 * Connects to our PostgreSQL database.
	 */
	@BeforeClass
	public static void connectDB() {
		
		db = new DatabaseController();
		con = db.getConnection();
	}

	/**
	 * Tests the user creation method.
	 */
	@Test
	public void testCreation() {
		
		fail("Not implemented");
	}
	
	/**
	 * Tests the login functionality of the User class.
	 */
	@Test
	public void testLogin() {

		boolean result = UserManager.checkCredentials("testUser", "password");
		assertTrue("User with correct details autheticated", result);
		
		result = UserManager.checkCredentials("testUser", "wrongpass");
		assertFalse("User with incorrect details denied", result);
	}
	
	/**
	 * Removes anything we may have left over in the class.
	 */
	@AfterClass
	public static void tearDown() {
	
		try {
			result.close();
		} catch (SQLException e) {
			System.err.println("Could not close ResultSet!");
			System.err.println(e.getMessage());
		} finally {
			try {
				sql.close();
			} catch (SQLException ex) {
				System.err.println("Failed to close PreparedStatement!");
				System.err.println(ex.getMessage());
			} finally {
				try {
					con.close();
				} catch (SQLException exc) {
					System.err.println("Could not close connection!");
					System.err.println(exc.getMessage());
				} finally {
					db.closeConnection();
					result = null;
					sql = null;
					con = null;
					db = null;
				}
			}
		}
	}

}
