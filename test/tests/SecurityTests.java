package tests;

import static org.junit.Assert.*;
import healthTracker.utils.Security;

import org.junit.Test;

/**
 * SecurityTests.java
 * 
 * Tests the functionality of the Security class, to ensure it creates valid 
 * hashes and SQL statements.
 * 
 * @author 6266215
 */
public class SecurityTests {

	/**
	 * Test MD5 encoding function to ensure it returns correct values.
	 */
	@Test
	public void testMD5() {
		
		String hash = Security.hashMD5("password");
		String MD5Result = "5f4dcc3b5aa765d61d8327deb882cf99";
		assertNotNull("Returned null", hash);
		assertEquals("Incorrect result", MD5Result, hash);
	}
	
	/**
	 * Test SHA-1 hashing function to ensure it returns correct values.
	 */
	@Test
	public void testSHA1() {
		
		String hash = Security.hashSHA1("password");
		String SHA1Result = "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8";
		assertNotNull("Returned null", hash);
		assertEquals("Incorrect result", SHA1Result, hash);
	}
}
