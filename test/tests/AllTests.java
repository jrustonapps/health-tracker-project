package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * AllTests.java
 * 
 * Runs all JUnit tests.
 * 
 * @author 6266215
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ 
	SecurityTests.class,
	DatabaseTests.class,
	UserTests.class 
})
public class AllTests {

}
